<?php

use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {

    if (Auth::check()) {
        return redirect('/admin/home');
    } else {

        return redirect('/admin/login');

    }
});


/******************  START ADMIN ROUTE::    ***********************************************/
Route::group(['prefix' => 'admin', 'namespace' => 'Admin\Auth', 'middleware' => 'guest'], function () {

    Route::get('/login', function () {
        return view('admin.auth.login');
    })->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'RegisterController@register');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');


});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin\Auth', 'middleware' => 'auth'], function () {
    Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
    Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
    Route::get('/logout', 'LoginController@logout')->name('admin.logout');

});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/upload', 'HomeController@uploadFile');
    Route::post('/excel-retailers-data', 'HomeController@saveRetailerData');
    Route::resource('client', 'ClientController');
    Route::get('/client-print-details/{id}','ClientController@print');
    Route::resource('retailers', 'RetailersController');
    Route::resource('sales-persons', 'SalesPersonsController');
    Route::resource('categories', 'CategoriesController');
    Route::resource('accounts', 'AccountsController');
    Route::resource('client-details', 'ClientDetailController');
    Route::get('rml',function () {
        return view('admin.rml');
    });
    Route::get('new-dubai',function () {
        return view('new-dubai');
    });

    Route::get('client-dubai/create-dubai-report','ClientController@dubaiReport');
    Route::post('client-dubai/create-dubai-report','ClientController@dubaiReportStore');
    Route::PATCH('client-dubai/create-dubai-report/{id}','ClientController@dubaiReportupdate');


    Route::get('/client-details-print-details/{id}','ClientDetailController@print');
    Route::get('client-dubai/create-dubai-report-client-details','ClientDetailController@dubaiReport');
    Route::post('client-dubai/create-dubai-report-client-details','ClientDetailController@dubaiReportStore');
    Route::PATCH('client-dubai/create-dubai-report-client-details/{id}','ClientDetailController@dubaiReportupdate');

    Route::get('dubai',function () {
        return view('admin.dubai');
    });

    Route::get('mumbai',function () {
        return view('admin.mumbai');
    });

    Route::get('delhi-gagan',function () {
        return view('admin.gagan');
    });

    Route::get('g2nme-report',function () {
        return view('admin.g2nme');
    });

    Route::get('new-lucknow',function () {
        return view('admin.new-lucknow');
    });

});
Route::fallback(function ($e) {
    return view('errors.404');
});






