
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result -National Technology</title>
    <link href="{{asset('dubai/css/1.css')}}" rel="stylesheet" />

    <script src="{{asset('dubai/js/3.js')}}"></script>

</head>
<body>

<div class="container body-content">


    <link href='https://fonts.googleapis.com/css?family=Cairo' rel='stylesheet'>
    <link href="{{asset('dubai/css/Site.css')}}" rel="stylesheet" />
    <link href="{{asset('dubai/js/footer.js')}}"  />
    <style>
        body {
            height: 100%;
        }

        .lb-large {
            font-size: large;
        }

        .label {
            color: blue;
            font-size: 17px;
        }

        .text {
            font-size: 17px;
        }
    </style>


    <div class="container">


        <div class="row" style="flex-wrap: wrap-reverse;" id="TEST">
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 text-center">
                <img src="{{asset('Eurofins.png')}}" class="logo" />
            </div>

            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 text-left">
                <div class="test-txt">Eurofins</div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 text-left ">
                <div class="test-txt2">COVID-19 by RT-PCR Test Result</div>
            </div>
        </div>
        <div class="row" id="TEST" style="padding-top:30px">
            <div class="col-md-7 text-left order-1">

                <label class="label">Patient Name :</label></br>
                <label class="text-label">Maryam Ahsan Ahmad Shaikh </label>
            </div>
            <div class="col-md-5 text-right order-2" style="float:right;">

                <div class="result-box result-nagative">Not Detected (Negative)</div>


            </div>
        </div>


        <form>
            <div class="row" style="margin-top:2%">

                <div class="bg-div col-md-3 col-lg-3 col-sm-6 col-xs-6">
                    <label class=" col-form-label label" for="Gender">Gender :</label><br />
                    <label class=" col-form-label text-label" id="Gender">Female </label>
                </div>

                <div class=" bg-div col-md-3 col-lg-3 col-sm-6 col-xs-6">

                    <label class=" col-form-label label" for="DOB">Date Of Birth :</label><br />
                    <label class=" col-form-label text-label" id="DOB">25/02/1986</label>

                </div>

                <div class="bg-div  col-md-3 col-lg-3 col-sm-6 col-xs-6">
                    <label class=" col-form-label label" for="Nationality">Nationality :</label><br />
                    <label class=" col-form-label text-label" id="Nationality">India </label>
                </div>
                <div class="bg-div  col-md-3 col-lg-3 col-sm-6 col-xs-6">
                    <label class=" col-form-label label" for="Passport">Passport No :</label><br />
                    <label class=" col-form-label text-label" id="Passport"> </label>
                </div>
            </div>

            <div class="row">

                <div class="bg-div col-md-3 col-lg-3 col-sm-6 col-xs-6">
                    <label class=" col-form-label label" for="UaeID">UAE ID :</label><br />
                    <label class=" col-form-label text-label" id="UaeID"> 784-1986-9304316-2</label>
                </div>

                <div class="bg-div col-md-3 col-lg-3 col-sm-6 col-xs-6">
                    <label class="col-form-label label">MRN : </label><br />
                    <label class="col-form-label text-label">662586-0 </label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6" style="height:30px;"></div>
            </div>


            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style=" padding:10px;">
                    <label class=" col-form-label label" for="RequestDate">Request Date :</label><br />
                    <label class=" col-form-label text-label" id="RequestDate"> 07/12/2021 02:59 PM</label>
                    <div class="div-line"></div>
                </div>

                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style=" padding:10px;">
                    <label class=" col-form-label label" for="CollectionDate">Collection Date :</label><br />
                    <label class=" col-form-label text-label" id="CollectionDate">
                        07/12/2021
                        01:00 PM
                    </label>
                    <div class="div-line"></div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style=" padding:10px;">
                    <label class=" col-form-label label" for="ReportingDate">Reporting Date :</label><br />
                    <label class=" col-form-label text-label" id="ReportingDate">

                        07/12/2021

                        07:15 PM
                    </label>

                    <div class="div-line"></div>
                </div>

                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" style=" padding:10px;">
                    <label class=" col-form-label label" for="AccNumber">Accession No :</label><br />
                    <label class=" col-form-label text-label" id="AccNumber">1361162</label>
                    <div class="div-line"></div>
                </div>
            </div>





            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="padding-top:30px;">
                    <label class="Performed-txt" for="TestPerformedBy">Test Performed By :</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <label class="Performed-txt2" id="TestPerformedBy">Eurofins - Dubai - UAE </label>
                </div>
            </div>


        </form>


    </div>

    <footer class="footer">
        <div class="container">
        <span class="footer-txt">
            Copyright © 2000-2021 <a href="http://nt-me.com/" target="_blank">National Technology Company, </a> All rights reserved.<br>
        </span>
        </div>
    </footer>


</div>

<script src="{{asset('dubai/js/1.js')}}"></script>

<script src="{{asset('dubai/js/2.js')}}"></script>


</body>
</html>
