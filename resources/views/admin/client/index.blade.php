@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="panel panel-success">
            <div class="panel-heading">Client</div>
            <div class="panel-body">
                <div class="col-md-12 page-action text-left">
                    <h3 style="color: red"><strong>SHL-MUMBAI-LUCKNOW</strong></h3>
                    <a href="{{ url('/admin/client/create') }}" class="btn btn-success btn-sm" title="Add New Client">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New India
                    </a>
                    <a href="{{ url('/admin/client-dubai/create-dubai-report') }}"
                       class="btn btn-danger btn-sm pull-right" title="Add New Client">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New Dubai
                    </a>
                </div>
                <div class="col-md-12">
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/client', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="data-table">
                            <thead>
                            <tr style="background: lightpink">
                                <th>Sr.no</th>
                                <th>Name</th>
                                <th>Passport No:</th>
                                <th>DOB</th>
                                <th>Age</th>
                                <th>LAB NO</th>
                                <th>Date</th>
                                <th>Payment Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($client as $key => $item)
                                <tr>
                                    <?php
                                    $first_date = new DateTime($item->created_at);
                                    $second_date = new DateTime(date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $item->dob))));
                                    $interval = $first_date->diff($second_date);

                                    ?>
                                    <td>{{ (($client->currentPage() - 1 ) * $client->perPage() ) + $loop->iteration }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->passport_no }}</td>
                                    <td>{{ $item->dob }}</td>
                                    <td>{{ $interval->y . " years, " . $interval->m." months, ".$interval->d." days " }}</td>
                                    <td>{{ $item->lab_no }}</td>
                                    <th>{{date('d F Y',strtotime($item->created_at))}}</th>
                                    <th>{!! $item->status==1?'<b style="color:green"><i class="fa fa-check"></i> PAID</b>':'<b style="color:red"><i class="fa fa-warning"></i> PENDING</b>' !!}</th>
                                    <td class="text-center">


                                        <a href="{{ url('/admin/client/' . $item->id . '/edit') }}" title="Edit Client">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                      aria-hidden="true"></i> Edit
                                            </button>
                                        </a>
                                        <a href="{{ url('/admin/client-print-details/' . $item->id) }}"
                                           title="Edit Client" target="_blank">
                                            <button class="btn btn-success btn-xs  print-button"><i class="fa fa-print"
                                                                                                    aria-hidden="true"></i>
                                                Print
                                            </button>
                                        </a>

                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div
                                class="pagination-wrapper"> {!! $client->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.print-button').click(function () {
            $(this).css('background', 'red');
        });
    </script>
@endsection
