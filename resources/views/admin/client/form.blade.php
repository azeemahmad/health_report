<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Client</b></div>
        <br/>
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="col-md-4 control-label">{{ 'Name' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="name" type="text" id="name"
                       value="{{ isset($client->name) ? $client->name : old('name')}}" required>
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
{{--        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">--}}
{{--            <label for="email" class="col-md-4 control-label">{{ 'Email' }} :</label>--}}
{{--            <div class="col-md-6">--}}
{{--                <input class="form-control" name="email" type="email" id="email"--}}
{{--                       value="{{ isset($client->email) ? $client->email : old('email')}}">--}}
{{--                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="form-group {{ $errors->has('passport_no') ? 'has-error' : ''}}">
            <label for="passport_no" class="col-md-4 control-label">{{ 'Passport No' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="passport_no" type="text" id="passport_no"
                       value="{{ isset($client->passport_no) ? $client->passport_no : old('passport_no')}}">
                {!! $errors->first('passport_no', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
            <label for="passport_no" class="col-md-4 control-label">{{ 'Date of Birth' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="dob" type="text" id="dob"
                       value="{{ isset($client->dob) ? $client->dob : old('dob')}}">
                {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('registered_date') ? 'has-error' : ''}}">
            <label for="registered_date" class="col-md-4 control-label">{{ 'Registered Date' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="registered_date" type="text" id="registered_date"
                       value="{{ isset($client->registered_date) ? $client->registered_date : date("d/m/Y h:iA",strtotime(date("Y-m-d H:i:s",strtotime(str_replace('/', '-', $lastData->registered_date)." +2 minutes"))))}}">
                {!! $errors->first('registered_date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('sample_date') ? 'has-error' : ''}}">
            <label for="sample_date" class="col-md-4 control-label">{{ 'Sample Date' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="sample_date" type="text" id="sample_date"
                       value="{{ isset($client->sample_date) ? $client->sample_date : date("d/m/Y h:iA",strtotime(date("Y-m-d H:i:s",strtotime(str_replace('/', '-', $lastData->sample_date)." +2 minutes"))))}}">
                {!! $errors->first('sample_date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('reported_date') ? 'has-error' : ''}}">
            <label for="reported_date" class="col-md-4 control-label">{{ 'Reported Date' }} :</label>
            <div class="col-md-6">

                <input class="form-control" name="reported_date" type="text" id="reported_date"
                       value="{{ isset($client->reported_date) ? $client->reported_date : date("d/m/Y h:iA",strtotime(date("Y-m-d H:i:s",strtotime(str_replace('/', '-', $lastData->reported_date)." +2 minutes"))))}}">
                {!! $errors->first('reported_date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('sample_no') ? 'has-error' : ''}}">
            <label for="sample_no" class="col-md-4 control-label">{{ 'Sample NO' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="sample_no" type="text" id="sample_no"
                       value="{{ isset($client->sample_no) ? $client->sample_no : $lastData->sample_no+2}}">
                {!! $errors->first('sample_no', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('lab_no') ? 'has-error' : ''}}">
            <label for="lab_no" class="col-md-4 control-label">{{ 'LAB NO' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="lab_no" type="text" id="lab_no"
                       value="{{ isset($client->lab_no) ? $client->lab_no : str_pad($lastData->lab_no+2,12,'0',STR_PAD_LEFT)}}">
                {!! $errors->first('lab_no', '<p class="help-block">:message</p>') !!}
            </div>
        </div>





        <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
            <label for="gender" class="col-md-4 control-label" style="color: red">{{ 'GENDER' }} :</label>
            <div class="col-md-6">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input"  name="gender"   value="male" checked>
                    <label class="custom-control-label"> MALE</label>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" class="custom-control-input" name="gender" value="female" @if(isset($client->gender) && $client->gender=='female') checked @endif>
                    <label class="custom-control-label"> FEMALE</label>
                </div>

                {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
            </div>
        </div>




        <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
            <label for="type" class="col-md-4 control-label" style="color: red">{{ 'REPORT TYPE' }} :</label>
            <div class="col-md-6">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input"  name="type"   value="normal" >
                    <label class="custom-control-label"> NORMAL</label>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" class="custom-control-input" name="type" value="google" checked>
                    <label class="custom-control-label"> GOOGLE</label>
                </div>

                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('report_type') ? 'has-error' : ''}}">
            <label for="report_type" class="col-md-4 control-label" style="color: red">{{ 'Negative / Positive' }} :</label>
            <div class="col-md-6">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input"  name="report_type"   value="negative" checked>
                    <label class="custom-control-label"> NEGATIVE</label>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" class="custom-control-input" name="report_type" value="positive" @if(isset($client->report_type) && $client->report_type=='positive') checked @endif>
                    <label class="custom-control-label"> POSITIVE</label>
                </div>

                {!! $errors->first('report_type', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
            <label for="country" class="col-md-4 control-label" style="color: red">{{ 'Country' }} :</label>
            <div class="col-md-6">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input"  name="country"   value="india" checked>
                    <label class="custom-control-label"> INDIA</label>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" class="custom-control-input" name="country" value="dubai">
                    <label class="custom-control-label"> DUBAI</label>
                    <input type="radio" class="custom-control-input" name="country" value="heli">
                    <label class="custom-control-label"> Heli</label>
                    <input type="radio" class="custom-control-input" name="country" value="lucknow">
                    <label class="custom-control-label"> Lucknow</label>
                </div>

                {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('passport_display') ? 'has-error' : ''}}">
            <label for="passport_display" class="col-md-4 control-label" style="color: red">{{ 'Passport Display' }} :</label>
            <div class="col-md-6">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input"  name="passport_display"   value="1" checked>
                    <label class="custom-control-label"> YES</label>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" class="custom-control-input" name="passport_display" value="0" @if(isset($client->passport_display) && $client->passport_display==0) checked @endif>
                    <label class="custom-control-label"> NO</label>
                </div>

                {!! $errors->first('passport_display', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
