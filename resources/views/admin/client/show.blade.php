@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <div class="panel panel-success">
                    <div class="panel-heading">Client {{ $client->id }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/client') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>

                        <a href="{{ url('/admin/client/' . $client->id . '/edit') }}" title="Edit Client">
                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/client' . '/' . $client->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Client"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $client->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $client->name }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $client->email }} </td>
                                </tr>
                                <tr>
                                    <th> Mobile</th>
                                    <td> {{ $client->mobile }} </td>
                                </tr>
                                <tr>
                                    <th> Password</th>
                                    <td> {{ $client->password }} </td>
                                </tr>
                                <tr>
                                    <th> Address</th>
                                    <td> {{ $client->address }} </td>
                                </tr>
                                <tr>
                                    <th> Logo</th>
                                    <td>
                                        @if(isset($client->logo) && $client->logo->isNotEmpty())
                                            @foreach($client->logo as $logo)
                                                <div class="col-md-3"><img src="{{asset('clientLogo/'.$logo->logo)}}" width="200px" height="150px"><br></div>
                                                @endforeach
                                            @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $client->status==1?'Enabled':'Disabled' }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
