<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Category</b></div>
        <br/>
         <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-md-4 control-label">{{ 'Name' }} :</label>
    <div class="col-md-6">
     <input class="form-control" name="name" type="text" id="name" value="{{ isset($category->name) ? $category->name : old('name')}}" required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    <label for="code" class="col-md-4 control-label">{{ 'Code' }} :</label>
    <div class="col-md-6">
     <input class="form-control" name="code" type="text" id="code" value="{{ isset($category->code) ? $category->code : old('code')}}" >
    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-md-4 control-label">{{ 'Status' }} :</label>
    <div class="col-md-6">
     <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($category->status) && $category->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
          <div class="form-group">
                <div class="col-md-offset-4 col-md-6">
                      <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
                </div>
          </div>
     </div>
</div>
