<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <style type="text/css">
        /*        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap');*/

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        /*
        @font-face {
            font-family: "times new roman", serif;
            src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/times new roman.ttf);
        }

        @font-face {
            font-family: Arial, sans-serif;
            src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/ARIAL.TTF);
        }
*/

        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-size: 14px;
            font-family: 'Roboto', sans-serif;
            /*            font-family: "times new roman", serif;*/
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            position: relative;
        }

        page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }

        .text-center {
            text-align: center;
        }

        .table {
            width: 100%;
            max-width: 100%;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        table {
            background-color: transparent;
            border-spacing: 0;
            border-collapse: collapse;
        }

        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 4px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        th {
            text-align: left;
        }


        p {
            font-size: 13px;
            margin-bottom: 5px;
            margin-top: 0px;
        }

        .fnt-1 {
            margin: 10px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .table {
            margin: 0px;
        }

        .v-mdl {
            vertical-align: middle !important;
        }

        .v-top {
            vertical-align: top !important;
        }

        .no-vorder {
            border: none !important;
        }

        .bd-right-none {
            border-right: none !important;
        }

        .bd-left-none {
            border-left: none !important;
        }

        .invoice-wrapper {
            min-width: 680px;
        }

        .table-bordered,
        .table-bordered>tbody>tr>td,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>td,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border: 1px solid #000;
        }

        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border-top: 0;
            border: 1px solid #000;
        }

        .s-table thead tr th {
            vertical-align: middle;
        }

        .s-table thead tr th:nth-child(5),
        .s-table tbody tr td:nth-child(5) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(6),
        .s-table tbody tr td:nth-child(6) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(7),
        .s-table tbody tr td:nth-child(7) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(8),
        .s-table tbody tr td:nth-child(8) {
            width: 75px;
        }

        .s-table tbody.fnt-invoce tr td {
            font-size: 11px;
        }

        .igst-table {}

        .igst-table tbody tr th:nth-child(3) {
            width: 120px;
        }

        .igst-table tbody tr th {
            font-weight: 400;
        }

        .img-th {
            width: 150px;
        }

        .img-th img {
            max-width: 150px;
        }

        .first-table p {
            margin-bottom: 5px;
        }

        .first-table h4 {
            font-size: 14px;
            margin: 0;
        }

        .party-div {
            padding: 5px;
        }

        .fnt-invoce td {
            font-weight: 600;
        }

        .fnt-invoce td.nrml {
            font-weight: 400;
        }

        .first-table p span.caps {
            text-transform: uppercase;
        }

        .first-table h4 {
            color: #333;
        }

        .first-table h4.city {
            margin-right: 20px;
            display: inline-block;
            float: left;
        }

        .first-table h4.city strong {
            font-weight: 700;
            color: #000;
        }

        .first-table h3 {
            display: block;
            width: 100%;
            float: left;
            margin: 0 0 5px;
            font-size: 13px;
        }

        .right p {
            width: 50%;
            display: inline-block;
            margin-bottom: 5px;
            float: left;
        }

        .first-table tr h1 {
            margin: 5px 0px;
            text-transform: uppercase;
        }

        .top-table tr {
            padding: 2px 8px;
        }

        .top-table tr h2,
        .top-table tr h1 {
            margin: 0px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .top-table tr h2 {
            font-size: 15px;
            color: #606060;
        }

        .top-table tr td {
            border: none !important;
            padding: 0px !important;
        }

        .top-table tr:nth-child(1) {
            border: 1px solid #000 !important;
        }

        .top-table tr:nth-child(1) p {
            width: 33.33%;
            float: left;
            display: inline-block;
            font-size: 13px;
            text-align: left;
        }

        .top-table tr:nth-child(1) p.text-right {
            text-align: right;
        }

        .igst-table tbody>tr>td:nth-child(1) {
            width: 60%;
            float: left;
        }

        .igst-table tbody>tr>td {
            width: 40%;
            float: left;
        }

        .ten {
            border: 1px dashed #000;
            top: -8px;
            bottom: -8px;
            left: -8px;
            right: -8px;
            width: 80%;
            position: relative;
        }

        .logoWrapper {
            position: relative;
            display: flex;
            gap: 25px;
            justify-content: space-between;
            align-items: flex-end;
        }

        .stampLogo {
            position: absolute;
            bottom: 24%;
            right: 0;
        }

        .reposrtStamp {
            position: absolute;
            bottom: 80px;
            left: 110px;
        }

        .mainDiv {
            display: flex;
            border: 1px solid #000;
            border-left: 0;
            border-right: 0;
            padding: 15px 0;
            width: 100%;
            justify-content: space-around;
        }

        /*.mainDiv p {*/
        /*    font-weight: 700;*/
        /*}*/

    </style>

</head>

<body>

<page size="A4" id="area-cv-resume" width="1000" style="margin: 0 auto;display: block;position: relative;">

    <div class="stampLogo">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/stamp.jpeg" width="100px" alt="">
    </div>
    <div class="reposrtStamp">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/reposrtStanp.jpg" width="90px" alt="">
    </div>
    <div class="logoWrapper" style="margin-bottom: 5px;">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/newheader.jpeg" class="img-responsive" style="width: 100%">
    </div>


    <div class="patient-detail">
        <table style="border: 0;width: 100%;border-top: 2px solid #000;border-bottom: 2px solid #000;">
            <tbody class="toptbody">
            <tr>
                <td style="padding: 5px 10px 0 !important;width: 18%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Name</p>
                </td>

                <td style="padding: 5px 10px 0 !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:14px;">: MR RIYAJULLAH</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; vertical-align: top;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">SAMPLE COLLECTED AT</p>
                </td>

                <td rowspan="2" style="padding: 2px 10px !important;width: 0%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:14px;"></p>
                </td>
            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; vertical-align: top;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">REF. BY</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:14px;">: SELF</p>
                </td>
                <td rowspan="2" style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:14px;">15/D SITAPHALWADI, MOUNT ROAD, NEAR JAYANT
                        OIL MILL MAZGAON, MUMMBAI 400010 </p>
                </td>
            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; vertical-align: top;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">TEST ASKED </p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:14px;">: COVID-19</p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="tab-head" style="margin: 0px 0;width:100%; ">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <table style="border: 0 solid #000; width: 100%;margin: 0px 0px;">
                        <tbody>
                        <tr style="height: 30px;"></tr>
                        <tr>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <p style="margin: 0"><strong>TEST NAME</strong></p>
                            </td>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <p style="margin: 0"><strong>RESULT</strong></p>
                            </td>
                        </tr>
                        <tr style="border-top: 1px solid #000;border-bottom: 1px solid #000;">
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <h4 style="margin: 3px 0;">
                                    COVID-19 QUALITATIVE PCR</h4>
                            </td>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <h4 style="margin: 0px 0;">NOT DETECTED</h4>
                            </td>

                        </tr>
                        </tbody>
                    </table>

                    <table class="box-table" style="margin-top: 00px;width: 100%;">
                        <thead>
                        <tr>
                            <td style="border: 0;padding:2px 5px;width: 20%;">

                            </td>
                            <td style="border: 0;padding:2px 5px;font-weight:600;">
                                <h4 style="margin: 0px 0;"><strong>Interpretation-</strong></h4>
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="border: 1px solid #000">
                            <td style="border:1px solid #000;padding: 8px 5px;">
                                <strong>NOT DETECTED</strong>
                            </td>
                            <td style="border:1px solid #000;padding:8px 5px;">COVID-19 RNA was not detected in the patient specimen or less than detection limit</td>
                        </tr>

                        <tr style="border: 1px solid #000">
                            <td style="border:1px solid #000;padding:8px 5px;">
                                <strong>DETECTED</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">COVID-19 RNA was detected in the patient specimen </td>
                        </tr>

                        <tr style="border: 1px solid #000">
                            <td style="border:1px solid #000;padding:8px 5px;">
                                <strong>INCONCLUSIVE</strong>
                            </td>
                            <td style="border:1px solid #000;padding:8px 5px;">Target is inconclusive, Advised to send fresh specimen for recheck
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="box-table" style="margin: 10px 0 0; width: 100%;">
                        <thead>
                        <tr>
                            <td style="border: 0;padding:0px 5px;width: 100%;">
                                <h4 style="margin: 0"><strong>ICMR Registration No</strong> <span style="font-weight: 400"> THYRO001</span></h4>
                            </td>

                        </tr>
                        </thead>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>

        <table style="margin-top: 5px;">
            <tbody>
            <tr>
                <td>
                    <p>The performance of this test has been validated & evaluated by National Institute of Virology/ICMR</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Indications</strong>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>COVID-19 is an infectious disease caused by the virus strain "severe acute respiratory syndrome coronavirus 2" (SARS-CoV-2), <br>
                        Common signs of infection include respiratory symptoms,fever,cough,shortness of breath and breathing difficulties.
                        In more severe cases,infection can causes pneumonia ,severe acture respiratory syndrome and kidney failure. </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>Methodology</strong>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>COVID19 detection by polymerase chain reaction (PCR) is based on the amplification of specific regions of the SARS-CoV-2 .<br>
                        In real Time PCR the amplified product is detected via fluorescent dyes.
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p><strong>Clinical Significance</strong>
                    </p>
                </td>
            </tr>


            <tr>
                <td>
                    <p>Detection of COVID-19 RNA in patients with COVID-19 infection.
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p><strong>Limitation of the assay</strong>
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>1.Presence of PCR inhibitors may interfere with PCR amplification. <br>
                        2.Undetected result does not rule out the possibility of infection.Presence of inhibitors,mutations & insufficient
                        organism RNA can influence the result.
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p><strong>Disclaimer</strong>
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>1.This test is intended for use in conjunction with clinical presentation and other laboratory markers.<br>
                        2.Improper specimen collection, handling, storage and transportation may results in false negative results.<br>
                        3.The report represents only the specimen received in laboratory.

                    </p>
                </td>
            </tr>

            <tr style="height: 20px;"></tr>

            <tr>
                <td>
                    <p style="text-align: center;">~~ End of report ~~</p>
                </td>
            </tr>
            </tbody>
        </table>

    </div>
    ______________________________________________________________________________________________________
    <div class="footer" style="width: 100%">
        <table style="width: 100%;">
            <tbody class="toptbody">

            <tr>
                <td style="padding: 5px 10px 0 !important;width: 27%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Sample Collected on (SCT)</p>
                </td>

                <td style="padding: 5px 10px 0 !important;width: 10%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:14px;">: 07 Jan 2022 09:34</p>
                </td>

                <td rowspan="5" style="padding: 2px 10px !important;width: 57%;margin:0  1%;text-align: left;vertical-align: top;">
                    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/sign.jpeg" style="width: 100%;">
                </td>


            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 27%;margin:0  1%;text-align: left;vertical-align: top;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Sample Received on (SRT)</p>
                </td>

                <td style="padding: 2px 10px !important;width: 10%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:14px;">: 07 Jan 2022 10:04</p>
                </td>

            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 27%;margin:0  1%;text-align: left;vertical-align: top;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Report Released on (RRT) </p>
                </td>

                <td style="padding: 2px 10px !important;width: 10%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:14px;">: 07 Jan 2022 18:27
                    </p>
                </td>
            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 27%;margin:0  1%;text-align: left;vertical-align: top;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Sample Type </p>
                </td>

                <td style="padding: 2px 10px !important;width: 10%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0;font-weight:;font-size:14px;">: SWABS
                    </p>
                </td>
            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 27%;margin:0  1%;text-align: left;vertical-align: top;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Labcode </p>
                </td>

                <td style="padding: 2px 10px !important;width: 10%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:14px;">: 1909613853
                    </p>
                </td>
            </tr>
            <tr>
                <td style="padding: 2px 10px !important;width: 27%;margin:0  1%;text-align: left;vertical-align: top;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Barcode </p>
                </td>

                <td style="padding: 2px 10px !important;width: 23%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:14px;">: M1697426
                    </p>
                </td>
            </tr>
            </tbody>
        </table>
    </div>




</page>
<div style="page-break-after: always;"></div>

<page size="A4" id="area-cv-resume" width="1000" style="margin: 0 auto;display: block;position: relative;">

    <div class="topDiv" style="border: 1px solid #000;">
        <h3 style="text-align: center;">CUSTOMER DETAILS</h3>
        <p style="padding-left: 50px;margin-bottom: -2px"><strong>As declared in our data base</strong></p>

        <div class="mainDiv">
            <p><strong>Name:</strong> MR RIYAJULLAH </p>
            <p><strong>Age:</strong> 33 Y <strong>Sex:</strong> M </p>
            <p><strong>Mobile No:</strong> 9979107244</p>
        </div>
        <table style="width: 100%; ">

            <tbody class="toptbody">

            <tr>
                <td style="padding: 5px 10px !important;width: 30%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Barcodes/Sample_Type</p>
                </td>

                <td style="padding: 5px 10px !important;width: 70%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:14px;">: M1697426 (SWABS)</p>
                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 30%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Labcode</p>
                </td>

                <td style="padding: 5px 10px !important;width: 70%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:14px;">: 1909613853</p>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px 10px !important;width: 30%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Ref By</p>
                </td>

                <td style="padding: 5px 10px !important;width: 70%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:14px;">: SELF</p>
                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 30%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Sample_Type/Tests</p>
                </td>

                <td style="padding: 5px 10px !important;width: 70%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:14px;">: SWABS:COVID -19 </p>
                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 30%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Sample Collected At </p>
                </td>

                <td style="padding: 5px 10px !important;width: 70%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:14px;">: 15/ D SITAPHALWADI, MOUNT ROAD, NEAR JAYANT OIL MILL MAZGAON, <br> &nbsp;&nbsp;&nbsp;MUMBAI 400010</p>
                </td>

            </tr>
            <tr>
              <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
             <tr>
                 <td style="padding: 5px 10px !important;width: 30%;margin:0  1%;text-align: left;">
                     <p style="margin: 0px;font-weight:600;font-size:15px;">Sample Collected on (SCT) </p>
                 </td>

                 <td style="padding: 5px 10px !important;width: 70%;margin:0  1%;text-align: left;">
                     <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:14px;">: 07 Jan 2022 09:34</p>
                 </td>
             </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 30%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:600;font-size:15px;">Report Released on (RRT) </p>
                </td>

                <td style="padding: 5px 10px !important;width: 70%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:14px;">: 07 Jan 2022 18:27</p>
                </td>
            </tr>
            </tbody>
        </table>

        <div style="border-top: 1px solid #000; text-align: center;padding: 5px 0;">
            <p><strong>Thyrocare, D-37/1,MIDC,Turbhe,Navi Mumbai - 400703. | Phone:022 - 4125 2525 |www.thyrocare.com | info@thyrocare.com</strong></p>
        </div>
    </div>
</page>

<page size="A4" id="area-cv-resume" width="1000" style="margin: 0 auto;display: block;position: relative;">
    <div>
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/0003.jpg" width="100%" alt="">
    </div>
</page>
</body>

</html>
