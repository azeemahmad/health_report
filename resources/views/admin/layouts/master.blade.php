<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <title>{{ isset($page_title)?$page_title:'Health Test '.Auth::guard('web')->user()->name  }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="{{ asset("/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="{{asset('/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link href="{{ asset("/bower_components/jQuery-MultiSelect/jquery.multiselect.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet"
          type="text/css"/>
          <link rel="stylesheet" href="{{asset('/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">

          <link rel="stylesheet" href="{{asset('/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
          <!-- Bootstrap time Picker -->


{{--    <link rel="stylesheet" href="{{asset('/admin/css/jquery.timepicker.min.css')}}">--}}
    <link href="{{ asset("/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css")}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('/admin/css/select2.min.css')}}" rel="stylesheet"  type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('/bower_components/sweetalert/sweetalert.css')}}">
    <meta name="format-detection" content="telephone=no">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{asset('admin/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('admin/js/respond.min.js')}}"></script>
    <![endif]-->
    <meta name="google-site-verification" content="LvLN0uKrwmfQaBz-y8iJQvDDqdhGXaUNni1EMePwXZQ"/>
    <script src="{{asset('admin/js/Chart.min.js')}}" charset="utf-8"></script>
<style>

.panel-body {
    padding: 15px 0 10px 0;
}
input[type='radio'] {
    -webkit-appearance: none;
    width: 20px;
    height: 20px;
    border-radius: 50%;
    outline: none;
    border: 3px solid gray;
}

input[type='radio']:before {
    content: '';
    display: block;
    width: 60%;
    height: 60%;
    margin: 20% auto;
    border-radius: 50%;
}

input[type="radio"]:checked:before {
    background: green;

}

input[type="radio"]:checked {
    border-color:green;
}
.blinking{
    animation:blinkingText 3s infinite;
    font-size: 18px;
    font-weight: 700;
}
@keyframes blinkingText{
    0%{     color: red;    }
    49%{    color: white; }
    60%{    color: red; }
    99%{    color:white;  }
    100%{   color: darkgreen;    }
}
    .help-block{
        color: red !important;
    }
    .sticky-thead{
        display: none !important;
    }
   .table-header-td {
    background: pink;
    }
    .datepicker {
       z-index: 10000000 !important;
   }
</style>

</head>
<body class="skin-blue">
<div class="wrapper">
    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')
    <div class="content-wrapper">
        <section class="content-header">
        </section>
        <section class="">
            @include('admin.layouts.error')
            @yield('content')
        </section>
    </div>
    @include('admin.layouts.footer')
</div>
<script src="{{ asset ("/bower_components/jquery/dist/jquery.min.js") }}"></script>
<script src="{{ asset ("/bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/admin-lte/dist/js/adminlte.min.js") }}" type="text/javascript"></script>
{{--<script src="{{asset('admin/js/jquery.timepicker.min.js')}}"></script>--}}
<script src="{{ asset ("/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}" type="text/javascript"></script>
<script src="{{ asset ("/bower_components/jQuery-MultiSelect/jquery.multiselect.js") }}"></script>
<script src="{{asset('/bower_components/sweetalert/sweetalert.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/js/select2.min.js')}}"></script>
<script src="{{asset('/admin/js/multiple-select.min.js')}}"></script>
<script src="{{ asset ("/bower_components/jquery-table2excel/dist/jquery.table2excel.min.js") }}"></script>

<script src="{{asset('/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('/bower_components/moment/moment.js')}}"></script>

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('ckeditor/adapters/jquery.js') }}"></script>
<script>
    $('.ckeditor').ckeditor();
</script>
<script>
    $(".selectSearchClass").select2();

    $('.checkboxvalue').click(function () {
            var id = $(this).val();
            $('#quantity-'+id).focus();
    });

    $('.quantityKeyup').attr("type", 'text');

    $(document).on("input", ".quantityKeyup", function() {
            this.value = this.value.replace(/\D/g,'');
        });

   // $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        $('#finalSchemeForm').submit(function () {
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            var first_date = moment(start_date, "DD/MM/YYYY").format('YYYY-MM-DD');
            var last_date = moment(end_date, "DD/MM/YYYY").format('YYYY-MM-DD');
            $('#start_date').val(first_date);
            $('#end_date').val(last_date);
            return true;
        });
   // });

    $(document).on('focus', ':input', function() {
    $(this).attr('autocomplete', 'off');
  });
</script>
@yield('scripts')
@if(isset($usersChart) && $usersChart)
    {!! $usersChart->script() !!}
    {!! $usersChart1->script() !!}
    {!! $usersChart2->script() !!}
    {!! $usersChart3->script() !!}
@endif
@if(isset($division_wise_revenue_report_chart))

{!! $division_wise_revenue_report_chart->script() !!}
@endif

@if(isset($distributors_comparison_chart))

{!! $distributors_comparison_chart->script() !!}
@endif

</body>
</html>
