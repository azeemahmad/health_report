<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Welcome in  {{Auth::guard('web')->user()->name}} Panel
    </div>
    <!-- Default to the left -->
    <strong>Copyright © {{date('Y')}} <a href="{{url('/admin/home')}}" >Health Test Admin</a>.</strong> All rights reserved.
</footer>
   