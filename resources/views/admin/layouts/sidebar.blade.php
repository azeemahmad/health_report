<style>
    .logged-in {
        color: chartreuse;
        font-size: 26px
    }

    .logged-out {
        color: red;
        font-size: 26px
    }
</style>
<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                {{-- @if(Auth::guard('web')->user()->image)
                    <img src="{{env('LOCAL_URL').'images/profile_image'.'/'.Auth::guard('web')->user()->image}}"
                         class="img-circle"
                         alt="User Image"/>
                @else --}}
                <img src="{{ asset("/bower_components/admin-lte/dist/img/avatar5.png") }}" class="img-circle"
                     alt="User Image"/>
                {{-- @endif --}}
            </div>
            <div class="pull-left info">
                <?php


                //$connected = @fsockopen("www.google.com", 80); //website, port  (try 80 or 443)


                ?>
                <p>{{ ucwords(Auth::guard('web')->user()->name)}}


            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Admin-Panel</li>

            <li class="{{ (request()->is('admin/home')) ? 'active' : '' }}"><a href="{{url('/admin/home')}}"><i
                            class="fa fa-home"></i> DashBoard</a></li>
            <li class="{{ (request()->is('admin/client')) ? 'active' : '' }}"><a href="{{url('/admin/client')}}"><i
                            class="fa fa-pencil"></i> Customer Data</a></li>

            <li class="{{ (request()->is('admin/retailers')) ? 'active' : '' }}"><a href="{{url('/admin/retailers')}}"><i
                            class="fa fa-users"></i> Retailers</a></li>
            <li class="{{ (request()->is('admin/sales-persons')) ? 'active' : '' }}"><a href="{{url('/admin/sales-persons')}}"><i
                            class="fa fa-book"></i> Sales Person data</a></li>

            <li class="{{ (request()->is('admin/categories')) ? 'active' : '' }}"><a href="{{url('/admin/categories')}}"><i
                            class="fa fa-square"></i> Category</a></li>
            <li class="{{ (request()->is('admin/accounts')) ? 'active' : '' }}"><a href="{{url('/admin/accounts')}}"><i
                            class="fa fa-inr"></i> Account</a></li>
            <li class="{{ (request()->is('admin/client-details')) ? 'active' : '' }}"><a href="{{url('/admin/client-details')}}"><i
                            class="fa fa-pencil"></i> AMR Client Data</a></li>


        </ul>
    </section>
</aside>