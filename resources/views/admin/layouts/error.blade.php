    @if (session('flash_message'))
    <span style="display: block;" class="alert alert-success">
    {{ session('flash_message') }}
     </span>
@endif
@if (session('error_message'))
    <span style="display: block;" class="alert alert-danger">
    {!! session('error_message') !!}
     </span>
@endif

@if (session('inventory_error_message'))
    <span style="display: block;" class="alert alert-info">
    {!! session('inventory_error_message') !!}
     </span>
@endif