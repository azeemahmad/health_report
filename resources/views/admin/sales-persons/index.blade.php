@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="panel panel-success">
            <div class="panel-heading">Salespersons</div>
            <div class="panel-body">
                <div class="col-md-12 page-action text-left">
                    <a href="{{ url('/admin/sales-persons/create') }}" class="btn btn-success btn-sm"
                       title="Add New SalesPerson">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                </div>
                <div class="col-md-12">
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/sales-persons', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="data-table">
                            <thead>
                            <tr>
                                <th>Sr.no</th>
                                <th>Sales Person Name</th>
                                <th>Mobile</th>
                                <th>Is Active
                                </th>
                                <th class="text-center">Actions
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($salespersons as $key => $item)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $item->sales_person_name }}</td>
                                    <td>{{ $item->mobile }}</td>
                                    <td>{{ $item->is_active==1?'ACTIVE':"IN-ACTIVE" }}</td>

                                    <td class="text-center">
                                        <a href="{{ url('/admin/sales-persons/' . $item->id) }}"
                                           title="View SalesPerson">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                   aria-hidden="true"></i> View
                                            </button>
                                        </a>

                                        <a href="{{ url('/admin/sales-persons/' . $item->id . '/edit') }}"
                                           title="Edit SalesPerson">
                                            <button class="btn btn-primary btn-xs"><i
                                                        class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                Edit
                                            </button>
                                        </a>

                                        <form method="POST"
                                              action="{{ url('admin/salespersons' . '/' . $item->id) }}"
                                              accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-xs"
                                                    title="Delete SalesPerson"
                                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                        class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $salespersons->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
