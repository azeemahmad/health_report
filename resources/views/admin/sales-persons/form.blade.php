<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>SalesPerson</b></div>
        <br/>
        <div class="form-group {{ $errors->has('sales_person_name') ? 'has-error' : ''}}">
            <label for="sales_person_name" class="col-md-4 control-label">{{ 'Sales Person Name' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="sales_person_name" type="text" id="sales_person_name"
                       value="{{ isset($salesperson->sales_person_name) ? $salesperson->sales_person_name : old('sales_person_name')}}"
                       required>
                {!! $errors->first('sales_person_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
            <label for="mobile" class="col-md-4 control-label">{{ 'Mobile' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="mobile" type="text" id="mobile"
                       value="{{ isset($salesperson->mobile) ? $salesperson->mobile : old('mobile')}}">
                {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('is_active') ? 'has-error' : ''}}">
            <label for="is_active" class="col-md-4 control-label">{{ 'Is Active' }} :</label>
            <div class="col-md-6">
                <select name="is_active" class="form-control" id="is_active">
                    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($salesperson->is_active) && $salesperson->is_active == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('is_active', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
