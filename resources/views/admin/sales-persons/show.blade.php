@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <div class="panel panel-success">
                    <div class="panel-heading">SalesPerson {{ $salesperson->id }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/sales-persons') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>

                            <a href="{{ url('/admin/sales-persons/' . $salesperson->id . '/edit') }}"
                               title="Edit SalesPerson">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                          aria-hidden="true"></i> Edit
                                </button>
                            </a>

                            <form method="POST" action="{{ url('admin/salespersons' . '/' . $salesperson->id) }}"
                                  accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-xs" title="Delete SalesPerson"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                                 aria-hidden="true"></i>
                                    Delete
                                </button>
                            </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $salesperson->id }}</td>
                                </tr>
                                <tr>
                                    <th> Sales Person Name</th>
                                    <td> {{ $salesperson->sales_person_name }} </td>
                                </tr>
                                <tr>
                                    <th> Is Active</th>
                                    <td> {{ $salesperson->is_active }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
