<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <style type="text/css">
        /*        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap');*/

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        /*
        @font-face {
            font-family: "times new roman", serif;
            src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/times new roman.ttf);
        }

        @font-face {
            font-family: Arial, sans-serif;
            src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/ARIAL.TTF);
        }
*/

        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-size: 14px;
            font-family: 'Roboto', sans-serif;
            /*            font-family: "times new roman", serif;*/
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }

        page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }

        .text-center {
            text-align: center;
        }

        .table {
            width: 100%;
            max-width: 100%;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        table {
            background-color: transparent;
            border-spacing: 0;
            border-collapse: collapse;
        }

        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 4px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        th {
            text-align: left;
        }


        p {
            font-size: 13px;
            margin-bottom: 5px;
            margin-top: 0px;
            position: relative;
            z-index: 99;
        }

        .fnt-1 {
            margin: 10px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .table {
            margin: 0px;
        }

        .v-mdl {
            vertical-align: middle !important;
        }

        .v-top {
            vertical-align: top !important;
        }

        .no-vorder {
            border: none !important;
        }

        .bd-right-none {
            border-right: none !important;
        }

        .bd-left-none {
            border-left: none !important;
        }

        .invoice-wrapper {
            min-width: 680px;
        }

        .table-bordered,
        .table-bordered>tbody>tr>td,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>td,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border: 1px solid #000;
        }

        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border-top: 0;
            border: 1px solid #000;
        }

        .s-table thead tr th {
            vertical-align: middle;
        }

        .s-table thead tr th:nth-child(5),
        .s-table tbody tr td:nth-child(5) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(6),
        .s-table tbody tr td:nth-child(6) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(7),
        .s-table tbody tr td:nth-child(7) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(8),
        .s-table tbody tr td:nth-child(8) {
            width: 75px;
        }

        .s-table tbody.fnt-invoce tr td {
            font-size: 11px;
        }

        .igst-table {}

        .igst-table tbody tr th:nth-child(3) {
            width: 120px;
        }

        .igst-table tbody tr th {
            font-weight: 400;
        }

        .img-th {
            width: 150px;
        }

        .img-th img {
            max-width: 150px;
        }

        .first-table p {
            margin-bottom: 5px;
        }

        .first-table h4 {
            font-size: 14px;
            margin: 0;
        }

        .party-div {
            padding: 5px;
        }

        .fnt-invoce td {
            font-weight: 600;
        }

        .fnt-invoce td.nrml {
            font-weight: 400;
        }

        .first-table p span.caps {
            text-transform: uppercase;
        }

        .first-table h4 {
            color: #333;
        }

        .first-table h4.city {
            margin-right: 20px;
            display: inline-block;
            float: left;
        }

        .first-table h4.city strong {
            font-weight: 700;
            color: #000;
        }

        .first-table h3 {
            display: block;
            width: 100%;
            float: left;
            margin: 0 0 5px;
            font-size: 13px;
        }

        .right p {
            width: 50%;
            display: inline-block;
            margin-bottom: 5px;
            float: left;
        }

        .first-table tr h1 {
            margin: 5px 0px;
            text-transform: uppercase;
        }

        .top-table tr {
            padding: 2px 8px;
        }

        .top-table tr h2,
        .top-table tr h1 {
            margin: 0px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .top-table tr h2 {
            font-size: 15px;
            color: #606060;
        }

        .top-table tr td {
            border: none !important;
            padding: 0px !important;
        }

        .top-table tr:nth-child(1) {
            border: 1px solid #000 !important;
        }

        .top-table tr:nth-child(1) p {
            width: 33.33%;
            float: left;
            display: inline-block;
            font-size: 13px;
            text-align: left;
        }

        .top-table tr:nth-child(1) p.text-right {
            text-align: right;
        }

        .igst-table tbody>tr>td:nth-child(1) {
            width: 60%;
            float: left;
        }

        .igst-table tbody>tr>td {
            width: 40%;
            float: left;
        }

        .ten {
            border: 1px dashed #000;
            top: -8px;
            bottom: -8px;
            left: -8px;
            right: -8px;
            width: 80%;
            position: relative;
        }

        .logoWrapper {
            position: relative;
            display: flex;
            gap: 25px;
            justify-content: space-between;
            align-items: flex-end;
        }

        .barcode {
            position: absolute;
            right: 0;
            top: 33%;
            z-index: 99;
        }

        p.listItem {
            position: relative;
            padding-left: 20px;
        }

        p.listItem .icon {
            position: absolute;
            left: 0px;
            top: 0;
        }

        p.listItem .icon img {
            width: 8px !important;
        }
        @media print {
            page[size="A4"] {
                width: 100%;
                /*height: 100%;*/
            }
        }
    </style>

</head>

<body>
<page size="A4" id="area-cv-resume" width="1000" style="margin: 0 auto;display: block;position: relative;">

    <div class="barcode">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/barcode-2.jpg" class="img-responsive" style="width: 135px;">
    </div>

    <div class="logoWrapper" style="margin-bottom: 5px;">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/header.jpeg" class="img-responsive" style="width: 100%">
    </div>

    <div class="patient-detail" style="padding: 0 15px;">
        <table style=";width: 100%;border: 1px solid #000;">
            <tbody class="toptbody">
            <tr>
                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:400;font-size:12px;">Patient Name</p>
                </td>

                <td style="padding: 5px 10px !important;width: 34%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0px;font-weight:600;font-size:12px;">: Mr ROSHAN KUMAR</p>
                </td>

                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;    border-left: 1px solid;">
                    <p style="margin: 0px;font-weight:400;font-size:12px;">Age / Sex</p>
                </td>

                <td style="padding: 5px 10px !important;width: 34%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:12px;">: 27 Yrs /MALE &nbsp;&nbsp; DOB : 03/06/1994</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;vertical-align: top;">
                    <p style="margin: 0px;font-weight:400;font-size:12px;">Patient Id </p>
                </td>

                <td style="padding: 5px 10px !important;width: 34%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:12px;">: 20539 &nbsp;&nbsp; &nbsp;&nbsp; Nationality: Indian</p>
                </td>

                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;border-left: 1px solid;">
                    <p style="margin: 0px;font-weight:400;font-size:12px;">Registration On</p>
                </td>

                <td style="padding: 5px 10px !important;width: 34%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:12px;">: 06/11/2021 11:25:57AM</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;vertical-align: top;">
                    <p style="margin: 0px;font-weight:400;font-size:12px;">Refer By</p>
                </td>

                <td style="padding: 5px 10px !important;width: 34%;margin:0  1%;text-align: left;vertical-align: top;">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:12px;">: TRAVEL</p>
                </td>

                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;vertical-align: top;border-left: 1px solid;  ">
                    <p style="margin: 0px;font-weight:400;font-size:12px;">Collected On</p>
                </td>

                <td style="padding: 5px 10px !important;width: 34%;margin:0  1%;text-align: left;vertical-align: top  ">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:12px;">: 06/11/2021 11:32:16AM</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:400;font-size:12px;">Mobile No</p>
                </td>

                <td style="padding: 5px 10px !important;width: 34%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0px;font-weight:400;font-size:12px;">: 7392987252 &nbsp;&nbsp; <span style="font-weight: 600">Passport No.:P0229808</span></p>
                </td>

                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;border-left: 1px solid; ">
                    <p style="margin: 0px;font-weight:600;font-size:12px;">Reported On</p>
                </td>

                <td style="padding: 5px 10px !important;width: 34%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:400;font-size:12px;">: 06/11/2021 03:10:14PM</p>

                </td>
            </tr>


            </tbody>
        </table>
    </div>

    <div class="tab-head" style="margin: 0px 0;width:100%;padding: 0 15px; ">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <table style="border: 0 solid #000; width: 100%;margin: 0px 0px;">
                        <tbody>
                        <tr>
                            <td style="padding:0px !important;margin:0  auto;text-align: center; ">
                                <h4 style="margin: 5px 0 0;font-size: 16px;padding: 5px 0px 0; border-top: 1px solid #000">MOLECULAR BIOLOGY REPORT</h4>

                                <h4 style="margin: 0px 0 4px;font-size: 14px;padding: 5px 0px;">
                                    <span style="border-bottom: 1px solid #000;font-weight: 700">SARS-COV-2 REAL TIME PCR</span>
                                </h4>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="box-table" style="margin-top: 00px;width: 100%;">

                        <tbody>
                        <tr style="border: 0">
                            <td style="border:0;padding:2px 5px;">
                                <strong>Test Principle</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">SARS-COV-2 REAL TIME PCR (QUALITATIVE)</td>
                        </tr>

                        <tr style="border: 0">
                            <td style="border:0;padding:2px 5px;">
                                <strong>Equipment</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">Bio Rad CFX 96.</td>
                        </tr>

                        <tr style="border: 0">
                            <td style="border:0;padding:2px 5px;">
                                <strong>Specimen Type</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">NASOPHARYNGEAL/OROPHARYNGEAL SWAB</td>
                        </tr>

                        <tr style="border: 0">
                            <td style="border:0;padding:2px 5px;">
                                <strong>ICMR Registration No: NELUCKDICLUP</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;"></td>
                        </tr>

                        </tbody>
                    </table>

                    <table class="box-table" style="margin: 0px 0; width: 100%;">
                        <thead>
                        <tr>
                            <td style="border: 0;padding:2px 5px;width: 50%;">
                                Results:
                            </td>
                            <td style="border: 0;padding:2px 5px;font-weight:600;"></td>
                            <td style="border: 0;padding:2px 5px;font-weight:600;"></td>
                        </tr>
                        </thead>
                    </table>

                    <table class="box-table" style="margin-top: 0px;width: 84%;">
                        <thead>
                        <tr style="border: 1px solid #000;">
                            <td style="border: 1px solid #000;;padding:2px 20px;font-weight:600;width: 50%;">Target</td>
                            <td style="border: 1px solid #000;;padding:2px 20px;font-weight:600;">Result</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="border: 1px solid #000;">
                            <td style="border:1px solid #000;;padding:2px 20px;">
                                SARS- Cov-2
                            </td>
                            <td style="border:1px solid #000;;padding:2px 20px;">NEGATIVE</td>
                        </tr>

                        <tr style="border: 1px solid #000;">
                            <td style="border:1px solid #000;;padding:2px 20px;">
                                Orf1ab & N gene
                            </td>
                            <td style="border:1px solid #000;;padding:2px 20px;">NEGATIVE</td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>

        <table style="margin-top: 0px;">
            <tbody>
            <tr>
                <td>
                    <h4 style="margin: 5px 0 0"><strong>Final Result : NEGATIVE For SARS-CoV-2:</strong></h4>
                    <p>This result is interpreted based on three gene analysis of Orf1ab & N gene for detection of SARS-CoV-2</p>
                    <p><span style="display: inline-block; border-bottom: 1px solid #000;"><strong>Test Methodlogy:</strong></span></p>
                    <p>The test is intended to detect the presence of SARS-Cov-2 in the patient sample This test utilises a qualitative multiplexed Taqman based real
                        time PCR for amplification of specific region of the genome of this pathogen. These genes were amplifies & detected as per manufacturer's
                        instructions.</p>
                    <h4 style="margin: 5px 0"><strong>Note</strong></h4>
                    <p class="listItem">
                                <span class="icon">
                                    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/listicon.jpg" style="width: 15px;"></span>
                        Negative results does not rule out the possibility of COVID-19 infection espicially if clinical suspecion is high. The combination
                        of clinical symptoms, typical CT imaging features, and dynamic changes must be considered and test repeated after few days.
                        Presence of inhibitors, mutations & insufficient organism RNA can influence the result.
                    </p>
                    <p class="listItem">
                                <span class="icon">
                                    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/listicon.jpg" style="width: 15px;"></span>
                        Bordeline positive cases (CT>33) may give variable results on repeat testing. The possible reasons could be the variations in kits and
                        instruments used.
                    </p>
                    <p class="listItem">
                                <span class="icon">
                                    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/listicon.jpg" style="width: 15px;"></span>
                        Covid-19 Test conducted as per ICMR/GOI Guidelines.
                    </p>
                    <p class="listItem">
                                <span class="icon">
                                    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/listicon.jpg" style="width: 15px;"></span>
                        Kindly consult referring Physician/Authorized Govt. hospital for appropriate follow-up.
                    </p>
                    <p class="listItem">
                                <span class="icon">
                                    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/listicon.jpg" style="width: 15px;"></span>
                        Inconclusive result specimen will be repeated.
                    </p>
                    <p class="listItem">
                                <span class="icon">
                                    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/listicon.jpg" style="width: 15px;"></span>
                        Test is performed using ICMR approved kit.
                    </p>
                    <p class="listItem">
                        [THIS TEST IS CONDUCTED AT MOLECULAR LAB DEPARTMENT OF NEW LUCKNOW DIAGNOSTIC CENTRE -LUCKNOW</p>
                </td>
            </tr>
            <tr>
                <td>
                    <h4 style="margin: 5px 0 5px"><strong>Limitation:</strong></h4>
                    <p class="listItem">
                                <span class="icon">
                                    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/listicon.jpg" style="width: 15px;"></span>
                        Positive results do not rule out bacterial infections or co-infection with other viruses.
                    </p>
                    <p class="listItem">
                                <span class="icon">
                                    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/listicon.jpg" style="width: 15px;"></span>
                        Optimum specimen types and timing for peak viral levels during infetions caused by SARS-COV-2 have not been determined.
                    </p>
                    <p class="listItem">
                        Collection of multiple specimen (type and time points) from the same patient may be necessary to detect the virus</p>
                    <p class="listItem">All reports to be clinically corelated</p>
                </td>
            </tr>

            <tr style="height: 0px;"></tr>

            </tbody>
        </table>

    </div>


    <div class="footer" style="width: 100%;margin-top: 60px;">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <div class="" style="display: flex;justify-content: space-between;">
                        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/footer.jpeg" style="width: 100%;">
                    </div>

                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/last/watermark.jpeg" class="img-responsive" style="        position: absolute;
    bottom: 20%;
    left: 58%;
    transform: translateX(-50%);
    width: 600px;
    opacity: .9;">

</page>
</body>

</html>
