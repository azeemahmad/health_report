<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$client->passport_no}}</title>

    <style type="text/css">
        /*        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap');*/

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        /*
                @font-face {
                    font-family: "times new roman", serif;
                    src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/times new roman.ttf);
                }

                @font-face {
                    font-family: Arial, sans-serif;
                    src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/ARIAL.TTF);
                }
        */

        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-size: 14px;
            font-family: 'Roboto', sans-serif;
            /*            font-family: "times new roman", serif;*/
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }

        page[size="A4"] {
            width: 23cm;
            height: 27.7cm;
        }

        .text-center {
            text-align: center;
        }

        .table {
            width: 100%;
            max-width: 100%;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        table {
            background-color: transparent;
            border-spacing: 0;
            border-collapse: collapse;
        }

        .table > tbody > tr > td,
        .table > tbody > tr > th,
        .table > tfoot > tr > td,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > thead > tr > th {
            padding: 4px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        th {
            text-align: left;
        }


        p {
            font-size: 13px;
            margin-bottom: 5px;
            margin-top: 5px;
        }

        .fnt-1 {
            margin: 10px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .table {
            margin: 0px;
        }

        .v-mdl {
            vertical-align: middle !important;
        }

        .v-top {
            vertical-align: top !important;
        }

        .no-vorder {
            border: none !important;
        }

        .bd-right-none {
            border-right: none !important;
        }

        .bd-left-none {
            border-left: none !important;
        }

        .invoice-wrapper {
            min-width: 680px;
        }

        .table-bordered,
        .table-bordered > tbody > tr > td,
        .table-bordered > tbody > tr > th,
        .table-bordered > tfoot > tr > td,
        .table-bordered > tfoot > tr > th,
        .table-bordered > thead > tr > td,
        .table-bordered > thead > tr > th {
            border: 1px solid #000;
        }

        .table-bordered > thead > tr > td,
        .table-bordered > thead > tr > th {
            border-top: 0;
            border: 1px solid #000;
        }

        .s-table thead tr th {
            vertical-align: middle;
        }

        .s-table thead tr th:nth-child(5),
        .s-table tbody tr td:nth-child(5) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(6),
        .s-table tbody tr td:nth-child(6) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(7),
        .s-table tbody tr td:nth-child(7) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(8),
        .s-table tbody tr td:nth-child(8) {
            width: 75px;
        }

        .s-table tbody.fnt-invoce tr td {
            font-size: 11px;
        }

        .igst-table {
        }

        .igst-table tbody tr th:nth-child(3) {
            width: 120px;
        }

        .igst-table tbody tr th {
            font-weight: 400;
        }

        .img-th {
            width: 150px;
        }

        .img-th img {
            max-width: 150px;
        }

        .first-table p {
            margin-bottom: 5px;
        }

        .first-table h4 {
            font-size: 14px;
            margin: 0;
        }

        .party-div {
            padding: 5px;
        }

        .fnt-invoce td {
            font-weight: 600;
        }

        .fnt-invoce td.nrml {
            font-weight: 400;
        }

        .first-table p span.caps {
            text-transform: uppercase;
        }

        .first-table h4 {
            color: #333;
        }

        .first-table h4.city {
            margin-right: 20px;
            display: inline-block;
            float: left;
        }

        .first-table h4.city strong {
            font-weight: 700;
            color: #000;
        }

        .first-table h3 {
            display: block;
            width: 100%;
            float: left;
            margin: 0 0 5px;
            font-size: 13px;
        }

        .right p {
            width: 50%;
            display: inline-block;
            margin-bottom: 5px;
            float: left;
        }

        .first-table tr h1 {
            margin: 5px 0px;
            text-transform: uppercase;
        }

        .top-table tr {
            padding: 2px 8px;
        }

        .top-table tr h2,
        .top-table tr h1 {
            margin: 0px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .top-table tr h2 {
            font-size: 15px;
            color: #606060;
        }

        .top-table tr td {
            border: none !important;
            padding: 0px !important;
        }

        .top-table tr:nth-child(1) {
            border: 1px solid #000 !important;
        }

        .top-table tr:nth-child(1) p {
            width: 33.33%;
            float: left;
            display: inline-block;
            font-size: 13px;
            text-align: left;
        }

        .top-table tr:nth-child(1) p.text-right {
            text-align: right;
        }

        .igst-table tbody > tr > td:nth-child(1) {
            width: 60%;
            float: left;
        }

        .igst-table tbody > tr > td {
            width: 40%;
            float: left;
        }

        .ten {
            border: 1px dashed #000;
            top: -8px;
            bottom: -8px;
            left: -8px;
            right: -8px;
            width: 80%;
            position: relative;
        }

        .logoWrapper {
            position: relative;
        }

        .rightLogo {
            position: absolute;
            right: -23px;
            top: 13px;
        }

        .box-table td {
            font-size: 13px;
            border: none;
            border-top: 2px solid #000;
            border-bottom: 2px solid #000;
        }

    </style>

</head>

<body>
<page size="A4" id="area-cv-resume" width="1000" style="margin: 0 auto;display: block;position: relative;">

    <div class="logoWrapper" style="margin-bottom: 10px;">
        <img src="{{asset('dubai_header.jpeg')}}" style="width: 100%" alt="">
        <div class="rightLogo">
                <img src="data:image/png;base64,{!! DNS1D::getBarcodePNG($client->dubai_lab_no, 'UPCA')  !!}" alt="barcode"  style="width: 175px; height: 25px;" />
        </div>
    </div>


    <div class="patient-detail">
        <table style="border: 0;width: 100%;border-top: 2px solid #000;border-bottom: 2px solid #000;">
            <tbody>
            <tr>
                <td style="padding: 1px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Name </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:600;font-size:14px;">: {{$client->name}}</p>
                </td>

                <td style="padding: 1px 10px !important;width: 17%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">File. No. </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:14px;">: DML02-{{$client->file_no}}</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 1px 10px !important;width: 14%;margin:0  1%;text-align: left; vertical-align: top;">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">DOB/Gender </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:14px;">
                        : {{str_replace('/','-',$client->dob)}} ({{$client->full_age}})</p>
                </td>

                <td style="padding: 1px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Referral Doctor </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:14px;">: HHDFVVIP</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 1px 10px !important;width: 14%;margin:0  1%;text-align: left;vertical-align: top ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Lab No. </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left; vertical-align: top ">
                    <p class="bd-btm" style="margin: 0px;font-weight:600;font-size:14px;">
                        : {{$client->dubai_lab_no}}</p>
                </td>

                <td style="padding: 1px 10px !important;width: 14%;margin:0  1%;text-align: left;vertical-align: top  ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Referral Clinic </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left;vertical-align: top  ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:14px;">: HEALTHHUB DAY SURGERY &nbsp;&nbsp;VVIP
                        4Hr (DUBAI FESTIVAL CITY
                        &nbsp;&nbsp;DRIVETHRU)
                    </p>

                </td>
            </tr>

            <tr>
                <td style="padding: 1px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Request Date </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:600;font-size:14px;">
                        : {{date('d-m-Y H:i:s',strtotime($client->dubai_request_date))}}</p>
                </td>

                <td style="padding: 1px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Clinic File No </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:14px;">:
                        HFC{{$client->clinic_file_no}}</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 1px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Insurance </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:600;font-size:14px;">: No</p>
                </td>

                <td style="padding: 1px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Referral Lab No </p>
                </td>

                <td style="padding: 1px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:14px;">: </p>

                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="tab-head" style="margin: 0px 0;width:100%; ">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <table style="border: 0 solid #000; width: 100%;margin: 0px 0px;">
                        <tbody>
                        <tr>
                            <td style="padding:0px !important;margin:0  auto;text-align: center; ">
                                <h2 style="margin: 0px;font-size: 18px;padding: 5px 0px;">MOLECULAR
                                    BIOLOGY/GENETICS</h2>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="box-table" style="margin-top: 00px;width: 100%;">
                        <thead>
                        <tr>
                            <td style="padding:2px 5px;font-weight:600;width: 50%;">Test Name</td>
                            <td style="padding:2px 5px;font-weight:600;">Result</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="border: 0;">
                                <h2 style="font-weight: 600;font-size: 13px;display: inline-block;border-bottom: 2px solid #000;margin: 5px 0;">
                                    *NOVEL CORONA VIRUS -SARS COV-2 (COVID19)</h2>
                            </td>
                        </tr>
                        <tr>
                            <td style="border:0;padding:2px 5px;">
                                <strong>Sample Type:</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">VTM with Swab (Nasopharyngeal)</td>
                        </tr>

                        <tr>
                            <td style="border:0;padding:2px 5px;">
                                <strong>Novel Corona Virus -SARS COV-2 (COVID19)</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;"><strong>Not Detected</strong></td>
                        </tr>

                        <tr>
                            <td style="border:0;padding:2px 5px;">
                                <strong>Not Detected (Negative)</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;text-align: justify">This test is a qualitative PCR test. Not detected
                                indicates that SARS-CoV-2 RNA is
                                either not present in the specimen or is present at a concentration below the assays
                                lower limit of detection. This result may be influenced by the stage of the infection
                                and the quality of the specimen collected for testing. Repeat test if deemed
                                necessary after 72 hours.
                            </td>
                        </tr>

                        <tr>
                            <td style="border:0;padding:2px 5px;">
                                <strong>Additional Patient Information</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">
                                <strong>Passport No : {{$client->passport_no}}</strong>

                            </td>
                        </tr>


                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>

        <table>
            <tr>
                <td>
                    <p style="margin:0px;">Reference Range: Not Detected</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="margin:0px;">Methodology: RT PCR (NAAT) </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Remarks:</p>
                    <p style="text-align: justify">REAL TIME PCR COVID-19 Detection Kit is a real time PCR based in-vitro diagnostic medical device
                        that is designed to detect the infection of Novel Corona
                        Virus -SARS COV-2 (COVID19) through simultaneous examination of the ORF1ab and N-gene using the
                        nucleic acid extracted from oropharyngeal or
                        nasopharyngeal smears. </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Limit of Detection (Analytical Sensitivity) is 10 copy / Reaction</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="text-align: justify">Kindly note all detected cases are to be immediately notified to the local regulatory health
                        authorities & requires clinical correlation and further evaluation as
                        indicated. Interpretation of results should be correlated with patient history and clinical
                        presentation. </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="text-align: justify"><strong>Note:</strong> Reporting of test should be in-line with DHA rules and regulations for
                        COVID-19 testing. <strong>Ref:- NCEMA guidelines for COVID 19 Reporting</strong></p>
                </td>
            </tr>

            <tr>
                <td>
                    <h2 style="text-align: left;padding-left: 30px;font-weight: 400;font-size: 14px;font-style: italic;margin: 0;">
                        Sample Type : Swab/SPUTUM/VTM/BAL</h2>
                </td>
            </tr>


            <tr>
                <td>
                    <h2 style="text-align: center;font-size: 14px;margin: 0;">---------------- End Of Report
                        ----------------</h2>
                </td>
            </tr>

        </table>

    </div>


    <div class="footer" style="width: 100%">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <div class="" style="display: flex;justify-content: space-between;">
                        <img src="{{asset('dubai_logo.jpeg')}}" style="width: 150px;">

                        <img src="{{url('Qrcode/'.$client->name.'-'.$client->passport_no.'.png')}}"
                             style="width: 100px;">
                    </div>

                </td>
            </tr>
            </tbody>
        </table>


        <table style="width: 100%;margin: 10px 0;border-top: 2px solid;border-bottom: 2px solid;padding: 20px 0;display: block;">
            <tbody>

            <tr>
                <td style="width:21%;text-align: left;position: absolute;bottom: 6%;padding: 0px 8px;">
                    <strong>Dr. Sreerenjini K.C.</strong><br>
                    <p>Laboratory Director</p>
                    <p>DHA/LS/2992011/246180</p>
                </td>

                <td style="width:56%;text-align: center;">
                    <p>These tests are accredited under ISO 15189:2012 unless specified by (*)
                        Sample processed on the same day of receipt unless specified otherwise.
                        Test results pertains only the sample tested and to be correlated with clinical history.<br>
                        Reference range related to Age/Gender.</p>
                    <table style="width: 100%;">
                        <tr>
                            <td style="font-size:12px;">H/L Collected On
                                : {{date('d-m-Y H:i:s',strtotime($client->dubai_collected_on))}}</td>
                            <td style="font-size:12px;">Received On
                                : {{date('d-m-Y H:i:s',strtotime($client->dubai_recieved_od))}}</td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;">Authenticated On
                                : {{date('d-m-Y H:i:s',strtotime($client->dunai_authenticated_on))}}</td>
                            <td style="font-size:12px;">Released On
                                : {{date('d-m-Y H:i:s',strtotime($client->dubai_released_on))}}</td>
                        </tr>
                    </table>

                </td>

                <td style="width:23%;text-align: left;padding: 0px 8px;">
                    <img src="{{asset('dubai_sign.jpeg')}}" class=""><br>
                    <strong>Dr. Vibhav Vishwa Mohan</strong><br>
                    <p>Specialist Clinical Pathology</p>
                    <p>DHA-P-0121857</p>
                </td>

            </tr>
            </tbody>
        </table>

        <table>
            <tbody>
            <tr>
                <td>
                    <img src="{{asset('dubai_footer.jpeg')}}" style="width: 80%;margin: 0 auto;display: block;">
                </td>
                <td class="pull-right" style="padding-top: 54px;">Page 1 of 1</td>
            </tr>

            </tbody>
        </table>
    </div>


</page>
{{--<script src="{{ asset ("/bower_components/jquery/dist/jquery.min.js") }}"></script>--}}
{{--<script src="{{ asset ("/bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>--}}
{{--<script src="{{ asset ("/bower_components/admin-lte/dist/js/adminlte.min.js") }}" type="text/javascript"></script>--}}
{{--<script>--}}
{{--    $(document).ready(function(){--}}
{{--        window.print();--}}
{{--    });--}}
{{--</script>--}}
</body>

</html>
