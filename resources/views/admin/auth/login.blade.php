<!DOCTYPE html>
<html>
<style>
    .help-block{
        color:red !important;
    }
    .login-page, .register-page {
        background: url({{asset('Desktop-Homepage.png')}}) 0% 0% / cover no-repeat !important;
    }

    @media(max-width:768px){
        .login-page, .register-page {
            background: url({{asset('mobile-Homepage.png')}}) 0% 0% / cover no-repeat !important;
        }
        .login-box, .register-box {
            padding-top: 35%;
        }
    }

    body {
    padding-top: 90px;
}
.panel-login {
	border-color: #ccc;
	-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #fff;
	border-color: #fff;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 15px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	color: #029f5b;
	font-size: 18px;
}
.panel-login>.panel-heading hr{
	margin-top: 10px;
	margin-bottom: 0px;
	clear: both;
	border: 0;
	height: 1px;
	background-image: -webkit-linear-gradient(left,rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.15),rgba(0, 0, 0, 0));
	background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -ms-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
	background-image: -o-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.15),rgba(0,0,0,0));
}
.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	height: 45px;
	border: 1px solid #ddd;
	font-size: 16px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #59B2E0;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #59B2E6;
}
.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #53A3CD;
	border-color: #53A3CD;
}
.forgot-password {
	text-decoration: underline;
	color: #888;
}
.forgot-password:hover,
.forgot-password:focus {
	text-decoration: underline;
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 14px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}
.success_msg{

    text-align: center;
    color: seagreen;
    font-size: larger;
    margin-bottom: 10px;

}

.error_msg{

text-align: center;
color: red;
font-size: larger;
margin-bottom: 10px;

}


</style>
<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{ asset('fundsmap.ico') }}">
    <title>{{ isset($page_title)?$page_title:"Health Test Login Panel" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />

    <!-- iCheck -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{asset('admin/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('admin/js/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body class="login-page">

  

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-login">
                <div class="login-logo">
                    <a href="#"><img src="{{asset('funds_map_logo.png')}}"><span></span></a>
                </div>
                <p class="login-box-msg"><span style="font-style: italic;font-weight: bold;">Hi Admin!</span> Sign in to your account.</p>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            @if (session('failed_message'))
                            <p class="alert alert-danger">
                                {{ session('failed_message') }}
                            </p>
                        @endif
                        @if (session('flash_message'))
                            <p class="alert alert-success">
                                {{ session('flash_message') }}
                            </p>
                        @endif

                        @if ($errors->has('email'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                    @endif

                    @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
                        <div class="success_msg">   </div>
                        <div class="error_msg">   </div>
                            <form  id="login-form" role="form" style="display: block;" class="form" method="post" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Email / Username" value="{{old('email')}}"  autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
                                </div>
  
                             
                                <div class="col-xs-4">
                                    {{-- <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In"> --}}

                                    <button name="login-submit"  type="submit" id ="btnFetch login-submit" class="btn btn-success btn-block btn-flat btn-login">Log In</button>
                
                                </div>
                                <div class="col-xs-8">
                                    <a href="{{url('/admin/password/reset')}}"><button type="button" class="btn btn-danger btn-block btn-flat ">Forgot Password</button></a>
                                </div>
                                </form>


                            <form id="register-form" action="{{ route('login') }}" method="post" role="form" style="display: none;">
                               
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="text" name="email" id="mobile_number" tabindex="1" class="form-control" placeholder="Enter Mobile Number" value="">
                                </div>

                                <div class="form-group">
                                   <button type="button" id="get_otp_btn" class="btn btn-info btn-block btn-flat ">Send OTP</button>
                                </div>

                                <div class="form-group">
                                    <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Enter OTP">
                                </div>
                            
                                <div class="form-group">
                                    {{-- <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In"> --}}

                                    <button   type="submit" id ="btnFetch login-submit btn-sign-in" class="btn btn-success btn-block btn-flat btn-login">Log In</button>
                
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset ("/bower_components/jquery/dist/jquery.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/bower_components/admin-lte/dist/js/adminlte.min.js") }}" type="text/javascript"></script>

<!-- iCheck -->

</body>
<script>
    $(document).ready(function() {
        $(document).ready(function(){
    $('form').attr('autocomplete', 'off');
});
        $(function() {

$('#login-form-link').click(function(e) {
    $('.success_msg').text('');
    $('.error_msg').text('');
    $("#login-form").delay(100).fadeIn(100);
     $("#register-form").fadeOut(100);
    $('#register-form-link').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
});
$('#register-form-link').click(function(e) {
    $('.success_msg').text('');
    $('.error_msg').text('');

    $("#register-form").delay(100).fadeIn(100);
     $("#login-form").fadeOut(100);
    $('#login-form-link').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
});

});

    $("#btnFetch").click(function() {
      // disable button
      $(this).prop("disabled", true);
      // add spinner to button
      $(this).html(
        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...`
      );

        $('form').submit();
      
    });


    $("body").on('click','#get_otp_btn',function(){

        var mobile_number = $('#mobile_number').val();
        var filter = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/;

        if (filter.test(mobile_number)) {
          $('#get_otp_btn').text("Please Wait...");
          $('#get_otp_btn').prop("disabled",true);
            
           $.ajax({
                url: "{{url('admin/send_otp_for_report_user_login')}}",
                type: 'GET',
                data: {mobile_number},
                success: function (response) {
            
                     $('#get_otp_btn').prop("disabled",false);
                  
                     if(response==0){
                        $('.error_msg').text('mobile number is not allowed for report Admin');
                       
                        $('#mobile_number').val('');
                     $('#get_otp_btn').text("Send OTP");

                     }

                     if(response==1){
                     $('#get_otp_btn').text("Resend OTP");

                        $('.success_msg').text('OTP is sent to your Mobile Number');

                     }


                },
                error: function(data, errorThrown)
                {
                   
                    $('.error_msg').text(data.responseJSON.message);
                }
            });

        }else{

            $('.error_msg').text("Please enter valid mobile number");
      

        }
    });
    
    
});

</script>
</html>
