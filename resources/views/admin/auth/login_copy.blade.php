<!DOCTYPE html>
<html>
<style>
    .help-block{
        color:red !important;
    }
    .login-page, .register-page {
        background: url({{asset('Desktop-Homepage.png')}}) 0% 0% / cover no-repeat !important;
    }

    @media(max-width:768px){
        .login-page, .register-page {
            background: url({{asset('mobile-Homepage.png')}}) 0% 0% / cover no-repeat !important;
        }
        .login-box, .register-box {
            padding-top: 35%;
        }
    }
</style>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{ asset('fundsmap.ico') }}">
    <title>{{ isset($page_title)?$page_title:"Orpat Login Panel" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />

    <!-- iCheck -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{asset('admin/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('admin/js/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">

    <!-- /.login-logo -->
    <div class="login-box-body">
        <div class="login-logo">
            <a href="#"><img src="{{asset('funds_map_logo.png')}}"><span></span></a>
        </div>
        <p class="login-box-msg"><span style="font-style: italic;font-weight: bold;">Hi Admin!</span> Sign in to your account.</p>
        @if (session('failed_message'))
            <p class="alert alert-danger">
                {{ session('failed_message') }}
            </p>
        @endif
        @if (session('flash_message'))
            <p class="alert alert-success">
                {{ session('flash_message') }}
            </p>
        @endif

        <form  class="form" method="post" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input type="text" name="email" class="form-control" placeholder="Email / Username" value="{{old('email')}}" autocomplete="off">
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off">

                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
{{--            <div class="form-group"><br>--}}
{{--                <label for="password" class="text-info">Remember me: </label>&nbsp;&nbsp;--}}
{{--                <input checked="checked" name="remember" type="checkbox" id="remember">--}}
{{--            </div>--}}
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" id ="btnFetch" class="btn btn-success btn-block btn-flat">Sign In</button>

                </div>
                <div class="col-xs-8">
                    <a href="{{url('/admin/password/reset')}}"><button type="button" class="btn btn-danger btn-block btn-flat ">Forgot Password</button></a>
                </div>
                <!-- /.col -->
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset ("/bower_components/jquery/dist/jquery.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset ("/bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ("/bower_components/admin-lte/dist/js/adminlte.min.js") }}" type="text/javascript"></script>

<!-- iCheck -->

</body>
<script>
    $(document).ready(function() {
    $("#btnFetch").click(function() {
      // disable button
      $(this).prop("disabled", true);
      // add spinner to button
      $(this).html(
        `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...`
      );

        $('form').submit();
      
    });
});

</script>
</html>
