@extends('admin.layouts.master')
@section('content')
    <div class="panel panel-success">
        <div class="panel-heading">Users</div>
        <div class="panel-body">
            <div class="col-md-12 page-action text-left">
                @can('add_users')
                <a href="{{ url('/admin/users/create')}}" class="btn btn-success btn-sm pull-left"
                   title="Add New User">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
                @endcan
            </div>
            <div class="col-md-12">
                <form method="GET" action="{{ url('/admin/users') }}" accept-charset="UTF-8"
                      class="navbar-form navbar-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               value="{{ request('search') }}">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                </form>
            </div>
            <br>
            <br>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="data-table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Password</th>
                        <th>Log In</th>
                        <th>Created At</th>
                        @can('edit_users', 'delete_users')
                            <th class="text-center">Actions</th>
                        @endcan
                    </tr>
                    </thead>
                        <tbody>

                    @foreach($result as $key => $item)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->roles->implode('name', ', ') }}</td>
                            <td>{{ $item->new_password}}</td>
                            <td>
                                {{-- @if($item->id !=1 && $item->id !=72 && Auth::user()->roles->implode('name', ', ')=='Admin') --}}
                                <form method="POST"
                                action="{{url('/admin/login_as_user/'.$item->id)}}"
                                accept-charset="UTF-8" style="display:inline">
                              {{ csrf_field() }}
                              <button type="submit" class="btn btn-success btn-xs"
                                      title="Delete SForm"
                            onclick="return confirm(&quot;Are you sure to login behalf of this user {{$item->name}}?&quot;)"><i class="fa fa-user" aria-hidden="true"></i> Log In ?

                              </button>
                          </form>
                          {{-- @endif --}}
                            </td>
                            <td>{{ $item->created_at->toFormattedDateString() }}</td>
                            @can('view_users')
                                <td class="text-center">

                                         <span @if($item->id==1 || $item->name=='Admin') style="display: none" @endif>
                                           <a href="{{ url('/admin/users/' . $item->id) }}" title="View users"><button class="btn btn-primary btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                        @include('shared._actions', [
                                        'entity' => 'users',
                                        'id' => $item->id
                                        ])
                                        </span>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper"> {!! $result->appends(['search' => Request::get('search')])->render() !!} </div>
                </div>

            </div>
        </div>
    </div>

@endsection
