<style>
    li.select2-selection__choice {
        color: black !important;
        background-color: lightblue !important;
    }
    #permissionHeading{
        margin-left: 284px;
        padding-bottom: 15px;
    }

    .selected{
        background: darkgreen;
    }

</style>
<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Users</b></div>
        <br/>
        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
            <label for="image" class="col-md-4 control-label">{{ 'Profile Image:' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="image" type="file" id="image">
                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="col-md-4 control-label">{{ 'Name : ' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="name" type="text" id="name"
                       value="{{ isset($user->name) ? $user->name : old('name')}}">
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('user_name') ? 'has-error' : ''}}">
            <label for="user_name" class="col-md-4 control-label">{{ 'User Name : ' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="user_name" type="text" id="user_name"
                       value="{{ isset($user->user_name) ? $user->user_name : ''}}">
                {!! $errors->first('user_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email" class="col-md-4 control-label">{{ 'Email : ' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="email" type="email" id="email"
                       value="{{ isset($user->email) ? $user->email : old('email')}}">
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
            <label for="mobile" class="col-md-4 control-label">{{ 'Mobile : ' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="mobile" type="text" id="mobile"
                       value="{{ isset($user->mobile) ? $user->mobile : old('mobile')}}">
                {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
{{--        <div class="form-group {{ $errors->has('region_id') ? 'has-error' : ''}}">--}}
{{--            <label for="region_id" class="col-md-4 control-label">{{ 'Region' }} :</label>--}}
{{--            <div class="col-md-6">--}}

{{--                <select name="region_id" class="form-control" id="region_id">--}}
{{--                     <option value="">{{'Select Region'}}</option>--}}
{{--                     @if(isset($regions))--}}
{{--                        @foreach($regions as $region)--}}
{{--                         <option value="{{$region->id}}" @if(isset($user->region) && $user->region->id==$region->id) {{'selected'}} @endif>{{$region->name}}</option>--}}
{{--                         @endforeach--}}
{{--                      @endif--}}
{{--                </select>--}}
{{--                {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
            <label for="state" class="col-md-4 control-label">{{ 'State : ' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="state" type="text" id="state"
                       value="{{ isset($user->state) ? $user->state : old('state')}}">
                {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
            <label for="city" class="col-md-4 control-label">{{ 'City : ' }}</label>

            <div class="col-md-6">
                <input class="form-control" name="city" type="text" id="city"
                       value="{{ isset($user->city) ? $user->city : old('city')}}">
                {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
            <label for="password" class="col-md-4 control-label">{{ 'Password : ' }}</label>
            <div class="col-md-6">
                <input class="form-control" name="password" type="password" id="password"
                       value="">
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('categories_id') ? 'has-error' : ''}}">
            <label for="categories_id" class="col-md-4 control-label">{{ 'Select Categories' }} :</label>
            <div class="col-md-6">
                <select name="categories_id[]" class="form-control" id="categories_id"  multiple>
                    <?php
                    if(isset($user->categories_id) && $user->categories_id != null){
                        $unser=unserialize($user->categories_id);
                    }
                    ?>
                    @foreach ($category as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($unser) && in_array($optionKey,$unser)) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('categories_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('Subcategory') ? 'has-error' : ''}}">
            <label for="subcategories_id" class="col-md-4 control-label">{{ 'Select Subcategory' }} :</label>
            <div class="col-md-6 testsasa">
                <select name="subcategories_id[]" class="form-control" id="subcategories_id"  multiple>
                    <?php
                    if(isset($user->subcategories_id) && $user->subcategories_id != null){
                        $selected_subcategories=unserialize($user->subcategories_id);
                    }
                    ?>
                    @foreach ($subcategory as $optionKeys => $optionValue)
                        <option value="{{ $optionKeys }}" {{ (isset($selected_subcategories) && in_array($optionKeys,$selected_subcategories)) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('subcategories_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('roles') ? 'has-error' : ''}}">
            <label for="roles" class="col-md-4 control-label">{{ 'Roles : ' }}</label>
            <div class="col-md-6">
                <b style="color: red;">Note:- </b><i>Select one or more than one roles.</i>
                @if(isset($rol))
                    {!! Form::select('roles[]', $rol, isset($user) ? $user->roles->pluck('id')->toArray() : null,  ['class' => 'form-control', 'id' =>'userlistselect', 'multiple']) !!}
                @else
                    <select name="roles[]" class="form-control" id="userlistselect" multiple>
                        @if(isset($roles) && !empty($roles))
                            @foreach($roles as $key => $value)
                                <option value="{{$value->id}}" {{ (collect(old('roles'))->contains($value->id)) ? 'selected':'' }}>{{$value->name}}</option>
                            @endforeach
                        @endif
                    </select>
               @endif
                {!! $errors->first('roles', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        @if(isset($user))
            @include('shared._permissions', ['closed' => 'true', 'model' => $user ])
        @endif
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
@section('scripts')
<script>
    $(document).ready(function() {
     // $('#userlistselect').select2();
        $('#userlistselect').multiselect({
            includeSelectAllOption: true,
            placeholder : "Select Roles",
            selectAll: true

        });

        $('#categories_id').multiselect({
            includeSelectAllOption: true,
            placeholder : "Select Categories",
            selectAll: true
        });

        $('#subcategories_id').multiselect({
            includeSelectAllOption: true,
            placeholder : "Select Subcategory",
            selectAll: true
        });

        $(document).on('change', '#categories_id',function(){           
            var category_ids =$('#categories_id').val();
            console.log(category_ids);
            $.ajax({
                type:'get',
                url: "{{url('/admin/ajax/load_sub_category')}}",
                data:{category_ids},
                success:function(data){
                    console.log(data);
                    var selOpts = "<select class='form-control' name='subcategories_id[]' id='subcategories_id' multiple>";
                    if(data['sub_categories']!==undefined && data['sub_categories'].length>0){
                
                        $.each(data['sub_categories'], function(k, v)
                        {
                            var id = data['sub_categories'][k].id;
                            var val = data['sub_categories'][k].name;
                            selOpts += "<option value='"+id+"'>"+val+"</option>";
                        });
                        selOpts +='</select>';
                        $('.testsasa').html(selOpts);
                        $('#subcategories_id').multiselect({
                            includeSelectAllOption: true,
                            placeholder : "Select subcategory",
                            selectAll: true
                        });
                    }



                }
            });
        });
    });
</script>
@endsection