<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Account</b></div>
        <br/>
        <div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
            <label for="date" class="col-md-4 control-label">{{ 'Date' }} :</label>
            <div class="col-md-6">
                <input class="form-control datepicker" name="date" type="text" id="date"
                       value="{{ isset($account->date) ? $account->date : old('date')}}"
                       required>
                {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('sales_person_name') ? 'has-error' : ''}}">
            <label for="sales_person_name" class="col-md-4 control-label">{{ 'Sales Person Name' }} :</label>
            <div class="col-md-6">
                <select name="sales_person_name" class="form-control selectSearchClass" id="sales_person_name">
                    @foreach ($salesPerson as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($account->sales_person_name) && $account->sales_person_name == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('sales_person_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('distance') ? 'has-error' : ''}}">
            <label for="distance" class="col-md-4 control-label">{{ 'Distance' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="distance" type="text" id="distance"
                       value="{{ isset($account->distance) ? $account->distance : old('distance')}}">
                {!! $errors->first('distance', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('advanced_amount') ? 'has-error' : ''}}">
            <label for="advanced_amount" class="col-md-4 control-label">{{ 'Advanced Amount' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="advanced_amount" type="text" id="advanced_amount"
                       value="{{ isset($account->advanced_amount) ? $account->advanced_amount : old('advanced_amount')}}">
                {!! $errors->first('advanced_amount', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('total_amount') ? 'has-error' : ''}}">
            <label for="total_amount" class="col-md-4 control-label">{{ 'Total Amount' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="total_amount" type="text" id="total_amount"
                       value="{{ isset($account->total_amount) ? $account->total_amount : old('total_amount')}}">
                {!! $errors->first('total_amount', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('total_lead') ? 'has-error' : ''}}">
            <label for="total_lead" class="col-md-4 control-label">{{ 'Total Lead' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="total_lead" type="numbar" id="total_lead"
                       value="{{ isset($account->total_lead) ? $account->total_lead : old('total_lead')}}">
                {!! $errors->first('total_lead', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('remark') ? 'has-error' : ''}}">
            <label for="remark" class="col-md-4 control-label">{{ 'Remark' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="remark" type="text" id="remark"
                       value="{{ isset($account->remark) ? $account->remark : old('remark')}}">
                {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
