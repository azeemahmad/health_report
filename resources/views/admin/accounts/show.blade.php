@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <div class="panel panel-success">
                    <div class="panel-heading">Account {{ $account->id }}</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/accounts') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        @can('edit_accounts', 'delete_accounts')
                        <a href="{{ url('/admin/accounts/' . $account->id . '/edit') }}" title="Edit Account"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/accounts' . '/' . $account->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Account" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $account->id }}</td>
                                    </tr>
                                    <tr><th> Sales Person Name </th><td> {{ $account->sales_person_name }} </td></tr><tr><th> Distance </th><td> {{ $account->distance }} </td></tr><tr><th> Advanced Amount </th><td> {{ $account->advanced_amount }} </td></tr><tr><th> Total Amount </th><td> {{ $account->total_amount }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
