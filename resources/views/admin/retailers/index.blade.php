@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="panel panel-success">
            <div class="panel-heading">Retailers</div>
            <div class="panel-body">
                <div class="col-md-12 page-action text-left">
                    <a href="{{ url('/admin/retailers/create') }}" class="btn btn-success btn-sm"
                       title="Add New Retailer">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                </div>
                <div class="col-md-12">
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/retailers', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered" id="data-table">
                            <thead>
                            <tr>
                                <th>Sr.no</th>
                                <th>Date</th>
                                <th>Owner Name</th>
                                <th>Retailer Outlet</th>
                                <th>Address 1</th>
                                <th>Mobile</th>
                                <th>Category</th>
                                <th>Area</th>
                                <th>City</th>
                                <th>Sales Person</th>
                                <th>Pincode</th>
                                    <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($retailers as $key => $item)
                                <tr>
                                    <td>{{ (($retailers->currentPage() - 1 ) * $retailers->perPage() ) + $loop->iteration }}</td>
                                    <td>{{ $item->date }}</td>
                                    <td>{{ $item->owner_name }}</td>
                                    <td>{{ $item->retailer_outlet }}</td>
                                    <td width="100px">{{ $item->address_1 }}</td>
                                    <td>{{ $item->mobile }}</td>
                                    <td>{{ $item->category_1 }}</td>
                                    <td>{{ $item->area }}</td>
                                    <td>{{ $item->city }}</td>
                                    <td>{{ $item->sales_person_name }}</td>
                                    <td>{{ $item->pincode }}</td>

                                        <td class="text-center">
                                            <a href="{{ url('/admin/retailers/' . $item->id) }}" title="View Retailer">
                                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i> View
                                                </button>
                                            </a>

                                                <a href="{{ url('/admin/retailers/' . $item->id . '/edit') }}"
                                                   title="Edit Retailer">
                                                    <button class="btn btn-primary btn-xs"><i
                                                                class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit
                                                    </button>
                                                </a>

                                                <form method="POST"
                                                      action="{{ url('admin/retailers' . '/' . $item->id) }}"
                                                      accept-charset="UTF-8" style="display:inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-xs"
                                                            title="Delete Retailer"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                    </button>
                                                </form>

                                        </td>



                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $retailers->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
