<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Retailer</b></div>
        <br/>
        <div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
            <label for="date" class="col-md-4 control-label">{{ 'Date' }} :</label>
            <div class="col-md-6">
                <input class="form-control datepicker" name="date" type="text" id="date"
                       value="{{ isset($retailer->date) ? $retailer->date : '29/06/2021'}}" required>
                {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('sales_person_name') ? 'has-error' : ''}}">
            <label for="sales_person_name" class="col-md-4 control-label">{{ 'Sales Person Name' }} :</label>
            <div class="col-md-6">
                <select name="sales_person_name" class="form-control selectSearchClass" id="sales_person_name">
                    @foreach ($salesPerson as $optionKey => $optionValue)
                        <option value="{{ 'HSARAT AZMI' }}" {{ (isset($retailer->sales_person_name) && $retailer->sales_person_name == $optionKey) ? 'selected' : ''}}>{{ 'HSARAT AZMI' }}</option>
                    @endforeach
                </select>
                {!! $errors->first('sales_person_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('owner_name') ? 'has-error' : ''}}">
            <label for="owner_name" class="col-md-4 control-label">{{ 'Owner Name' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="owner_name" type="text" id="owner_name"
                       value="{{ isset($retailer->owner_name) ? $retailer->owner_name : old('owner_name')}}">
                {!! $errors->first('owner_name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('retailer_outlet') ? 'has-error' : ''}}">
            <label for="retailer_outlet" class="col-md-4 control-label">{{ 'Retailer Outlet' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="retailer_outlet" type="text" id="retailer_outlet"
                       value="{{ isset($retailer->retailer_outlet) ? $retailer->retailer_outlet : old('retailer_outlet')}}">
                {!! $errors->first('retailer_outlet', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('whatsapp_mobile') ? 'has-error' : ''}}">
            <label for="whatsapp_mobile" class="col-md-4 control-label">{{ 'Whatsapp Mobile' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="whatsapp_mobile" type="text" id="whatsapp_mobile"
                       value="{{ isset($retailer->whatsapp_mobile) ? $retailer->whatsapp_mobile : old('whatsapp_mobile')}}">
                {!! $errors->first('whatsapp_mobile', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
            <label for="mobile" class="col-md-4 control-label">{{ 'Mobile' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="mobile" type="text" id="mobile"
                       value="{{ isset($retailer->mobile) ? $retailer->mobile : old('mobile')}}">
                {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('category_1') ? 'has-error' : ''}}">
            <label for="category_1" class="col-md-4 control-label">{{ 'Category 1' }} :</label>
            <div class="col-md-6">
                <select name="category_1" class="form-control selectSearchClass" id="category_1">
                    <option value="">SELECT CATEGORY 1</option>
                    @foreach ($category as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($retailer->category_1) && $retailer->category_1 == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('category_1', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('category_2') ? 'has-error' : ''}}">
            <label for="category_2" class="col-md-4 control-label">{{ 'Category 2' }} :</label>
            <div class="col-md-6">
                <select name="category_2" class="form-control selectSearchClass" id="category_2">
                    <option value="">SELECT CATEGORY 2</option>
                    @foreach ($category as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($retailer->category_2) && $retailer->category_2 == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('category_2', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('address_1') ? 'has-error' : ''}}">
            <label for="address_1" class="col-md-4 control-label">{{ 'Address 1' }} :</label>
            <div class="col-md-6">
                <textarea class="form-control" rows="5" name="address_1" type="textarea" id="address_1"
                          required>{{ isset($retailer->address_1) ? $retailer->address_1 : 'PHULPUR,AZAMGARH'}}</textarea>
                {!! $errors->first('address_1', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('pincode') ? 'has-error' : ''}}">
            <label for="pincode" class="col-md-4 control-label">{{ 'Pincode' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="pincode" type="number" id="pincode"
                       value="{{ isset($retailer->pincode) ? $retailer->pincode : '276304'}}">
                {!! $errors->first('pincode', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
            <label for="city" class="col-md-4 control-label">{{ 'City' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="city" type="text" id="city"
                       value="{{ isset($retailer->city) ? $retailer->city : 'PHULPUR'}}">
                {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('area') ? 'has-error' : ''}}">
            <label for="area" class="col-md-4 control-label">{{ 'Area' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="area" type="text" id="area"
                       value="{{ isset($retailer->area) ? $retailer->area : 'PHULPUR'}}">
                {!! $errors->first('area', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>

        <div class="form-group {{ $errors->has('address_2') ? 'has-error' : ''}}">
            <label for="address_2" class="col-md-4 control-label">{{ 'Address 2' }} :</label>
            <div class="col-md-6">
                <textarea class="form-control" rows="5" name="address_2" type="textarea"
                          id="address_2">{{ isset($retailer->address_2) ? $retailer->address_2 : old('address_2')}}</textarea>
                {!! $errors->first('address_2', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('address_3') ? 'has-error' : ''}}">
            <label for="address_3" class="col-md-4 control-label">{{ 'Address 3' }} :</label>
            <div class="col-md-6">
                <textarea class="form-control" rows="5" name="address_3" type="textarea"
                          id="address_3">{{ isset($retailer->address_3) ? $retailer->address_3 : old('address_3')}}</textarea>
                {!! $errors->first('address_3', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('address_4') ? 'has-error' : ''}}">
            <label for="address_4" class="col-md-4 control-label">{{ 'Address 4' }} :</label>
            <div class="col-md-6">
                <textarea class="form-control" rows="5" name="address_4" type="textarea"
                          id="address_4">{{ isset($retailer->address_4) ? $retailer->address_4 : old('address_4')}}</textarea>
                {!! $errors->first('address_4', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
            <label for="state" class="col-md-4 control-label">{{ 'State' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="state" type="text" id="state"
                       value="{{ isset($retailer->state) ? $retailer->state : 'UTTAR PRADESH'}}">
                {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('land_line_number') ? 'has-error' : ''}}">
            <label for="land_line_number" class="col-md-4 control-label">{{ 'Land Line Number' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="land_line_number" type="text" id="land_line_number"
                       value="{{ isset($retailer->land_line_number) ? $retailer->land_line_number : old('land_line_number')}}">
                {!! $errors->first('land_line_number', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email" class="col-md-4 control-label">{{ 'Email' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="email" type="text" id="email"
                       value="{{ isset($retailer->email) ? $retailer->email : old('email')}}">
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('classification') ? 'has-error' : ''}}">
            <label for="classification" class="col-md-4 control-label">{{ 'Classification' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="classification" type="text" id="classification"
                       value="{{ isset($retailer->classification) ? $retailer->classification : old('classification')}}">
                {!! $errors->first('classification', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('category_3') ? 'has-error' : ''}}">
            <label for="category_3" class="col-md-4 control-label">{{ 'Category 3' }} :</label>
            <div class="col-md-6">
                <select name="category_3" class="form-control selectSearchClass" id="category_3">
                    <option value="">SELECT CATEGORY 3</option>
                    @foreach ($category as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($retailer->category_3) && $retailer->category_3 == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('category_3', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <?php
        $availability_status = [
            'NO' => 'NO',
            'YES' => 'YES'
        ];
        ?>
        <div class="form-group {{ $errors->has('availability_status') ? 'has-error' : ''}}">
            <label for="availability_status" class="col-md-4 control-label">{{ 'Availability Status' }} :</label>
            <div class="col-md-6">
                <select name="availability_status" class="form-control selectSearchClass" id="availability_status">
                   @foreach ($availability_status as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($retailer->availability_status) && $retailer->availability_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('availability_status', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('territory_code') ? 'has-error' : ''}}">
            <label for="territory_code" class="col-md-4 control-label">{{ 'Territory Code' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="territory_code" type="text" id="territory_code"
                       value="{{ isset($retailer->territory_code) ? $retailer->territory_code : old('territory_code')}}">
                {!! $errors->first('territory_code', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('date_of_birth') ? 'has-error' : ''}}">
            <label for="date_of_birth" class="col-md-4 control-label">{{ 'Date Of Birth' }} :</label>
            <div class="col-md-6">
                <input class="form-control datepicker" name="date_of_birth" type="text" id="date_of_birth"
                       value="{{ isset($retailer->date_of_birth) ? $retailer->date_of_birth : old('date_of_birth')}}">
                {!! $errors->first('date_of_birth', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('wedding_year') ? 'has-error' : ''}}">
            <label for="wedding_year" class="col-md-4 control-label">{{ 'Wedding Year' }} :</label>
            <div class="col-md-6">
                <input class="form-control datepicker" name="wedding_year" type="text" id="wedding_year"
                       value="{{ isset($retailer->wedding_year) ? $retailer->wedding_year : old('wedding_year')}}">
                {!! $errors->first('wedding_year', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('pan_number') ? 'has-error' : ''}}">
            <label for="pan_number" class="col-md-4 control-label">{{ 'Pan Number' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="pan_number" type="text" id="pan_number"
                       value="{{ isset($retailer->pan_number) ? $retailer->pan_number : old('pan_number')}}">
                {!! $errors->first('pan_number', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('aadhar_number') ? 'has-error' : ''}}">
            <label for="aadhar_number" class="col-md-4 control-label">{{ 'Aadhar Number' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="aadhar_number" type="text" id="aadhar_number"
                       value="{{ isset($retailer->aadhar_number) ? $retailer->aadhar_number : old('aadhar_number')}}">
                {!! $errors->first('aadhar_number', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('gst_number') ? 'has-error' : ''}}">
            <label for="gst_number" class="col-md-4 control-label">{{ 'Gst Number' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="gst_number" type="text" id="gst_number"
                       value="{{ isset($retailer->gst_number) ? $retailer->gst_number : old('gst_number')}}">
                {!! $errors->first('gst_number', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
