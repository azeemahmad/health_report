@extends('admin.layouts.master')
@section('content')
    <style>
        .require:after{
            content:'*';
            color:red;
        }
    </style>
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title" style="padding:12px 0px;font-size:25px;"><strong>Upload Inventory Master</strong></h3>
            </div>
            <div class="panel-body">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('success') }}
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                @endif
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                <h3>Import File Form:</h3>
                <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 20px;" action="{{ URL::to('admin/excel-retailers-data') }}" class="form-horizontal" method="post" enctype="multipart/form-data" id="uploadInventoryFile">
                    {{csrf_field()}}
                    <div class="form-group {{ $errors->has('import_file') ? 'has-error' : ''}}">
                        {!! Form::label('import_file', 'Select a file', ['class' => 'col-md-4 control-label require']) !!}
                        <div class="col-md-6">
                            <input type="file" name="import_file" class="form-control"  required>
                            {!! $errors->first('import_file', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class="col-md-4"></div>
                        <div class="col-md-6">
                            <button class="btn btn-primary" type="submit" id="import_file"><i class="fa fa-upload" aria-hidden="true"></i> Import Excel File</button>
                            <div id="displayImage" style="display: none"><img src="{{asset('ajax-loader.gif')}}"></div>
                        </div>
                    </div>
                </form>
                <br/>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
            $('#uploadInventoryFile').submit(function () {
                $('#import_file').css('background','red');
                $('#displayImage').show();
                $('#import_file').attr("disabled", true);
                return true;
            });
        });

    </script>
@endsection
