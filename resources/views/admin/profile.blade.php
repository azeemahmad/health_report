@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body">

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" accept-charset="UTF-8" class="form-horizontal"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="pull-right">
                                @if (session('error_password'))
                                    <span class="alert alert-danger">
                              {{ session('error_password') }}
                             </span>
                                @endif
                            </div>
                            <div class="panel-group">
                                <div class="panel panel-default  panel-primary">
                                    <div class="panel-heading"><b>{{Auth::user()->name}}</b></div>
                                    <br/>

                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                        <label for="name" class="col-md-4 control-label">{{ 'Name' }}</label>

                                        <div class="col-md-6">
                                            <input class="form-control" name="name" type="text" id="name"
                                                   value="{{ Auth::user()->name}}" required>
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                        <label for="email" class="col-md-4 control-label">{{ 'Email' }}</label>

                                        <div class="col-md-6">
                                            <input class="form-control" name="email" type="email" id="email"
                                                   value="{{ Auth::user()->email}}" readonly>
                                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                                        <label for="mobile" class="col-md-4 control-label">{{ 'Mobile : ' }}</label>

                                        <div class="col-md-6">
                                            <input class="form-control" name="mobile" type="text" id="mobile"
                                                   value="{{ isset(Auth::user()->mobile) ? Auth::user()->mobile : old('mobile')}}">
                                            {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
                                        <label for="state" class="col-md-4 control-label">{{ 'State : ' }}</label>
                                        <div class="col-md-6">
                                            <input class="form-control" name="state" type="text" id="state"
                                                   value="{{ isset(Auth::user()->state) ? Auth::user()->state : old('state')}}">
                                            {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
                                        <label for="city" class="col-md-4 control-label">{{ 'City : ' }}</label>

                                        <div class="col-md-6">
                                            <input class="form-control" name="city" type="text" id="city"
                                                   value="{{ isset(Auth::user()->city) ? Auth::user()->city : old('city')}}">
                                            {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('old_password') ? 'has-error' : ''}}">
                                        <label for="old_password"
                                               class="col-md-4 control-label">{{ 'Old Password' }}</label>

                                        <div class="col-md-6">
                                            <input class="form-control" name="old_password" type="password"
                                                   id="old_password">
                                            {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                        <label for="password"
                                               class="col-md-4 control-label">{{ 'New Password' }}</label>

                                        <div class="col-md-6">
                                            <input class="form-control" name="password" type="password" id="password">
                                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : ''}}">
                                        <label for="password_confirmation"
                                               class="col-md-4 control-label">{{ 'Password Confirmation' }}</label>

                                        <div class="col-md-6">
                                            <input class="form-control" name="password_confirmation" type="password"
                                                   id="password_confirmation">
                                            {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>



                                    <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                                        <label for="image" class="col-md-4 control-label">{{ 'Profile Image' }}</label>

                                        <div class="col-md-6">
                                            <input class="form-control" name="image" type="file" id="image">
                                            {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    @if(Auth::user()->image)
                                        <div class="pull-right"><img
                                                    src="{{asset('images/profile_image/'.Auth::user()->image)}}"
                                                    height="180px" width="180px"></div>
                                    @endif


                                    <div class="form-group">
                                        <div class="col-md-offset-4 col-md-6">
                                            <input class="btn btn-primary" type="submit" value="{{'Update' }}">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
