<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Client</b></div>
        <br/>
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="col-md-4 control-label">{{ 'Name' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="name" type="text" id="name"
                       value="{{ isset($client->name) ? $client->name : old('name')}}" required>
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('passport_no') ? 'has-error' : ''}}">
            <label for="passport_no" class="col-md-4 control-label">{{ 'Passport No' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="passport_no" type="text" id="passport_no"
                       value="{{ isset($client->passport_no) ? $client->passport_no : old('passport_no')}}">
                {!! $errors->first('passport_no', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
            <label for="passport_no" class="col-md-4 control-label">{{ 'Date of Birth' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="dob" type="text" id="dob"
                       value="{{ isset($client->dob) ? $client->dob : old('dob')}}">
                {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('dubai_lab_no') ? 'has-error' : ''}}">
            <label for="dubai_lab_no" class="col-md-4 control-label">{{ 'Lab No' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="dubai_lab_no" type="text" id="dubai_lab_no" required
                       value="{{ isset($client->dubai_lab_no) ? $client->dubai_lab_no : $lastData->dubai_lab_no+2}}">
                {!! $errors->first('dubai_lab_no', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('file_no') ? 'has-error' : ''}}">
            <label for="file_no" class="col-md-4 control-label">{{ 'File No' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="file_no" type="text" id="file_no" required
                       value="{{ isset($client->file_no) ? $client->file_no : $lastData->file_no+2}}">
                {!! $errors->first('file_no', '<p class="help-block">:message</p>') !!}
            </div>
        </div>



        <div class="form-group {{ $errors->has('clinic_file_no') ? 'has-error' : ''}}">
            <label for="file_no" class="col-md-4 control-label">{{ 'Clinic File No' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="clinic_file_no" type="text" id="clinic_file_no" required
                       value="{{ isset($client->clinic_file_no) ? $client->clinic_file_no : $lastData->clinic_file_no+2}}">
                {!! $errors->first('clinic_file_no', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('dubai_request_date') ? 'has-error' : ''}}">
            <label for="dubai_request_date" class="col-md-4 control-label">{{ 'Request Date' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="dubai_request_date" type="text" id="dubai_request_date"
                       value="{{ isset($client->dubai_request_date) ? $client->dubai_request_date : date('Y-m-d H:i:s',strtotime("+2 minutes 29 second", strtotime($lastData->dubai_request_date)))}}">
                {!! $errors->first('dubai_request_date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('dubai_collected_on') ? 'has-error' : ''}}">
            <label for="dubai_collected_on" class="col-md-4 control-label">{{ 'Collected Date' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="dubai_collected_on" type="text" id="dubai_collected_on"
                       value="{{ isset($client->dubai_collected_on) ? $client->dubai_collected_on : date('Y-m-d H:i:s',strtotime("+2 minutes 29 second", strtotime($lastData->dubai_collected_on)))}}">
                {!! $errors->first('dubai_collected_on', '<p class="help-block">:message</p>') !!}
            </div>
        </div>


        <div class="form-group {{ $errors->has('dubai_recieved_od') ? 'has-error' : ''}}">
            <label for="dubai_recieved_od" class="col-md-4 control-label">{{ 'Received Date' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="dubai_recieved_od" type="text" id="dubai_recieved_od"
                       value="{{ isset($client->dubai_recieved_od) ? $client->dubai_recieved_od : date('Y-m-d H:i:s',strtotime("+2 minutes 29 second", strtotime($lastData->dubai_recieved_od)))}}">
                {!! $errors->first('dubai_recieved_od', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('dunai_authenticated_on') ? 'has-error' : ''}}">
            <label for="dunai_authenticated_on" class="col-md-4 control-label">{{ 'Authenticated Date' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="dunai_authenticated_on" type="text" id="dunai_authenticated_on"
                       value="{{ isset($client->dunai_authenticated_on) ? $client->dunai_authenticated_on : date('Y-m-d H:i:s',strtotime("+2 minutes 29 second", strtotime($lastData->dunai_authenticated_on)))}}">
                {!! $errors->first('dunai_authenticated_on', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group {{ $errors->has('dubai_released_on') ? 'has-error' : ''}}">
            <label for="dubai_released_on" class="col-md-4 control-label">{{ 'Released Date' }} :</label>
            <div class="col-md-6">
                <input class="form-control" name="dubai_released_on" type="text" id="dubai_released_on"
                       value="{{ isset($client->dubai_released_on) ? $client->dubai_released_on : date('Y-m-d H:i:s',strtotime("+2 minutes 29 second", strtotime($lastData->dubai_released_on)))}}">
                {!! $errors->first('dubai_released_on', '<p class="help-block">:message</p>') !!}
            </div>
        </div>



        <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
            <label for="gender" class="col-md-4 control-label" style="color: red">{{ 'GENDER' }} :</label>
            <div class="col-md-6">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input"  name="gender"   value="male" checked>
                    <label class="custom-control-label"> MALE</label>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" class="custom-control-input" name="gender" value="female" @if(isset($client->gender) && $client->gender=='female') checked @endif>
                    <label class="custom-control-label"> FEMALE</label>
                </div>

                {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
            </div>
        </div>




        <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
            <label for="type" class="col-md-4 control-label" style="color: red">{{ 'REPORT TYPE' }} :</label>
            <div class="col-md-6">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input"  name="type"   value="normal" >
                    <label class="custom-control-label"> NORMAL</label>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" class="custom-control-input" name="type" value="google" checked>
                    <label class="custom-control-label"> GOOGLE</label>
                </div>

                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('report_type') ? 'has-error' : ''}}">
            <label for="report_type" class="col-md-4 control-label" style="color: red">{{ 'Negative / Positive' }} :</label>
            <div class="col-md-6">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input"  name="report_type"   value="negative" checked>
                    <label class="custom-control-label"> NEGATIVE</label>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" class="custom-control-input" name="report_type" value="positive" @if(isset($client->report_type) && $client->report_type=='positive') checked @endif>
                    <label class="custom-control-label"> POSITIVE</label>
                </div>

                {!! $errors->first('report_type', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
            <label for="country" class="col-md-4 control-label" style="color: red">{{ 'Country' }} :</label>
            <div class="col-md-6">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input"  name="country"   value="india">
                    <label class="custom-control-label"> INDIA</label>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" class="custom-control-input" name="country" value="dubai" checked>
                    <label class="custom-control-label"> DUBAI</label>
                    <input type="radio" class="custom-control-input" name="country" value="heli">
                    <label class="custom-control-label"> Heli</label>
                </div>

                {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
