<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <style type="text/css">
        /*        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap');*/

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        /*
        @font-face {
            font-family: "times new roman", serif;
            src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/times new roman.ttf);
        }

        @font-face {
            font-family: Arial, sans-serif;
            src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/ARIAL.TTF);
        }
*/

        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-size: 14px;
            font-family: 'Roboto', sans-serif;
            /*            font-family: "times new roman", serif;*/
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
            position: relative;
            border-left: 30px solid #8dd5f1;
            border-right: 6px solid gray;

        }

        page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }

        .text-center {
            text-align: center;
        }

        .table {
            width: 100%;
            max-width: 100%;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        table {
            background-color: transparent;
            border-spacing: 0;
            border-collapse: collapse;
        }

        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 4px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        th {
            text-align: left;
        }


        p {
            font-size: 13px;
            margin-bottom: 5px;
            margin-top: 0px;
        }

        .fnt-1 {
            margin: 10px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .table {
            margin: 0px;
        }

        .v-mdl {
            vertical-align: middle !important;
        }

        .v-top {
            vertical-align: top !important;
        }

        .no-vorder {
            border: none !important;
        }

        .bd-right-none {
            border-right: none !important;
        }

        .bd-left-none {
            border-left: none !important;
        }

        .invoice-wrapper {
            min-width: 680px;
        }

        .table-bordered,
        .table-bordered>tbody>tr>td,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>td,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border: 1px solid #000;
        }

        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border-top: 0;
            border: 1px solid #000;
        }

        .s-table thead tr th {
            vertical-align: middle;
        }

        .s-table thead tr th:nth-child(5),
        .s-table tbody tr td:nth-child(5) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(6),
        .s-table tbody tr td:nth-child(6) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(7),
        .s-table tbody tr td:nth-child(7) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(8),
        .s-table tbody tr td:nth-child(8) {
            width: 75px;
        }

        .s-table tbody.fnt-invoce tr td {
            font-size: 11px;
        }

        .igst-table {}

        .igst-table tbody tr th:nth-child(3) {
            width: 120px;
        }

        .igst-table tbody tr th {
            font-weight: 400;
        }

        .img-th {
            width: 150px;
        }

        .img-th img {
            max-width: 150px;
        }

        .first-table p {
            margin-bottom: 5px;
        }

        .first-table h4 {
            font-size: 14px;
            margin: 0;
        }

        .party-div {
            padding: 5px;
        }

        .fnt-invoce td {
            font-weight: normal;
        }

        .fnt-invoce td.nrml {
            font-weight: 400;
        }

        .first-table p span.caps {
            text-transform: uppercase;
        }

        .first-table h4 {
            color: #333;
        }

        .first-table h4.city {
            margin-right: 20px;
            display: inline-block;
            float: left;
        }

        .first-table h4.city strong {
            font-weight: 700;
            color: #000;
        }

        .first-table h3 {
            display: block;
            width: 100%;
            float: left;
            margin: 0 0 5px;
            font-size: 13px;
        }

        .right p {
            width: 50%;
            display: inline-block;
            margin-bottom: 5px;
            float: left;
        }

        .first-table tr h1 {
            margin: 5px 0px;
            text-transform: uppercase;
        }

        .top-table tr {
            padding: 2px 8px;
        }

        .top-table tr h2,
        .top-table tr h1 {
            margin: 0px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .top-table tr h2 {
            font-size: 15px;
            color: #606060;
        }

        .top-table tr td {
            border: none !important;
            padding: 0px !important;
        }

        .top-table tr:nth-child(1) {
            border: 1px solid #000 !important;
        }

        .top-table tr:nth-child(1) p {
            width: 33.33%;
            float: left;
            display: inline-block;
            font-size: 13px;
            text-align: left;
        }

        .top-table tr:nth-child(1) p.text-right {
            text-align: right;
        }

        .igst-table tbody>tr>td:nth-child(1) {
            width: 60%;
            float: left;
        }

        .igst-table tbody>tr>td {
            width: 40%;
            float: left;
        }

        .ten {
            border: 1px dashed #000;
            top: -8px;
            bottom: -8px;
            left: -8px;
            right: -8px;
            width: 80%;
            position: relative;
        }

        .logoWrapper {
            position: relative;
            display: flex;
            gap: 25px;
            justify-content: space-between;
            align-items: flex-end;
        }

        .border-blue {
            width: 20px;
            background-color: #8dd5f1;
            position: absolute;
            left: 0;
            height: 100vh;
            z-index: 99;

        }
        .toptbody{
            font-family: "Times New Roman";
        }


    </style>

</head>

<body>

<page size="A4" id="area-cv-resume" width="1000" style="margin: 0 auto;display: block;position: relative;">

    <!--        <div class="border-blue"></div>-->
    <div class="logoWrapper" style="margin-bottom: 5px;">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/top-logo.jpg" class="img-responsive" style="width: 100%">
    </div>


    <div class="patient-detail" style="padding-left: 15px;">
        <table style="border: 0;width: 100%;border-top: 2px solid #000;border-bottom: 2px solid #000;">
            <tbody class="toptbody">
            <tr>
                <td style="padding: 5px 10px 0 !important;width: 18%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Patient Name</p>
                </td>

                <td style="padding: 5px 10px 0 !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">Mr. HUSSAIN AHMAD</p>
                </td>

                <td style="padding: 5px 10px 0 !important;width: 18%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Patient Id</p>
                </td>

                <td style="padding: 5px 10px 0 !important;width: 40%;margin:0  1%;text-align: left;">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">20202233917</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; vertical-align: top;">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Sex / Age</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">Male/32 Yrs 03 Mon 25 Days</p>
                </td>

                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Registered On</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">05/01/2022 09:07:06</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left;vertical-align: top ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Ref. Dr</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; vertical-align: top ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">Dr. SELF*</p>
                </td>

                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left;vertical-align: top  ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Receiving On</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left;vertical-align: top  ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">05/01/2022 10:13:33</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Sample From</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">SELF</p>
                </td>

                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Printed On</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">05/01/2022 18:25:39</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Sample</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">Nasopharygeal &amp; Oropharyngeal Swabs</p>
                </td>

                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">SRF No.</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">07080006137173</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">DOB</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">10/09/1989 </p>
                </td>

                <td style="padding: 2px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Passport No.</p>
                </td>

                <td style="padding: 2px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">V1666839</p>

                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="tab-head" style="margin: 0px 0;width:100%;padding-left: 15px; ">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <table style="border: 0 solid #000; width: 100%;margin: 0px 0px;">
                        <tbody>
                        <tr style="height: 5px;"></tr>
                        <tr style="border-top: 1px solid #000;border-bottom: 1px solid #000;">
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <p style="margin: 0">Test Name</p>
                            </td>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <p style="margin: 0">Value</p>
                            </td>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <p style="margin: 0">Unit</p>
                            </td>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <p style="margin: 0">Biological Ref Interval</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <h4 style="display: inline-block;border-bottom: 1px solid #000;margin: 3px 0;">COVID-19 Virus Qualitative PCR #</h4>
                            </td>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <h4 style="margin: 0px 0;">NEGATIVE</h4>
                            </td>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <p></p>
                            </td>
                            <td style="padding:3px !important;margin:0  auto;text-align: left; ">
                                <p></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="box-table" style="margin-top: 00px;width: 100%;">
                        <thead>
                        <tr>
                            <td style="border: 0;padding:2px 5px;width: 50%;">
                                <h4 style="margin: 0px 0;"><strong>Interpretation-</strong></h4>
                            </td>
                            <td style="border: 0;padding:2px 5px;font-weight:normal;"></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="border: 0">
                            <td style="border:0;padding:2px 5px;">
                                <strong>Result</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">Interpretation</td>
                        </tr>

                        <tr style="border: 0">
                            <td style="border:0;padding: 0px 5px;">
                                <strong>Positive</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">RNA specific to SARS-COV-2 Detected</td>
                        </tr>

                        <tr style="border: 0">
                            <td style="border:0;padding:2px 5px;">
                                <strong>Negative</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">RNA specific to SARS-COV-2 Not Detected</td>
                        </tr>

                        <tr style="border: 0">
                            <td style="border:0;padding:2px 5px;">
                                <strong>Inconclusive</strong>
                            </td>
                            <td style="border:0;padding:2px 5px;">Inconclusive. This could be due to low viral load in the sample. A repeat sample is recommended for confirmation
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="box-table" style="margin: 10px 0 0; width: 100%;">
                        <thead>
                        <tr>
                            <td style="border: 0;padding-left:25px;width: 100%;">
                                <h4 style="margin: 0">ICMR Registration number for Covid – 19 is GPIPRD</h4>
                            </td>

                        </tr>
                        </thead>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>

        <table style="margin-top: 5px;">
            <tbody>
            <tr>
                <ol type="1">
                    <li>Negative result does not rule out the possibility of Covid – 19 infection. Presence of inhibitors, mutations &amp;
                            insufficient RNA specific to SARS- CoV-2 can influence the test result. Kindly correlate the results with clinical
                            findings. A negative result in a single upper respiratory tract sample does not rule out SARS-Cov-2 infection.
                            Hence in such cases a repeat sample should be sent. Lower respiratory tract samples like sputum, BAL, ET
                            aspirate are appropriate samples
                            especially in severe and progressive lung disease</li>
                    <li>Covid – 19 Test conducted by kits approved by ICMR</li>
                    <li>Kindly consult referring physician / Authorized hospitals for appropriate follow up.</li>
                    <li>Test conducted on Nasopharyngeal &amp; Oropharyngeal Swabs.</li>
                </ol>
              </tr>


            <tr style="height: 10px;"></tr>

            <tr>
                <td>
                    <p>Comments: Coronaviruses (CoV) are a large family of viruses that causes illness ranging from the common cold to more
                        severe diseases such as Middle East Respiratory Syndrome (MERS-CoV) and severe Acute Respiratory Syndrome
                        (SARS-CoV). Coronavirus disease (COVID-19) is a new strain that was discovered in 2019 and has not been previously
                        identified in humans. Common signs of infection include respiratory symptoms, fever, cough, shortness of breath and
                        breathing difficulties. In more severe cases, infection can cause pneumonia, severe acute respiratory syndrome and
                        kidney failure.</p>
                </td>
            </tr>

            <tr style="height: 30px;"></tr>

            </tbody>
        </table>

    </div>


    <div class="footer" style="width: 100%">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <div class="" style="display: flex;justify-content: space-between;">
                        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/footer-logo.jpg" style="width: 100%;">
                    </div>

                </td>
            </tr>
            </tbody>
        </table>
    </div>


</page>
</body>
</html>
