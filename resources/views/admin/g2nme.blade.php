<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Orpat</title>

    <style type="text/css">
        /*        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap');*/

        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        /*
        @font-face {
            font-family: "times new roman", serif;
            src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/times new roman.ttf);
        }

        @font-face {
            font-family: Arial, sans-serif;
            src: url(http://dev.firsteconomy.com/emailerImages/orpat/per/ARIAL.TTF);
        }
*/

        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-size: 14px;
            font-family: 'Roboto', sans-serif;
            /*            font-family: "times new roman", serif;*/
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }

        page[size="A4"] {
            width: 21cm;
            height: 29.7cm;
        }

        .text-center {
            text-align: center;
        }

        .table {
            width: 100%;
            max-width: 100%;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        table {
            background-color: transparent;
            border-spacing: 0;
            border-collapse: collapse;
        }

        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 4px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        th {
            text-align: left;
        }


        p {
            font-size: 13px;
            margin-bottom: 5px;
            margin-top: 0px;
        }

        .fnt-1 {
            margin: 10px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .table {
            margin: 0px;
        }

        .v-mdl {
            vertical-align: middle !important;
        }

        .v-top {
            vertical-align: top !important;
        }

        .no-vorder {
            border: none !important;
        }

        .bd-right-none {
            border-right: none !important;
        }

        .bd-left-none {
            border-left: none !important;
        }

        .invoice-wrapper {
            min-width: 680px;
        }

        .table-bordered,
        .table-bordered>tbody>tr>td,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>td,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border: 1px solid #000;
        }

        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border-top: 0;
            border: 1px solid #000;
        }

        .s-table thead tr th {
            vertical-align: middle;
        }

        .s-table thead tr th:nth-child(5),
        .s-table tbody tr td:nth-child(5) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(6),
        .s-table tbody tr td:nth-child(6) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(7),
        .s-table tbody tr td:nth-child(7) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(8),
        .s-table tbody tr td:nth-child(8) {
            width: 75px;
        }

        .s-table tbody.fnt-invoce tr td {
            font-size: 11px;
        }

        .igst-table {}

        .igst-table tbody tr th:nth-child(3) {
            width: 120px;
        }

        .igst-table tbody tr th {
            font-weight: 400;
        }

        .img-th {
            width: 150px;
        }

        .img-th img {
            max-width: 150px;
        }

        .first-table p {
            margin-bottom: 5px;
        }

        .first-table h4 {
            font-size: 14px;
            margin: 0;
        }

        .party-div {
            padding: 5px;
        }

        .fnt-invoce td {
            font-weight: 600;
        }

        .fnt-invoce td.nrml {
            font-weight: 400;
        }

        .first-table p span.caps {
            text-transform: uppercase;
        }

        .first-table h4 {
            color: #333;
        }

        .first-table h4.city {
            margin-right: 20px;
            display: inline-block;
            float: left;
        }

        .first-table h4.city strong {
            font-weight: 700;
            color: #000;
        }

        .first-table h3 {
            display: block;
            width: 100%;
            float: left;
            margin: 0 0 5px;
            font-size: 13px;
        }

        .right p {
            width: 50%;
            display: inline-block;
            margin-bottom: 5px;
            float: left;
        }

        .first-table tr h1 {
            margin: 5px 0px;
            text-transform: uppercase;
        }

        .top-table tr {
            padding: 2px 8px;
        }

        .top-table tr h2,
        .top-table tr h1 {
            margin: 0px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .top-table tr h2 {
            font-size: 15px;
            color: #606060;
        }

        .top-table tr td {
            border: none !important;
            padding: 0px !important;
        }

        .top-table tr:nth-child(1) {
            border: 1px solid #000 !important;
        }

        .top-table tr:nth-child(1) p {
            width: 33.33%;
            float: left;
            display: inline-block;
            font-size: 13px;
            text-align: left;
        }

        .top-table tr:nth-child(1) p.text-right {
            text-align: right;
        }

        .igst-table tbody>tr>td:nth-child(1) {
            width: 60%;
            float: left;
        }

        .igst-table tbody>tr>td {
            width: 40%;
            float: left;
        }

        .ten {
            border: 1px dashed #000;
            top: -8px;
            bottom: -8px;
            left: -8px;
            right: -8px;
            width: 80%;
            position: relative;
        }

        .logoWrapper {
            position: relative;
            display: flex;
            gap: 25px;
            justify-content: space-between;
            align-items: flex-end;
        }
        .patient-detail{
            font-family: "Times New Roman";
        }

    </style>

</head>

<body>
<page size="A4" id="area-cv-resume" width="1000" style="margin: 0 auto;display: block;position: relative;">

    <div class="logoWrapper" style="margin-bottom: 10px;">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/1.jpeg" class="img-responsive" style="width: 250px">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/2.jpeg" class="img-responsive" style="width: 160px">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/3.jpeg" class="img-responsive" style="width: 120px">
        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/4.jpeg" class="img-responsive" style="width: 80px;margin-bottom: 40px;margin-left: -80px">
    </div>


    <div class="patient-detail">
        <table style="border: 0;width: 100%;border-top: 2px solid #000;border-bottom: 2px solid #000;">
            <tbody class="toptbody">
            <tr>
                <td style="padding: 5px 10px !important;width: 18%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Registration Date</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">: 23/11/2021</p>
                </td>

                <td style="padding: 5px 10px !important;width: 18%;margin:0  1%;text-align: left;">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Patient Id Ref No.</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">: N9169940</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left; vertical-align: top;">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Registration ID </p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">: 1021785</p>
                </td>

                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Mobile Number</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">: 9088176443</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;vertical-align: top ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Patient Name</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; vertical-align: top ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">: Mr. MOHAMMAD HAFIZ</p>
                </td>

                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left;vertical-align: top  ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Collected On</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left;vertical-align: top  ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">: 23/11/2021 11:02:26</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Sex/Age</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">: Male / 29 Yrs</p>
                </td>

                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Received On</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">: 23/11/2021 12:09:34</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">SRF No.</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">: 0606202185911</p>
                </td>

                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Reported On</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;">: 23/11/2021 18:56:16</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Sample type</p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:normal;font-size:14px;">: Nasopharyngeal &amp; Oropharyngeal </p>
                </td>

                <td style="padding: 5px 10px !important;width: 14%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;"></p>
                </td>

                <td style="padding: 5px 10px !important;width: 40%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:normal;font-size:14px;"></p>

                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="tab-head" style="margin: 0px 0;width:100%; ">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <table style="border: 0 solid #000; width: 100%;margin: 0px 0px;">
                        <tbody>
                        <tr>
                            <td style="padding:0px !important;margin:0  auto;text-align: center; ">
                                <h2 style="margin: 0px;font-size: 18px;padding: 5px 0px;">MOLECULAR BIOLOGY</h2>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="box-table" style="margin-top: 00px;width: 100%;">
                        <thead>
                        <tr>
                            <td style="border: 0;padding:4px 5px;width: 50%;">
                                <strong>COVID 19- RT PCR</strong>
                            </td>
                            <td style="border: 0;padding:4px 5px;font-weight:600;"></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="border: 0">
                            <td style="border:0;padding:4px 5px;">
                                <strong>E GENE * </strong>
                            </td>
                            <td style="border:0;padding:4px 5px;">Negative</td>
                        </tr>

                        <tr style="border: 0">
                            <td style="border:0;padding:4px 5px;">
                                <strong>RdRp Gene N Gene *</strong>
                            </td>
                            <td style="border:0;padding:4px 5px;">Negative</td>
                        </tr>

                        <tr style="border: 0">
                            <td style="border:0;padding:4px 5px;">
                                <strong>N Gene *</strong>
                            </td>
                            <td style="border:0;padding:4px 5px;">Negative</td>
                        </tr>

                        <tr style="border: 0">
                            <td style="border:0;padding:4px 5px;">
                                <strong>FINAL RESULT OF SARS-CoV2</strong>
                            </td>
                            <td style="border:0;padding:4px 5px;">Negative
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <table class="box-table" style="margin: 10px 0; width: 100%;">
                        <thead>
                        <tr>
                            <td style="border: 0;padding:2px 5px;width: 50%;">
                                <strong>Results Interpretation guideline</strong>
                            </td>
                            <td style="border: 0;padding:2px 5px;font-weight:600;"></td>
                        </tr>
                        </thead>
                    </table>

                    <table class="box-table" style="margin-top: 00px;width: 100%;">
                        <thead>
                        <tr style="border: 1px solid #000;">
                            <td style="border: 0;padding:2px 5px;font-weight:600;width: 50%;">Result</td>
                            <td style="border: 0;padding:2px 5px;font-weight:600;">Remarks</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="border: 1px solid #000;">
                            <td style="border:0;padding:2px 5px;">
                                Positive
                            </td>
                            <td style="border:0;padding:2px 5px;">RNA Specific to SARS-CoV-2 Present in sample</td>
                        </tr>

                        <tr style="border: 1px solid #000;">
                            <td style="border:0;padding:2px 5px;">
                                Negative
                            </td>
                            <td style="border:0;padding:2px 5px;">RNA Specific to SARS-CoV-2 Not present in sample</td>
                        </tr>

                        <tr style="border: 1px solid #000;">
                            <td style="border:0;padding:2px 5px;">
                                Inconclusive
                            </td>
                            <td style="border:0;padding:2px 5px;">A repeat sample is required for confirmation
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>

        <table style="margin-top: 10px;">
            <tbody>
            <tr>
                <td>
                    <p><strong>NOTE:</strong></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>1. Negative Control &amp; Positive Control are used in each assay.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>2. Sample may be considered as positive for covid-19 if targets from E gene and, either RdRp gene or N Gene or both, results in CT value <span> &lt; 35</span>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>3. Positive results do not rule out bacterial infection or co-infection with other viruses.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>4. Negative result does not rule out the possibility of 2019-nCoV Infection. Ct values differ from
                        one kit to the other.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>5. Sample collection deviations, transportation, presence of inhibitors/mutations &amp; insufficient
                        organism RNA can influence the PCR amplification and result.</p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>6. Covid- 19 Qualitative RT-PCR test is conducted using the kit validated and approved by ICMR
                    </p>

                </td>
            </tr>

            <tr>
                <td>
                    <p>7. Kindly consul treferring Physician / Authorized hospital for appropriate follow up.</p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>8. This is a qualitative test.The Ct values do not provide a measure of viral load due to inherent
                        variability in sampling and kits. As per ICMR guidelines, it is not recommended to rely on
                        numerical Ct values for determining infectiousness of COVID-19 patients and deciding patient management protocols.</p>
                </td>
            </tr>

            <tr style="height: 20px;"></tr>

            <tr>
                <td>
                    <p><strong>COMMENTS:</strong> COVID-19 virus is type a of Coronaviruses (CoV) and belong to largefamily of viruses that
                        cause illness ranging from the common cold to more severe diseases such as Severe Acute Respiratory
                        Syndrome (SARS-CoV) &amp; Middle East Respiratory Syndrome (MERS
                        -CoV). COVID-19 is a new strain, discovered in 2019 and has not been previously identified in
                        humans. Common signs of infection include respiratory symptoms, fever, cough, shortness of breath
                        and breathing difficulties. In more severe cases, infection can cause pneumonia, severe acuter
                        espiratory syndrome and kidney failure. <strong>(ICMR Registration Id for COVID-19 Testing : G2MPLGH)</strong></p>
                </td>
            </tr>

            <tr>
                <td>
                    <p><strong>Test Performed at: Genes2Me Pvt Ltd, R004, Ground floor, Blk-2, M3M Cosmopolitan, Sector 66, Gurgaon - 122018</strong></p>
                </td>
            </tr>

            <tr style="height: 20px;"></tr>
            <tr>
                <td>
                    <p style="text-align: center"><strong>*** End of Report ***</strong></p>
                </td>
            </tr>

            <tr style="height: 20px;"></tr>

            </tbody>
        </table>

    </div>


    <div class="footer" style="width: 100%">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <div class="" style="display: flex;justify-content: space-between;margin-top: 90px">
                        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/firstSign.jpeg" style="width: 170px;">
                        <img src="http://dev.firsteconomy.com/emailerImages/orpat/8-1-2021/secondSign.jpeg" style="width: 170px;">
                    </div>

                </td>
            </tr>
            </tbody>
        </table>
    </div>


</page>
</body>

</html>
