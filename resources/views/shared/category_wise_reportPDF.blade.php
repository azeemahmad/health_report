

<!DOCTYPE html>
<html>
<body>
<style>
     @media print {      
        .date_container {
          display: none;
       }
       .print_btn {
           display: none;
       }
       .refresh_btn{
        display: none;

       }
       .main-footer, .dataTables_filter, .dt-buttons{
           display: none;
       }

       .table thead tr td,.table tbody tr td{
            border-width: 1px !important;
            border-style: solid !important;
            border-color: black !important;
            font-size: 8px !important;           
        }
        @page 
        { size: landscape; 
        }
    }

</style>
<!-- 
<link rel="stylesheet" type="text/css"  href="{{ asset ("/datatable/datatables.min.css") }}" />
<link rel="stylesheet" type="text/css"  href="{{ asset ("/datatable/buttons.dataTables.min.css") }}" /> -->


    <div class="container date_container">
        <div class="row">
            <div class="col-md-11">

                    <div class="panel-body">
                        <div style="color: #fff; background-color: #337ab7;border-color: #337ab7;    line-height: 35px;border-radius: 5px;text-align: center;"><b> Category Wise Report
                        </b></div>
                    </div>
                </div>
            </div>
        </div>
        @if(count($category)>0)
          <style>
                        table.printarea, th, td {
                border-bottom: 0 !important;
                }
                .responsive tbody td {
                border-top: 0 !important;
                }
                            table.printarea, th, td {
                        border: 0.5px solid black !important;
                        text-align: center;

                        }

                        tr.noBorder td {
                border: 0 !important;
                }
                /* .buttons-html5{
                    display: n
                } */        
         </style>
        <div class="container-fluid ">
            <h2>Category Wise Report</h2>            
        
            <span style="margin:2px;"></span>       
         
            <div style="overflow-x:auto;    padding: 0px 13px;">

        <table class="table responsive display row-border hover" id="data-table"  style="width:100%;border:none;border-collapse: collapse;">
            
                       
                <tr>  
                    <th style="width: 15%">Sub Category</th>
                    <th style="width: 5%">Opening Total</th>
                    <th style="width: 5%">Production</th>
                    <th style="width: 5%">Total</th>
                    <th style="width: 5%">Integrated Qty </th>
                    <th style="width: 5%">State Qty </th>
                    <th style="width: 5%">Nepal Qty</th>
                    <th style="width: 5%">Export Qty</th>    
                    <th style="width: 5%">Integrated Taxable Value</th>
                    <th style="width: 5%">State Taxable Value</th>
                    <th style="width: 5%">Nepal Value</th>
                    <th style="width: 5%">Export Value</th>
                    <th style="width: 5%">Total Taxable Value</th>
                    <th style="width: 5%">CGST</th>
                    <th style="width: 5%">SGST</th>
                    <th style="width: 5%">IGST</th>                  
                    <th style="width: 5%">GRAND TOTAL</th>                  
                    <th style="width: 5%">Closing Total</th>                    
                  </tr>  
            
            <tbody>

                <?php                
                    $previous_date = date('Y-m-d',strtotime('-1 day', strtotime($start_date)));
                    $grand_opening_total = 0;
                    $grand_production_total = 0;
                    $grand_total = 0;
                    $grand_integrated_quantity = 0;
                    $grand_state_quantity = 0;
                    $grand_nepal_quantity = 0;
                    $grand_nepal_quantity          =0;
                    $grand_export_quantity        =0;
                    $grand_integrated_taxable_value =0;
                    $grand_state_taxable_value      =0;
                    $grand_nepal_taxable_value        =0;
                    $grand_export_taxable_value      =0;
                    $grand_total_taxable_amount                 =0;
                    $grand_state_tax                       =0;
                    $grand_integrated_tax                  =0;
                    $grand_closing_quantity                        = 0;
                    $grand_total_cost=0;
                ?>


                <tr> 
                     <td colspan="18" style="color: red"><b> FINISH GOODS PRDODUCT SUMMARY </b></td>
                </tr>
        
        @foreach($category as $cat) 
           <tr>  
               <td colspan="18" style="color:darkred"><b>{{$cat->name}}</b></td>
           </tr>

            <?php 
            $subcategories = $cat->subcategories->pluck('name','id')->toArray();
            $sub_opening_total = 0;
            $sub_production_quantity = 0;
            $sub_total = 0;
            $sub_integrated_quantity = 0;
            $sub_state_quantity = 0;
            $sub_nepal_quantity =0;
            $sub_export_quantity=0;
            $sub_integrated_taxable_value =0;
            $sub_state_taxable_value=0;
            $sub_nepal_taxable_value=0;
            $sub_export_taxable_value =0;
            $sub_total_taxable_amount=0;
            $sub_state_tax =0;
            $sub_integrated_tax=0;
            $sub_total_cost=0;
            $sub_closing_quantity=0;
            ?>
        
        @foreach ($subcategories as $sub_category_id => $subcategory) 
        
        <?php 
            
            $opening_total                          = 0;
            $closing_total                          = 0;
            $production_quantity             = 0;
            $total                                  = 0;
            $closing_quantity                          = 0;
            $exports_old_sold_quanity               = 0;
            $old_production_stock                   = 0;
            $sform_old_sold_quanity                 = 0;
            $nepal_old_sold_quanity                 = 0;
            $integrated_quantity          = 0;
            $integrated_taxable_value   = 0;
            $state_quantity               = 0;
            $state_taxable_value        = 0;
            $export_quantity          = 0;
            $nepal_quantity            = 0;
            $total_taxable_amount                   = 0;
            $integrated_tax                    = 0;
            $state_tax                         = 0;
            $export_taxable_value         = 0;
            $nepal_taxable_value           = 0;

                $previous_date = date('Y-m-d',strtotime('-1 day', strtotime($start_date)));
                
                $subV = App\Category::find($sub_category_id);

                foreach($subV->productSucategory as $prod)
                    {   
                        $opening = App\Models\ReportData::where('is_part',0)->where('product_id',$prod->id)->whereDate('date', '<=', $previous_date)->orderBy('date','desc')->first();
                       
                        if($opening)
                        {  
                            $opening_total += $opening->closing_quantity;
                        }
                    }

                $production_quantity =$records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('production_quantity');

                $integrated_quantity = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('integrated_quantity');
                $state_quantity = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('state_quantity');
                $nepal_quantity = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('nepal_quantity');
                $export_quantity = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('export_quantity');
                $total_quantity = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('total_quantity');
                $integrated_taxable_value = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('integrated_taxable_value');
                $state_taxable_value = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('state_taxable_value');
                $nepal_taxable_value = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('nepal_taxable_value');
                $export_taxable_value = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('export_taxable_value');
                $export_taxable_value = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('export_taxable_value');

                $total_taxable_amount = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('total_taxable_amount');
                $state_tax  = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('state_tax');
                $integrated_tax   = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('integrated_tax');
                $total_cost   = $records->where('is_part',0)->where('sub_category_id', $sub_category_id)->sum('total_cost');
                

                $total  =$production_quantity +$opening_total;
                $closing_quantity = $total-$total_quantity ;
        ?>
        
            <tr >
                <td><b>{{$subcategory}}</b></td>
                <td>{{$opening_total}}                  <?php $sub_opening_total +=$opening_total; $grand_opening_total +=$opening_total;  ?></td>
                <td>{{$production_quantity}}            <?php $sub_production_quantity+= $production_quantity ; $grand_production_total +=$production_quantity;?></td>
                <td>{{$total}}                          <?php $sub_total+=$total; $grand_total +=$total; ?></td>
                <td>{{$integrated_quantity}}            <?php $sub_integrated_quantity +=$integrated_quantity; $grand_integrated_quantity +=$integrated_quantity; ?></td>
                <td>{{$state_quantity}}                 <?php $sub_state_quantity +=  $state_quantity ;$grand_state_quantity +$state_quantity?></td>                
                <td>{{$nepal_quantity }}                <?php $sub_nepal_quantity += $nepal_quantity;$grand_nepal_quantity +=$nepal_quantity; ?></td>
                <td>{{$export_quantity }}</td>          <?php $sub_export_quantity+=$export_quantity;$grand_export_quantity +=$export_quantity; ?>
                <td>{{$integrated_taxable_value}}</td>  <?php $sub_integrated_taxable_value +=$integrated_taxable_value;$grand_integrated_taxable_value        +=$integrated_taxable_value; ?>
                <td>{{$state_taxable_value}}</td>       <?php $sub_state_taxable_value+=$state_taxable_value;$grand_state_taxable_value             +=$state_taxable_value; ?>                
                <td>{{$nepal_taxable_value}}  </td>     <?php $sub_nepal_taxable_value +=$nepal_taxable_value;$grand_nepal_taxable_value               +=$nepal_taxable_value; ?>
                <td>{{$export_taxable_value}}</td>      <?php $sub_export_taxable_value +=$export_taxable_value;$grand_export_taxable_value             +=$export_taxable_value; ?>
                <td>{{$total_taxable_amount}}</td>      <?php $sub_total_taxable_amount +=$total_taxable_amount;$grand_total_taxable_amount                        +=$total_taxable_amount; ?>
                <td>{{  round($state_tax /2,2)}}</td>           
                <td>{{  round($state_tax /2,2)}}</td>  <?php $sub_state_tax +=$state_tax; $grand_state_tax                              +=$state_tax ; ?>
                <td>{{  round($integrated_tax ,2)}}            <?php $sub_integrated_tax +=$integrated_tax ; $grand_integrated_tax                         += $integrated_tax ;  ?></td>
                <td>{{$total_cost}} <?php $sub_total_cost += $total_cost; $grand_total_cost+=$total_cost; ?></td>
                <td>{{$closing_quantity}}                              <?php $sub_closing_quantity+=$closing_quantity; $grand_closing_quantity                               +=$closing_quantity; ?></td>
           
            </tr>

                @endforeach

                <tr style="    background: black;
                color: white;" >  
                    <th style="border-color: white !important">TOTAL {{$cat->name}}</th>
                    <th style="border-color: white !important">{{$sub_opening_total }}</th>
                    <th style="border-color: white !important">{{$sub_production_quantity}}</th>
                    <th style="border-color: white !important">{{$sub_total}}</th>
                    <th style="border-color: white !important">{{$sub_integrated_quantity}}</th>
                    <th style="border-color: white !important">{{ $sub_state_quantity }}</th>
                    <th style="border-color: white !important">{{$sub_nepal_quantity}}</th>
                    <th style="border-color: white !important">{{$sub_export_quantity}}</th>
                    <th style="border-color: white !important">{{$sub_integrated_taxable_value }}</th>
                    <th style="border-color: white !important">{{$sub_state_taxable_value}}</th>
                    <th style="border-color: white !important">{{$sub_nepal_taxable_value}}</th>
                    <th style="border-color: white !important">{{$sub_export_taxable_value }}</th>
                    <th style="border-color: white !important">{{$sub_total_taxable_amount}}</th>
                    <th style="border-color: white !important">{{  round($sub_state_tax /2,2)}}</th>
                    <th style="border-color: white !important">{{  round($sub_state_tax /2,2)}}</th>
                    <th style="border-color: white !important">{{  round( $sub_integrated_tax,2)}}</th>
                    <th style="border-color: white !important">{{  round( $sub_total_cost,2)}}</th>

                    <th style="border-color: white !important">{{$sub_closing_quantity}}</th>
                    
                  </tr>

                @endforeach
            <?php 
                              
                              $truck_freight_charges= App\ExportOrderDetail::whereDate('created_at','>=',$start_date)
                        ->whereDate('created_at','<=', $end_date)
                        ->whereNotNull('invoice_no')->sum('truck_freight_charges');

            ?>

                <tr class="danger">
                    
                    <td style="text-align: left;text-transform:uppercase"><b>Ocean Freight and Other Charges</b></td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>

                    <td style="text-align: right;">{{round($truck_freight_charges,2)}} <?php
                    $grand_total_taxable_amount +=$truck_freight_charges;
                    $grand_export_taxable_value +=$truck_freight_charges;
                    $grand_total_cost+=$truck_freight_charges;

                    
                    ?></td>
                    <td>{{round($truck_freight_charges,2)}}</td>
              
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>

                </tr>
                <tr style="   background: darkgreen;
                color: white;">
                    <td><b>TOTAL GOODS PRDODUCT</b></td>
                    <td><b>{{  $grand_opening_total}}</b></td>
                    <td><b>{{  $grand_production_total}}</b></td>
                    <td><b>{{  $grand_total}}</b></td>
                    <td><b>{{  $grand_integrated_quantity}}</b></td>
                    <td><b>{{  $grand_state_quantity}}</b></td>
                    <td><b>{{  $grand_nepal_quantity }}</b></td>
                    <td><b>{{  $grand_export_quantity }}</b></td>
                    <td><b>{{  round($grand_integrated_taxable_value,2)}}</b></td>
                    <td><b>{{  round($grand_state_taxable_value ,2)}}</b></td>
                    <td><b>{{  round($grand_nepal_taxable_value,2)}}</b></td>
                    <td><b>{{  round($grand_export_taxable_value,2)}}</b></td>
                    <td><b>{{  round($grand_total_taxable_amount,2)  }}</b></td>
                    <td><b>{{  round($grand_state_tax /2,2)}}{{''}}</b></td>
                    <td><b>{{  round($grand_state_tax /2,2)}}{{''}}</b></td>
                    <td><b>{{  round($grand_integrated_tax ,2)}}</b></td>
                    <td><b>{{  round($grand_total_cost ,2)}}</b></td>

                    <td><b>{{  round($grand_closing_quantity,2)}}</b></td>
                  
                </tr>
              

                
            <?php 
            $p_grand_opening_total = 0;
            $p_grand_production_total = 0;
            $p_grand_total = 0;
            $p_grand_integrated_quantity = 0;
            $p_grand_state_quantity = 0;
            $p_grand_nepal_quantity = 0;
            $p_grand_nepal_quantity          =0;
            $p_grand_export_quantity        =0;
            $p_grand_integrated_taxable_value =0;
            $p_grand_state_taxable_value      =0;
            $p_grand_nepal_taxable_value        =0;
            $p_grand_export_taxable_value      =0;
            $p_grand_total_taxable_amount                 =0;
            $p_grand_state_tax                       =0;
            $p_grand_integrated_tax                  =0;
            $p_grand_closing_quantity                        = 0;
            $p_grand_total_cost=0;
        ?>

            <tr>
                <td colspan="18"></td>
            </tr>
               <tr>  
                 <td colspan="18" style="color: red"><b> FINISH PART SUMMARY </b></td>
               </tr>

    <?php 

        
        $opening_total                          = 0;
        $production_quantity             = 0;
        $total                                  = 0;
        $closing_quantity                          = 0;
        $exports_old_sold_quanity               = 0;
        $old_production_stock                   = 0;
        $sform_old_sold_quanity                 = 0;
        $nepal_old_sold_quanity                 = 0;
        $integrated_quantity          = 0;
        $integrated_taxable_value   = 0;
        $state_quantity               = 0;
        $state_taxable_value        = 0;
        $export_quantity          = 0;
        $nepal_quantity            = 0;
        $total_taxable_amount                   = 0;
        $integrated_tax                    = 0;
        $state_tax                         = 0;
        $export_taxable_value         = 0;
        $nepal_taxable_value           = 0;
?>
              
        @foreach($category as $cat)  
        <?php 

            $production_quantity =$records->where('is_part',1)->where('category_id', $cat->id)->sum('production_quantity');
            $integrated_quantity = $records->where('is_part',1)->where('category_id', $cat->id)->sum('integrated_quantity');
            $state_quantity = $records->where('is_part',1)->where('category_id', $cat->id)->sum('state_quantity');
            $nepal_quantity = $records->where('is_part',1)->where('category_id', $cat->id)->sum('nepal_quantity');
            $export_quantity = $records->where('is_part',1)->where('category_id', $cat->id)->sum('export_quantity');
            $integrated_taxable_value = $records->where('is_part',1)->where('category_id', $cat->id)->sum('integrated_taxable_value');
            $state_taxable_value = $records->where('is_part',1)->where('category_id', $cat->id)->sum('state_taxable_value');
            $nepal_taxable_value = $records->where('is_part',1)->where('category_id', $cat->id)->sum('nepal_taxable_value');
            $export_taxable_value = $records->where('is_part',1)->where('category_id', $cat->id)->sum('export_taxable_value');
            $export_taxable_value = $records->where('is_part',1)->where('category_id', $cat->id)->sum('export_taxable_value');

            $total_taxable_amount = $records->where('is_part',1)->where('category_id', $cat->id)->sum('total_taxable_amount');
            $state_tax  = $records->where('is_part',1)->where('category_id', $cat->id)->sum('state_tax');
            $integrated_tax   = $records->where('is_part',1)->where('category_id', $cat->id)->sum('integrated_tax');
            $total_cost   = $records->where('is_part',1)->where('category_id', $cat->id)->sum('total_cost');            

            $total  =$production_quantity ;
    ?>
    
        <tr >
            <td><b>{{$cat->name}}</b></td>
            <td>0</td>
            <td>{{$production_quantity}}            <?php $p_grand_production_total                +=$production_quantity;        $grand_production_total                +=$production_quantity;        ?></td>
            <td>{{$total}}                          <?php $p_grand_total                           +=$total;                      $grand_total                           +=$total;                      ?></td>
            <td>{{$integrated_quantity}}            <?php $p_grand_integrated_quantity             +=$integrated_quantity;        $grand_integrated_quantity             +=$integrated_quantity;        ?></td>
            <td>{{$state_quantity}}                 <?php $p_grand_state_quantity                  +=$state_quantity;             $grand_state_quantity                  +=$state_quantity;              ?></td>                
            <td>{{$nepal_quantity }}                <?php $p_grand_nepal_quantity                  +=$nepal_quantity;             $grand_nepal_quantity                  +=$nepal_quantity;             ?></td>
            <td>{{$export_quantity }}</td>          <?php $p_grand_export_quantity                 +=$export_quantity;            $grand_export_quantity                 +=$export_quantity;            ?>
            <td>{{$integrated_taxable_value}}</td>  <?php $p_grand_integrated_taxable_value        +=$integrated_taxable_value;   $grand_integrated_taxable_value        +=$integrated_taxable_value;   ?>
            <td>{{$state_taxable_value}}</td>       <?php $p_grand_state_taxable_value             +=$state_taxable_value;        $grand_state_taxable_value             +=$state_taxable_value;        ?>                
            <td>{{$nepal_taxable_value}}  </td>     <?php $p_grand_nepal_taxable_value             +=$nepal_taxable_value;        $grand_nepal_taxable_value             +=$nepal_taxable_value;        ?>
            <td>{{$export_taxable_value}}</td>      <?php $p_grand_export_taxable_value            +=$export_taxable_value;       $grand_export_taxable_value            +=$export_taxable_value;       ?>
            <td>{{$total_taxable_amount}}</td>      <?php $p_grand_total_taxable_amount            +=$total_taxable_amount;       $grand_total_taxable_amount            +=$total_taxable_amount;       ?>
            <td>{{  round($state_tax /2,2)}}</td>           
            <td>{{  round($state_tax /2,2)}}</td>   <?php $p_grand_state_tax                       +=$state_tax ;                 $grand_state_tax                       +=$state_tax ;                 ?>
            <td>{{  round($integrated_tax ,2)}}     <?php $p_grand_integrated_tax                  += $integrated_tax ;           $grand_integrated_tax                  += $integrated_tax ;           ?></td>
            <td>{{$total_cost}}                     <?php $p_grand_total_cost                      +=$total_cost;                 $grand_total_cost                      +=$total_cost;                 ?></td>
            <td>0</td>
       
        </tr>

        @endforeach

            <tr style="   background: darkgreen;
            color: white;">
                <td><b>TOTAL PARTS </b></td>
                <td><b>{{  $p_grand_opening_total}}</b></td>
                <td><b>{{  $p_grand_production_total}}</b></td>
                <td><b>{{  $p_grand_total}}</b></td>
                <td><b>{{  $p_grand_integrated_quantity}}</b></td>
                <td><b>{{  $p_grand_state_quantity}}</b></td>
                <td><b>{{  $p_grand_nepal_quantity }}</b></td>
                <td><b>{{  $p_grand_export_quantity }}</b></td>
                <td><b>{{  round($p_grand_integrated_taxable_value,2)}}</b></td>
                <td><b>{{  round($p_grand_state_taxable_value ,2)}}</b></td>
                <td><b>{{  round($p_grand_nepal_taxable_value,2)}}</b></td>
                <td><b>{{  round($p_grand_export_taxable_value,2)}}</b></td>
                <td><b>{{  round($p_grand_total_taxable_amount,2)  }}</b></td>
                <td><b>{{  round($p_grand_state_tax /2,2)}}{{''}}</b></td>
                <td><b>{{  round($p_grand_state_tax /2,2)}}{{''}}</b></td>
                <td><b>{{  round($p_grand_integrated_tax ,2)}}</b></td>
                <td><b>{{  round($p_grand_total_cost ,2)}}</b></td>

                <td><b>{{  round($p_grand_closing_quantity,2)}}</b></td>
              
            </tr>
            <tr>
                <td colspan="18"></td>
            </tr>

                <tr style="   background: rgb(136, 14, 85); color: white;">
                    <td><b>GRAND TOTAL </b></td>
                    <td><b>{{  $grand_opening_total}}</b></td>
                    <td><b>{{  $grand_production_total}}</b></td>
                    <td><b>{{  $grand_total}}</b></td>
                    <td><b>{{  $grand_integrated_quantity}}</b></td>
                    <td><b>{{  $grand_state_quantity}}</b></td>
                    <td><b>{{  $grand_nepal_quantity }}</b></td>
                    <td><b>{{  $grand_export_quantity }}</b></td>
                    <td><b>{{  round($grand_integrated_taxable_value,2)}}</b></td>
                    <td><b>{{  round($grand_state_taxable_value ,2)}}</b></td>
                    <td><b>{{  round($grand_nepal_taxable_value,2)}}</b></td>
                    <td><b>{{  round($grand_export_taxable_value,2)}}</b></td>
                    <td><b>{{  round($grand_total_taxable_amount,2)  }}</b></td>
                    <td><b>{{  round($grand_state_tax /2,2)}}{{''}}</b></td>
                    <td><b>{{  round($grand_state_tax /2,2)}}{{''}}</b></td>
                    <td><b>{{  round($grand_integrated_tax ,2)}}</b></td>
                    <td><b>{{  round($grand_total_cost ,2)}}</b></td>

                    <td><b>{{  round($grand_closing_quantity,2)}}</b></td>              
                </tr>

            </tbody>
        </table>
        </div>
    </div>


    @endif
    </body>
</html>

@section('scripts')

<!-- <script src="{{ asset ("/datatable/jszip.min.js") }}"></script>
<script src="{{ asset ("/datatable/dataTables.min.js") }}"></script>
<script src="{{ asset ("/datatable/dataTables.buttons.min.js") }}"></script>
<script src="{{ asset ("/datatable/buttons.html5.min.js") }}"></script> -->


  <script>


        $(document).ready(function() {

        });
    </script>
@endsection
