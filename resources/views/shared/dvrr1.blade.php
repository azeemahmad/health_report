
@extends('shared.master1')
@section('content')

<body>
<link rel="stylesheet" type="text/css"  href="http://localhost:8000/datatable/datatables.min.css" />
<link rel="stylesheet" type="text/css"  href="http://localhost:8000/datatable/buttons.dataTables.min.css" />

    <div class="container date_container">
        <div class="row">
            <div class="col-md-11">
                    <div class="panel-body">
                        <div style="color: #fff; background-color: #337ab7;border-color: #337ab7;    line-height: 35px;border-radius: 5px;text-align: center;"><b>Daily Report</b></div>
                    </div>

                    <div class="panel-body">
                        <div style="color: #fff; background-color: #337ab7;border-color: #337ab7;    line-height: 35px;border-radius: 5px;text-align: center;"><b>Division Wise Revenue Report</b></div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    {!! $division_wise_revenue_report_chart->container() !!}
                </div>
                <div class="col-md-6">
                    <table class="table table-striped w-auto" id="example">
        
                        <!--Table head-->
                        <thead>
                          <tr>
                            <th>Division</th>
                            <th>Amount</th> 
                            <th>Percent (%)</th>                          
                          </tr>
                        </thead>                      
                        <tbody>
                            <?php
                                $total = 0; 
                                $totals = 0;
                                $percent = 0.0; 
                            ?>
                            @foreach ($recordsDetails as $key => $items)
                            <?php $totals += $items; ?>
                            @endforeach

                            @foreach ($recordsDetails as $key => $item)
                            <?php                               
                                $total += $item;
                                if($totals != 0)
                                {
                                    $percent = $item * 100 / $totals; 
                                }
                                else
                                {
                                    $percent = 0.0;  
                                }        
                            ?>
                                
                              <tr class="table-info">
                                <td>{{$key}}</td>
                                <td>&#8377; {{$item}}</td>      
                                <td>{{number_format($percent,2)}} %</td>                    
                              </tr>
                            @endforeach
                              <tr style=" background: rgb(136, 14, 85); color: white;">
                                <td>GRAND TOTAL</td>
                                <td>&#8377; {{$total}}</td>  
                                <td>100 %</td>                         
                              </tr>                        
                        </tbody>  
                      </table>
                </div>            
            </div>
        </div>   
   </div>
@endsection
@section('scripts')
<script src="{{ asset ("/datatable/jszip.min.js") }}"></script>
<script src="{{ asset ("/datatable/dataTables.min.js") }}"></script>
<script src="{{ asset ("/datatable/dataTables.buttons.min.js") }}"></script>
<script src="{{ asset ("/datatable/buttons.html5.min.js") }}"></script>
<script src="{{ asset ("/datatable/buttons.print.min.js") }}"></script>
<script src="{{ asset ("/datatable/pdfmake.min.js") }}"></script>
<script src="{{ asset ("/datatable/vfs_fonts.js") }}"></script>

    <script>
        $(document).ready(function() {
            document.title = 'Division Wise Revenue Report ' +new Date(); 
            $('#example').DataTable( {
                dom: 'Bfrtip',
                "bPaginate": false,
                "ordering": false,
                'name' :'sds',
               
                buttons: [
                    'copy', 'csv', 'excel', 'pdfHtml5', 'print'
                ]                
            } );
        });
    </script>
@endsection

