<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                {{-- @if(Auth::guard('web')->user()->image)
                    <img src="{{env('LOCAL_URL').'images/profile_image'.'/'.Auth::guard('web')->user()->image}}"
                         class="img-circle"
                         alt="User Image"/>
                @else --}}
                <img src="{{ asset("/bower_components/admin-lte/dist/img/avatar5.png") }}" class="img-circle"
                     alt="User Image"/>
                {{-- @endif --}}
            </div>
            <div class="pull-left info">
                <p>{{ ucwords(Auth::guard('web')->user()->name)}}</p>
                {{Auth::guard('web')->user()->roles->implode('name', ', ')}}
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Admin-Panel</li>
            @if(Auth::user()->roles->first()->name=='Dispatch')
                <li class="{{ (request()->is('admin/home')) ? 'active' : '' }}"><a href="{{url('/admin/home')}}"><i
                                class="fa fa-home"></i> DashBoard</a></li>

            @endif
            @if(Auth::user()->roles->first()->name=='Admin')

                <li class="{{ (request()->is('admin/home')) ? 'active' : '' }}"><a href="{{url('/admin/home')}}"><i
                                class="fa fa-home"></i> DashBoard</a></li>
                <li class="{{ (request()->is('admin/graph')) ? 'active' : '' }}"><a href="{{url('/admin/graph')}}"><i
                                class="fa fa-home"></i> Report </a></li>

                <li class="treeview {{(request()->is('admin/expensecategories*')) ||(request()->is('admin/creditnoteremarks*')) ||(request()->is('admin/rq_subjects*')) ||(request()->is('admin/invocepartsmasters*')) ||(request()->is('admin/invocemasters*')) ||(request()->is('admin/financialyears*')) || (request()->is('admin/divisions*')) ||(request()->is('admin/dispatch_transport_details*')) || (request()->is('admin/locations/state*')) || (request()->is('admin/locations/city*')) ||(request()->is('admin/locations/pincode*'))||  (request()->is('admin/offers*')) || (request()->is('admin/products*')) ||(request()->is('admin/product/bulkupload*')) ||(request()->is('admin/product/bulk_products_update*')) || (request()->is('admin/regions*')) || (request()->is('admin/gsts*')) || (request()->is('admin/categories*')) || (request()->is('admin/warehouses*')) || (request()->is('admin/racks*')) || (request()->is('admin/orderstatuses*')) || (request()->is('admin/colors*')) || (request()->is('admin/months*')) || (request()->is('admin/parts*')) || (request()->is('admin/hsns*')) || (request()->is('admin/pincodes*'))  ? 'menu-open' : '' }}  ">
                    <a href="#"><i class="fa fa-gear "></i> Master <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{(request()->is('admin/expensecategories*')) ||(request()->is('admin/creditnoteremarks*')) ||(request()->is('admin/rq_subjects*')) || (request()->is('admin/invocepartsmasters*')) ||(request()->is('admin/invocemasters*')) ||(request()->is('admin/financialyears*')) ||(request()->is('admin/divisions*')) ||(request()->is('admin/dispatch_transport_details*')) || (request()->is('admin/locations/state*')) || (request()->is('admin/locations/city*')) ||(request()->is('admin/locations/pincode*'))||  (request()->is('admin/offers*'))  ||(request()->is('admin/products*')) ||(request()->is('admin/product/bulkupload*')) ||(request()->is('admin/product/bulk_products_update*'))|| (request()->is('admin/regions*')) || (request()->is('admin/gsts*')) || (request()->is('admin/categories*')) || (request()->is('admin/warehouses*')) || (request()->is('admin/racks*')) || (request()->is('admin/orderstatuses*')) || (request()->is('admin/colors*')) || (request()->is('admin/months*')) || (request()->is('admin/parts*')) || (request()->is('admin/hsns*')) || (request()->is('admin/pincodes*')) ? 'display:block' : '' }}">
                        
                        @can('view_divisions')
                            <li class="{{ (request()->is('admin/divisions*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/divisions')}}"><i class="fa fa fa-connectdevelop	
                                        "
                                                                              aria-hidden="true"></i> Division</a></li>
                        @endcan
                        @can('view_categories')
                            <li class="{{ (request()->is('admin/categories*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/categories')}}"><i class="fa fa-codiepie"
                                                                               aria-hidden="true"></i> Category</a></li>
                        @endcan
                        
                        @can('view_products')
                            <li class="{{ (request()->is('admin/products*')) ? 'active' : '' }}">
                                <a href="{{url('/admin/products')}}"><i class="fa fa-cube" aria-hidden="true"></i>
                                    Products</a>
                            </li>
                        @endcan
                        @can('view_parts')
                            <li class="{{ (request()->is('admin/parts*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/parts')}}"><i class="fa fa-cubes" aria-hidden="true"></i>
                                    Parts</a></li>
                        @endcan
                        @can('view_financialyears')
                            <li class="{{ (request()->is('admin/financialyears*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/financialyears')}}"><i class="fa fa-fax"
                                                                                   aria-hidden="true"></i>
                                    Financial Year</a></li>
                        @endcan
                        @can('view_invocemasters')
                            <li class="{{ (request()->is('admin/invocemasters*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/invocemasters')}}"><i class="fa fa-file-excel-o"
                                                                                  aria-hidden="true"></i>
                                    S-Form Invoice Series</a></li>
                        @endcan
                        @can('view_invocepartsmasters')
                            <li class="{{ (request()->is('admin/invocepartsmasters*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/invocepartsmasters')}}"><i class="fa fa-file-excel-o"
                                                                                       aria-hidden="true"></i>
                                    P-Form Invoice Series</a></li>
                        @endcan
                        <li class="{{ (request()->is('admin/dispatch_transport_details*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/dispatch_transport_details')}}"><i class="fa fa-truck"
                                                                                           aria-hidden="true"></i>
                                Dispatch Transport</a></li>
                        <li class="{{ (request()->is('admin/rq_subjects*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/rq_subjects')}}"><i class="fa fa-envelope-o"
                                                                            aria-hidden="true"></i> Replacement Query
                                Subject</a></li>
                        @can('view_gsts')
                            <li class="{{ (request()->is('admin/gsts*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/gsts')}}"><i class="fa fa-list-alt" aria-hidden="true"></i>
                                    GST Slab</a></li>
                        @endcan
                        @can('view_hsns')
                            <li class="{{ (request()->is('admin/hsns*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/hsns')}}"><i class="fa fa-list-alt" aria-hidden="true"></i>
                                    HSN Codes</a></li>
                        @endcan
                        @can('view_creditnoteremarks')
                            <li class="{{ (request()->is('admin/creditnoteremarks*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/creditnoteremarks')}}"><i class="fa fa-inr  "
                                                                                      aria-hidden="true"></i>
                                    CN / DN Remarks</a></li>
                        @endcan

                        
                        @can('view_colors')
                            <li class="{{ (request()->is('admin/colors*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/colors')}}"><i class="fa fa-list-alt"
                                                                           aria-hidden="true"></i> Color</a></li>
                        @endcan
                        @can('view_months')
                            <li class="{{ (request()->is('admin/months*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/months')}}"><i class="fa fa-list-alt"
                                                                           aria-hidden="true"></i> Month</a></li>
                        @endcan

                        @can('view_racks')
                            <li class="{{ (request()->is('admin/racks*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/racks')}}"><i class="fa fa-list-alt" aria-hidden="true"></i>
                                    Blocks</a>
                            </li>
                        @endcan


                        <li class="{{ (request()->is('admin/expensecategories*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/expensecategories')}}"><i class="fa fa-list-alt"
                                                                                  aria-hidden="true"> </i> Expense
                                Categories</a></li>

                        @can('view_orderstatuses')
                            <li class="{{ (request()->is('admin/orderstatuses*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/orderstatuses')}}"><i class="fa fa-list-alt"
                                                                                  aria-hidden="true"></i> Order
                                    Status</a></li>
                        @endcan



                        @can('view_offers')
                            <li class="{{ (request()->is('admin/offers*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/offers')}}"><i class="fa fa-list-alt"
                                                                           aria-hidden="true"></i> Offers</a></li>
                        @endcan

                        <li class="{{ (request()->is('admin/locations/state*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/locations/state')}}"><i class="fa fa-building-o"
                                                                               aria-hidden="true"></i> State</a></li>


                        <li class="{{ (request()->is('admin/locations/city*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/locations/city')}}"><i class="fa fa-building-o"
                                                                              aria-hidden="true"></i> City </a></li>


                        <li class="{{ (request()->is('admin/locations/pincode*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/locations/pincode')}}"><i class="fa fa-building-o"
                                                                                 aria-hidden="true"></i> Pincode </a>
                        </li>
                        @can('view_regions')
                            <li class="{{ (request()->is('admin/regions*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/regions')}}"><i class="fa fa-list-alt"
                                                                            aria-hidden="true"></i> Region</a></li>

                        @endcan
                        @can('view_distributor_bank_details')
                            <li class="{{ (request()->is('admin/distributor_bank_details*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/distributor_bank_details')}}"><i class="fa fa-inr"
                                                                                             aria-hidden="true"></i>
                                    Distributors Bank Details</a></li>
                        @endcan
                    </ul>
                </li>


            <!-- <li class="{{ (request()->is('admin/distributors*')) ? 'active' : '' }}" ><a  href="{{url('/admin/distributors')}}"><i class="fa fa-users"></i> Distributors</a></li> -->

            @endif
            @if(Auth::user()->roles->first()->name=='Admin' ||  Auth::user()->roles->first()->name=='Distributor Registration')

                <li class="{{ (request()->is('admin/distributors*')) ? 'active' : '' }}"><a
                            href="{{url('/admin/distributors')}}"><i class="fa fa-book" aria-hidden="true"></i>
                        Distributors List</a></li>

                {{-- <li class="treeview  {{ (request()->is('admin/distributors*')) ||(request()->is('admin/product/distributors')) ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-pagelines"></i>  Distributors <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu" style="{{ (request()->is('admin/distributors*')) ? 'display:block' : '' }} {{ (request()->is('admin/product/distributors')) ? 'display:block' : '' }}">
                        @can('view_distributors')
                            <li class="{{ (request()->is('admin/distributors*')) ? 'active' : '' }}"><a href="{{url('/admin/distributors')}}"><i class="fa fa-book" aria-hidden="true"></i> Distributors List</a></li>
                    @endcan
                    <!-- @can('view_products')
                        <li class="{{ (request()->is('admin/product/bulkupload*')) ? 'active' : '' }}"><a href="{{url('/admin/product/bulkupload')}}"><i class="fa fa-upload" aria-hidden="true"></i> Bulk Upload Products</a></li>
                    @endcan -->
                    </ul>
                </li> --}}
            @endif

            @can('view_users')
                <li class="{{ (request()->is('admin/users*')) ? 'active' : '' }}"><a href="{{url('/admin/users')}}"><i
                                class="fa fa-users"></i> Users</a></li>
            @endcan

            @can('view_roles')
                <li class="{{ (request()->is('admin/roles*')) ? 'active' : '' }}"><a href="{{url('/admin/roles')}}"><i
                                class="fa fa-book"></i> Role</a></li>
            @endcan

            {{--            @can('view_sales-person-distributor')--}}
            @if(Auth::user()->roles->first()->name=='Admin')
                <li class="treeview  {{ (request()->is('admin/sales-person-distributor*')) ||(request()->is('admin/assignmeeting*')) ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-gift"></i> BD / Sales <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/sales-person-distributor*')) ? 'display:block' : '' }}">
                        {{--                        @can('view_sales-person-distributor')--}}
                        <li class="{{ (request()->is('admin/sales-person-distributor*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/sales-person-distributor')}}"><i class="fa fa-gift"
                                                                                        aria-hidden="true"></i> Field
                                Executive List </a></li>
                        {{--                        @endcan--}}
                        {{--                        @can('view_assignmeeting')--}}
                        <li class="{{ (request()->is('admin/beats*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/beats')}}"><i class="fa fa-gift" aria-hidden="true"></i> Beats
                            </a></li>

                        <li class="{{ (request()->is('admin/assignmeeting*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/assignmeeting/index')}}"><i class="fa fa-gift"
                                                                                   aria-hidden="true"></i> Assign
                                Meetings </a></li>
                        {{--                        @endcan--}}

                    </ul>
                </li>
            @endif
            {{--            @endcan--}}


            @if(Auth::user()->categories_id != null )

                @can('view_qrcodes')
                    <li class="treeview  {{(request()->is('admin/reprint-qrcodes*')) || (request()->is('admin/reprintqr_from*')) || (request()->is('admin/print_all_selected_id*')) || (request()->is('admin/reprintqrcodelist*'))  || (request()->is('admin/reprint-qrcodes/create*')) || (request()->is('admin/qrcodes*')) ||(request()->is('admin/listingBarcodeInventory*')) ? 'menu-open' : '' }} ">
                        <a href="#"><i class="fa fa-qrcode"></i> QR & Barcode <i
                                    class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu"
                            style="{{(request()->is('admin/reprint-qrcodes*')) || (request()->is('admin/printqrCode*')) || (request()->is('admin/reprintqr_from*')) || (request()->is('admin/print_all_selected_id*')) || (request()->is('admin/reprintqrcodelist*'))  || (request()->is('admin/reprint-qrcodes/create*')) || (request()->is('admin/listingBarcodeInventory*')) ||(request()->is('admin/qrcodes*')) ? 'display:block' : '' }}">

                            @can('add_qrcodes')
                                <li class="{{ (request()->is('admin/qrcodes/create*')) ? 'active' : '' }}"><a
                                            href="{{url('/admin/qrcodes/create')}}"><i class="fa fa-qrcode"></i>
                                        Add QR Code</a></li>
                                <li class="{{ (request()->is('admin/reprint-qrcodes/create*')) ? 'active' : '' }}"><a
                                            href="{{url('/admin/reprint-qrcodes/create')}}"><i class="fa fa-qrcode"></i>
                                        Request Single QR</a></li>
                            @endcan
                            @can('view_qrcodes')
                                <li class="{{ (request()->is('admin/qrcodes*')) ? 'active' : '' }}"><a
                                            href="{{url('admin/qrcodes')}}"><i class="fa fa-gift"
                                                                               aria-hidden="true"></i>
                                        @if(Auth::user()->roles->first()->name=='Production')  Your QR Request  @else QR
                                        Request / Listing @endif </a></li>

                            @endcan
                            @can('print_qrcode')
                                <li class="{{ (request()->is('admin/reprint-qrcodes*')) ? 'active' : '' }}"><a
                                            href="{{url('admin/reprint-qrcodes')}}"><i class="fa fa-gift"
                                                                                       aria-hidden="true"></i> QR
                                        Reprint
                                        Single </a></li>
                                <li class="{{ (request()->is('admin/reprintqrcodelist*')) ? 'active' : '' }}"><a
                                            href="{{url('admin/reprintqrcodelist')}}"><i class="fa fa-gift"
                                                                                         aria-hidden="true"></i> QR
                                        Reprint
                                        List </a></li>
                            @endcan


                            {{--                        @can('view_listingBarcodeInventory')--}}
                            {{--                            <li class="{{ (request()->is('admin/listingBarcodeInventory*')) ? 'active' : '' }}"><a--}}
                            {{--                                        href="{{url('admin/listingBarcodeInventory')}}"><i class="fa fa-gift"--}}
                            {{--                                                                                           aria-hidden="true"></i>--}}
                            {{--                                    Upload Line Data </a></li>--}}
                            {{--                        @endcan--}}

                        </ul>
                    </li>

                    @can('view_product-qrcodes')
                        <li class="{{ request()->is('admin/product-qrcodes*') ? 'active' : '' }}">
                            <a
                                    href="{{url('admin/product-qrcodes')}}"><i class="fa fa-cube"
                                                                               aria-hidden="true"></i>
                                Product Qrcodes</a></li>
                        </li>
                    @endcan
                @endcan

            @endif

            @can('view_product-qrcodes--')
                <li class="treeview  {{ request()->is('admin/product-qrcodes*')  ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-qrcode"></i> Other Qrcodes <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ request()->is('admin/product-qrcodes*') ? 'display:block' : '' }}">
                        @can('view_products')
                            <li class="{{ request()->is('admin/product-qrcodes*') ? 'active' : '' }}">
                                <a
                                        href="{{url('admin/product-qrcodes')}}"><i class="fa fa-cube"
                                                                                   aria-hidden="true"></i>
                                    Product Qrcodes</a></li>
                            {{-- <li class="{{ request()->is('admin/product-qrcodes/product-code-qr-image-download') ? 'active' : '' }}">
                                    <a
                                    href="{{url('admin/product-qrcodes/product-code-qr-image-download')}}"><i class="fa fa-image " aria-hidden="true"></i>
                                     Qrcodes Image</a></li> --}}
                        @endcan

                    </ul>
                </li>
            @endcan


        <!-- @can('view_qrcodes')

            <li class="{{ (request()->is('admin/qrcodes*')) ? 'active' : '' }}"><a href="{{url('/admin/qrcodes')}}"><i class="fa fa-qrcode"></i>Production</a></li>
                @endcan
            @can('view_sform')
            <li class="{{ (request()->is('admin/sform*')) ? 'active' : '' }}"><a href="{{url('/admin/sform')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> S-Form Request</a></li>
                @endcan
            @can('view_pform')
            <li class="{{ (request()->is('admin/pform*')) ? 'active' : '' }}"><a href="{{url('/admin/pform')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> P-Form Request</a></li>
                @endcan -->


        <!-- @can('view_truck-freight')
            <li><a href="{{url('/admin/truck-freight')}}"><i class="fa fa-users"></i> Truck Freight Chart</a></li>
            @endcan -->

            @can('view_truck-freight')
                <li class="treeview  {{ (request()->is('admin/truck-freight*')) ||(request()->is('admin/truck-freights/bulkupload*')) ||(request()->is('admin/truck-routes*'))  ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-truck"></i> Truck Freight Chart <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/truck-freight*')) ? 'display:block' : '' }} {{ (request()->is('admin/product/truck-freight*')) ? 'display:block' : '' }} {{ (request()->is('admin/truck-routes*')) ? 'display:block' : '' }} ">
                        @can('view_products')
                            <li class="{{ (request()->is('admin/truck-freight/*')) || (request()->is('admin/truck-freight')) ? 'active' : '' }}">
                                <a href="{{url('/admin/truck-freight')}}"><i class="fa fa-truck" aria-hidden="true"></i>
                                    Truck Freight</a></li>
                        @endcan
                        @can('view_products')
                            <li class="{{ (request()->is('admin/truck-freights/bulkupload*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/truck-freights/bulkupload')}}"><i class="fa fa-upload"
                                                                                              aria-hidden="true"></i>
                                    Bulk Truck Freight </a></li>
                        @endcan
                        @can('view_products')
                            <li class="{{ (request()->is('admin/truck-routes*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/truck-routes')}}"><i class="fa fa-upload"
                                                                                 aria-hidden="true"></i>
                                    Truck Routes </a></li>
                        @endcan
                        @can('view_products')
                            <li class="{{ (request()->is('admin/truck-routes-bulkupload*')) ? 'active' : '' }}"><a
                                        href="{{url('/admin/truck-routes-bulkupload')}}"><i class="fa fa-upload"
                                                                                            aria-hidden="true"></i>
                                    Bulk Truck Routes </a></li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('view_coupons')
                @if(Auth::user()->roles->first()->name !='Finance')
                    <li class="treeview  {{(request()->is('admin/distributed-wise-coupons*')) ||(request()->is('admin/coupons_retailer')) || (request()->is('admin/coupons_retailer/*')) || (request()->is('admin/coupon_retailer-types*')) || (request()->is('admin/coupons*')) ||(request()->is('admin/coupon-types*')) ||(request()->is('admin/deleted-coupons*')) ? 'menu-open' : '' }} ">
                        <a href="#"><i class="fa fa-gift"></i> Discount <i
                                    class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu"
                            style="{{(request()->is('admin/coupons_retailer')) || (request()->is('admin/coupons_retailer/*')) || (request()->is('admin/coupon_retailer-types*')) || (request()->is('admin/distributed-wise-coupons*')) || (request()->is('admin/coupons*')) ||(request()->is('admin/coupon-types*')) ||(request()->is('admin/deleted-coupons*')) ? 'display:block' : '' }}">


                            <li class="treeview  {{(request()->is('admin/distributed-wise-coupons*')) || (request()->is('admin/coupons')) || (request()->is('admin/coupons/*')) ||(request()->is('admin/deleted-coupons*')) ? 'menu-open' : '' }} ">
                                <a href="#"><i class="fa fa-gift"></i> Orpat to Distributor wise <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu"
                                    style="{{(request()->is('admin/distributed-wise-coupons*')) || (request()->is('admin/coupons')) || (request()->is('admin/coupons/*')) || (request()->is('admin/coupon-types')) || (request()->is('admin/coupon-types/*'))  ||(request()->is('admin/deleted-coupons*')) ? 'display:block' : '' }}">
                                    @can('view_coupons')
                                        <li class="{{ (request()->is('admin/coupons')) || (request()->is('admin/coupons/*')) ? 'active' : '' }}">
                                            <a href="{{url('/admin/coupons')}}"><i class="fa fa-gift"
                                                                                   aria-hidden="true"></i> Discounts
                                            </a>
                                        </li>
                                    @endcan
                                    @can('view_coupon-types')
                                        <li class="{{ (request()->is('admin/coupon-types')) || (request()->is('admin/coupon-types/*'))  ? 'active' : '' }}">
                                            <a href="{{url('/admin/coupon-types')}}"><i class="fa fa-tag"
                                                                                        aria-hidden="true"></i> Discount
                                                Types </a></li>
                                    @endcan
                                    @can('view_coupons')
                                        <li class="{{ (request()->is('admin/Orpat to Retailer wis-wise-coupons*')) ? 'active' : '' }}">
                                            <a href="{{url('/admin/distributed-wise-coupons')}}"><i class="fa fa-users"
                                                                                                    aria-hidden="true"></i>
                                                Distributor Wise </a></li>
                                    @endcan
                                    @can('view_coupons')
                                        <li class="{{ (request()->is('admin/deleted-coupons*')) ? 'active' : '' }}"><a
                                                    href="{{url('/admin/deleted-coupons')}}"><i class="fa fa-trash"
                                                                                                aria-hidden="true"></i>
                                                Deleted Discount </a></li>
                                    @endcan

                                </ul>
                            </li>

                            <li class="treeview  {{  (request()->is('admin/coupons_retailer')) || (request()->is('admin/coupons_retailer/*')) || (request()->is('admin/coupon_retailer-types*'))? 'menu-open' : '' }} ">
                                <a href="#"><i class="fa fa-gift"></i> Orpat to Retailer wise <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu"
                                    style="{{(request()->is('admin/coupons_retailer')) || (request()->is('admin/coupons_retailer/*')) ||(request()->is('admin/coupon_retailer-types*')) ? 'display:block' : '' }}">
                                    @can('view_coupons')
                                        <li class="{{  (request()->is('admin/coupons_retailer')) || (request()->is('admin/coupons_retailer/*')) ? 'active' : '' }}">
                                            <a href="{{url('/admin/coupons_retailer')}}"><i class="fa fa-gift"
                                                                                            aria-hidden="true"></i>Orpat
                                                to
                                                Retailer wise</a></li>
                                    @endcan
                                    @can('view_coupon-types')
                                        <li class="{{ (request()->is('admin/coupon_retailer-types*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('/admin/coupon_retailer-types')}}"><i class="fa fa-tag"
                                                                                                      aria-hidden="true"></i>
                                                Discount Types </a></li>
                                    @endcan
                                    @can('view_coupons')
                                        <li class="{{ (request()->is('admin/distributed-wise-coupons*')) ? 'active' : '' }}">
                                            <a href="{{url('/admin/distributed-wise-coupons')}}"><i class="fa fa-users"
                                                                                                    aria-hidden="true"></i>
                                                Distributor Wise </a></li>
                                    @endcan
                                    @can('view_coupons')
                                        <li class="{{ (request()->is('admin/deleted-coupons*')) ? 'active' : '' }}"><a
                                                    href="{{url('/admin/deleted-coupons')}}"><i class="fa fa-trash"
                                                                                                aria-hidden="true"></i>
                                                Deleted Discount </a></li>
                                    @endcan

                                </ul>
                            </li>

                        </ul>
                    </li>
                @endif
            @endcan


            {{--            @can('view_coupons')--}}
            {{--                <li class="treeview  {{(request()->is('admin/schemediscounts*')) ? 'menu-open' : '' }} ">--}}
            {{--                    <a href="#"><i class="fa fa-gift"></i> Schemes <i--}}
            {{--                                class="fa fa-angle-left pull-right"></i></a>--}}
            {{--                    <ul class="treeview-menu"--}}
            {{--                        style="{{(request()->is('admin/schemediscounts*')) ? 'display:block' : '' }}">--}}
            {{--                        @can('view_coupons')--}}
            {{--                            <li class="{{ (request()->is('admin/schemediscounts*')) ? 'active' : '' }}"><a--}}
            {{--                                        href="{{url('/admin/schemediscounts')}}"><i class="fa fa-gift"--}}
            {{--                                                                                    aria-hidden="true"></i> Scheme--}}
            {{--                                    discounts </a></li>--}}
            {{--                        @endcan--}}

            {{--                    </ul>--}}
            {{--                </li>--}}
            {{--            @endcan--}}

            @can('view_schemes')
                <li class="treeview  {{ (request()->is('admin/schemes*')) ||(request()->is('admin/categoryschemes*')) ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-gift"></i> Schemes <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="treeview ">
                            <a href="#"><i class="fa fa-gift"></i> S-Form -Normal-Scheme <i
                                        class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                {{--                                <li class="{{ (request()->is('admin/schemes*')) ? 'active' : '' }}"><a--}}
                                {{--                                            href="{{url('admin/schemes')}}"><i class="fa fa-users"></i> Model Wise</a>--}}
                                {{--                                </li>--}}

                                <li class="{{ (request()->is('admin/categoryschemes*')) ? 'active' : '' }}"><a
                                            href="{{url('admin/categoryschemes')}}"><i
                                                class="fa fa-shopping-cart"></i> Category Wise</a></li>

                                <li class="{{ (request()->is('admin/categoryschemes_expired*')) ? 'active' : '' }}"><a
                                            href="{{url('admin/categoryschemes_expired')}}"><i
                                                class="fa fa-shopping-cart"></i> Category Wise Expired</a></li>

                            </ul>
                        </li>
                        <li class="treeview ">
                            <a href="#"><i class="fa fa-gift"></i> Invoice-wise-Scheme <i
                                        class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class=""><a
                                            href="{{url('/admin/invoice-wise-scheme')}}"><i class="fa fa-users"></i>
                                        Invoice-wise-Scheme</a>
                                </li>

                                <li class=""><a
                                            href="{{url('/admin/invoice-wise-scheme-expired')}}"><i
                                                class="fa fa-users"></i> Invoice-wise-Scheme expired</a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview ">
                            <a href="#"><i class="fa fa-gift"></i> S-Form -Monthly-Scheme <i
                                        class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class=""><a
                                            href="{{url('/admin/monthly-wise-scheme')}}"><i class="fa fa-users"></i>
                                        Monthly Scheme</a>
                                </li>
                            </ul>
                        </li>

                        <li class="treeview ">
                            <a href="#"><i class="fa fa-gift"></i> S-Form -Annual-Scheme <i
                                        class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class=""><a
                                            href="{{url('/admin/annual-wise-scheme')}}"><i class="fa fa-users"></i>
                                        Turnover Wise</a>
                                </li>
                                <li class=""><a
                                            href="{{url('/admin/foreign-wise-scheme')}}"><i class="fa fa-users"></i>
                                        Foreign Trip Wise</a>
                                </li>
                            </ul>
                        </li>
                        <li class="treeview ">
                            <a href="#"><i class="fa fa-gift"></i> P-Form Scheme <i
                                        class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                {{--                                <li class=""><a--}}
                                {{--                                            href="{{url('admin/p-form-scheme')}}"><i class="fa fa-users"></i> Model Wise</a>--}}
                                {{--                                </li>--}}

                                <li class=""><a
                                            href="{{url('/admin/p-form-scheme-category-wise')}}"><i
                                                class="fa fa-shopping-cart"></i> Category Wise</a></li>

                                <li class=""><a
                                            href="{{url('/admin/pform-expirescheme-catwise')}}"><i
                                                class="fa fa-shopping-cart"></i> Category Wise Expired</a></li>

                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="treeview  {{ (request()->is('admin/retailers-categoryscheme*')) ||(request()->is('admin/retailers-p-form-scheme-category-wise*')) ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-gift"></i> Distributor-to-Retailer-Schemes <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="treeview ">
                            <a href="#"><i class="fa fa-gift"></i> S-Form -Normal-Scheme <i
                                        class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class="{{ (request()->is('admin/retailers-categoryscheme*')) ? 'active' : '' }}"><a
                                            href="{{url('admin/retailers-categoryscheme')}}"><i
                                                class="fa fa-shopping-cart"></i> Category Wise</a></li>

                                <li class="{{ (request()->is('admin/retailers-cat-expired*')) ? 'active' : '' }}"><a
                                            href="{{url('admin/retailers-cat-expired')}}"><i
                                                class="fa fa-shopping-cart"></i> Category Wise Expired</a>
                                </li>

                            </ul>
                        </li>

                        <li class="treeview ">
                            <a href="#"><i class="fa fa-gift"></i> P-Form Scheme <i
                                        class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class=""><a
                                            href="{{url('/admin/retailers-p-form-scheme')}}"><i
                                                class="fa fa-shopping-cart"></i> Category Wise</a></li>

                                <li class=""><a
                                            href="{{url('/admin/retailers-exp-part-scheme')}}"><i
                                                class="fa fa-shopping-cart"></i> Category Wise Expired</a></li>

                            </ul>
                        </li>
                    </ul>
                </li>
            @endcan
            @can('view_deletedbarcodes')
                <li class="{{ (request()->is('admin/deletedbarcodes*')) ? 'active' : '' }}"><a
                            href="{{url('admin/deletedbarcodes')}}"><i class="fa fa-trash" aria-hidden="true"></i>
                        Delete Single Barcode</a></li>
            @endcan

            @can('view_inward')

                <li class="{{ (request()->is('admin/verify-inwards*')) ? 'active' : '' }}"><a
                            href="{{url('admin/verify-inwards')}}"><i class="fa fa-upload" aria-hidden="true"></i>
                        Upload Inwards</a></li>
                <li class="{{ (request()->is('admin/aprroved-uploade-inwards*')) ? 'active' : '' }}"><a
                            href="{{url('admin/aprroved-uploade-inwards')}}"><i class="fa fa-check"
                                                                                aria-hidden="true"></i> Approved
                        Uploaded Inwards</a></li>
                <li class="{{ (request()->is('admin/process-for-invoice-save*')) ||(request()->is('admin/start-preparing-for-invoice*')) ? 'active' : '' }}"><a
                            href="{{url('admin/start-preparing-for-invoice')}}"><i class="fa fa-files-o"
                                                                                   aria-hidden="true"></i> Process for
                        Invoice</a></li>

                <li class="treeview  {{ (request()->is('admin/generated_distributor_awb*'))  || (request()->is('admin/saved-invoices-final*'))  || (request()->is('admin/generated-invoice-final*'))? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-shopping-cart"></i> S-Form Final Invoice<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/generated_distributor_awb*'))  ||(request()->is('admin/saved-invoices-final*')) ? 'display:block' : '' }}">

                        <li class="{{ (request()->is('admin/saved-invoices-final*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/saved-invoices-final')}}"><i
                                        class="fa fa-save"></i> Saved Invoice</a></li>

                        <li class="{{ (request()->is('admin/generated-invoice-final')) ? 'active' : '' }}">
                            <a href="{{url('admin/generated-invoice-final')}}"><span class="glyphicon glyphicon-check"></span> Generated Invoice</a></li>

                        <li class="{{ (request()->is('admin/generated_distributor_awb')) ? 'active' : '' }}">
                            <a href="{{url('admin/generated_distributor_awb')}}"><i
                                        class="fa fa-ticket"></i>Generated E-Way bill</a></li>


                    </ul>
                </li>


                <li class="treeview  {{ (request()->is('admin/pending-pform-order-invoice*'))  || (request()->is('admin/saved-pform-order-invoice*')) || (request()->is('admin/generated-pform-order-invoice*'))? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa fa fa-paypal"></i> P Form Order<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{(request()->is('admin/saved-pform-order-invoice*')) || (request()->is('admin/generated-pform-order-invoice*')) || (request()->is('admin/pending-pform-order-invoice*'))   ? 'display:block' : '' }}">

                        <li class="{{ (request()->is('admin/pending-pform-order-invoice*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/pending-pform-order-invoice')}}"><i
                                        class="fa fa-clock-o"></i>Pending P-Form Order</a></li>

                        <li class="{{ (request()->is('admin/saved-pform-order-invoice')) ? 'active' : '' }}">
                            <a href="{{url('admin/saved-pform-order-invoice')}}"><i
                                        class="fa fa-save"></i>Saved P-Form Invoice</a></li>

                        <li class="{{ (request()->is('admin/generated-pform-order-invoice')) ? 'active' : '' }}">
                            <a href="{{url('admin/generated-pform-order-invoice')}}"><span class="glyphicon glyphicon-check"></span>Generated P-Form Invoice</a></li>


                    </ul>
                </li>

               <!--   START EXPORT AND NEPAL ORDER DISPATCH !-->
                <li class="treeview  {{ (request()->is('admin/export*'))? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-shopping-cart"></i> Manage Export Orders<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/saved-invoices-final*')) ? 'display:block' : '' }}">

                        <li class="{{ (request()->is('admin/export-pending-orders-dispatch*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/export-pending-orders-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Export-Pending-Orders</a>
                        </li>

                        <li class="{{ (request()->is('admin/export-upload-inwards-orders-dispatch*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/export-upload-inwards-orders-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Upload Inwards</a>
                        </li>
                        <li class="{{ (request()->is('admin/export-approved-uploaded-inwards-dispatch*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/export-approved-uploaded-inwards-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Approved Uploaded Inwards</a>
                        </li>
                        <li class="{{ (request()->is('admin/export-process-for-Invoice-dispatch*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/export-process-for-Invoice-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Process for Invoice</a>
                        </li>


                        <li class="{{ (request()->is('admin/export-orders-saved-details-dispatch')) ? 'active' : '' }}">
                            <a href="{{url('admin/export-orders-saved-details-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Export Order Details</a>
                        </li>


                    </ul>
                </li>


                <li class="treeview  {{ (request()->is('admin/nepal*'))? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-shopping-cart"></i> Manage Nepal Orders<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/saved-invoices-final*')) ? 'display:block' : '' }}">

                        <li class="{{ (request()->is('admin/nepal-pending-orders-dispatch*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/nepal-pending-orders-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Export-Pending-Orders</a>
                        </li>

                        <li class="{{ (request()->is('admin/nepal-upload-inwards-orders-dispatch*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/nepal-upload-inwards-orders-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Upload Inwards</a>
                        </li>
                        <li class="{{ (request()->is('admin/nepal-approved-uploaded-inwards-dispatch*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/nepal-approved-uploaded-inwards-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Approved Uploaded Inwards</a>
                        </li>
                        <li class="{{ (request()->is('admin/nepal-process-for-Invoice-dispatch*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/nepal-process-for-Invoice-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Process for Invoice</a>
                        </li>


                        <li class="{{ (request()->is('admin/nepal-orders-saved-details-dispatch')) ? 'active' : '' }}">
                            <a href="{{url('admin/nepal-orders-saved-details-dispatch')}}"><i
                                        class="fa fa-shopping-cart"></i> Nepal Order Details</a>
                        </li>


                    </ul>
                </li>

                <!--   END EXPORT AND NEPAL ORDER DISPATCH !-->

{{--                <li class="treeview  {{ (request()->is('admin/export-upload-inwards-orders*'))  || (request()->is('admin/view-exports-inwards-details*')) || (request()->is('admin/export-orders-saved-details*'))? 'menu-open' : '' }} ">--}}
{{--                    <a href="#"><i class="fa fa-shopping-cart"></i> Manage Export Orders<i--}}
{{--                                class="fa fa-angle-left pull-right"></i></a>--}}
{{--                    <ul class="treeview-menu"--}}
{{--                        style="{{ (request()->is('admin/export-upload-inwards-orders*'))  || (request()->is('admin/view-exports-inwards-details*')) || (request()->is('admin/export-orders-saved-details*')) ? 'display:block' : '' }}">--}}

{{--                        <li class="{{ (request()->is('admin/export-upload-inwards-orders*')) ? 'active' : '' }}"><a--}}
{{--                                    href="{{url('admin/export-upload-inwards-orders')}}"><i--}}
{{--                                        class="fa fa-shopping-cart"></i> Upload Inwards</a>--}}
{{--                        </li>--}}

{{--                        <li class="{{ (request()->is('admin/view-exports-inwards-details')) ? 'active' : '' }}">--}}
{{--                            <a href="{{url('admin/view-exports-inwards-details')}}"><i--}}
{{--                                        class="fa fa-shopping-cart"></i> View Inwards Details</a>--}}
{{--                        </li>--}}

{{--                        <li class="{{ (request()->is('admin/export-orders-saved-details')) ? 'active' : '' }}">--}}
{{--                            <a href="{{url('admin/export-orders-saved-details')}}"><i--}}
{{--                                        class="fa fa-shopping-cart"></i> Export Order Details</a>--}}
{{--                        </li>--}}


{{--                    </ul>--}}
{{--                </li>--}}


{{--                <li class="treeview  {{ (request()->is('admin/nepal-upload-inwards-orders*'))  || (request()->is('admin/view-nepal-inwards-details*')) || (request()->is('admin/nepal-orders-saved-details*'))? 'menu-open' : '' }} ">--}}
{{--                    <a href="#"><i class="fa fa-shopping-cart"></i> Manage Nepal Orders<i--}}
{{--                                class="fa fa-angle-left pull-right"></i></a>--}}
{{--                    <ul class="treeview-menu"--}}
{{--                        style="{{ (request()->is('admin/nepal-upload-inwards-orders*'))  || (request()->is('admin/view-nepal-inwards-details*')) || (request()->is('admin/nepal-orders-saved-details*')) ? 'display:block' : '' }}">--}}

{{--                        <li class="{{ (request()->is('admin/nepal-upload-inwards-orders*')) ? 'active' : '' }}"><a--}}
{{--                                    href="{{url('admin/nepal-upload-inwards-orders')}}"><i--}}
{{--                                        class="fa fa-shopping-cart"></i> Upload Inwards</a>--}}
{{--                        </li>--}}

{{--                        <li class="{{ (request()->is('admin/view-nepal-inwards-details')) ? 'active' : '' }}">--}}
{{--                            <a href="{{url('admin/view-nepal-inwards-details')}}"><i--}}
{{--                                        class="fa fa-shopping-cart"></i> View Inwards Details</a>--}}
{{--                        </li>--}}

{{--                        <li class="{{ (request()->is('admin/nepal-orders-saved-details')) ? 'active' : '' }}">--}}
{{--                            <a href="{{url('admin/nepal-orders-saved-details')}}"><i--}}
{{--                                        class="fa fa-shopping-cart"></i> Nepal Order Details</a>--}}
{{--                        </li>--}}

{{--                    </ul>--}}
{{--                </li>--}}

            @endcan


            @if(Auth::user()->roles->first()->name=='Admin' ||Auth::user()->roles->first()->name=='Service' ||    Auth::user()->roles->first()->name=='Account Manager' || Auth::user()->roles->first()->name=='Senior Account Manager' )

                @can('view_sform')

                    <li class="treeview  {{request()->is('admin/distributor-replacement-forms*') || (request()->is('admin/approved-orders*')) || (request()->is('admin/pending-orders*')) || (request()->is('admin/dispatched-orders*')) ? 'menu-open' : '' }} ">
                        <a href="#"><i class="fa fa-shopping-cart"></i>All Orders <i
                                    class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu"
                            style="{{request()->is('admin/distributor-replacement-forms*') ||  (request()->is('admin/approved-orders*')) || (request()->is('admin/pending-orders*')) || (request()->is('admin/dispatched-orders*')) ? 'display:block' : '' }}">

                            <li class="{{ (request()->is('admin/pending-orders*')) ? 'active' : '' }}">
                                <a href="{{url('/admin/pending-orders')}}"><i class="fa fa-clock-o"
                                                                              aria-hidden="true"></i> <span>Pending Orders </span></a>
                            </li>


                            <li class="{{ (request()->is('admin/approved-orders*')) ? 'active' : '' }}">
                                <a href="{{url('/admin/approved-orders')}}"><i class="fa fa-check"
                                                                               aria-hidden="true"></i> <span>Approved Orders</span></a>
                            </li>


                            <li class="{{ (request()->is('admin/dispatched-orders*')) ? 'active' : '' }}">
                                <a href="{{url('/admin/dispatched-orders')}}"><i class="fa fa-truck"
                                                                                 aria-hidden="true"></i> <span>Dispatched Orders</span></a>
                            </li>

                            @if (Auth::guard('web')->user()->roles->implode('name', ', ') == 'Account Manager, Junior Account Management' || Auth::guard('web')->user()->roles->implode('name', ', ') =='Admin')
                                <li class="{{ (request()->is('admin/create-sform-complete-order*')) ? 'active' : '' }}">
                                    <a href="{{url('/admin/create-sform-complete-order')}}"><i class="fa fa-pencil"
                                                                                               aria-hidden="true"></i>
                                        <span>Create All Sform Order</span></a>
                                </li>
                            @endif



                            @can('view_distributor-replacement-form')



                                <li class="treeview  {{ (request()->is('admin/admin/distributor-replacement*'))  ? 'menu-open' : '' }} ">

                                    <a href="#"><i class="fa fa-tripadvisor"></i> Query Form <i
                                                class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu"

                                        style="{{ (request()->is('admin/distributor-replacement-forms*'))  ? 'display:block' : '' }}"

                                    >
                                        <li class="{{ (request()->is('admin/distributor-replacement-forms/pending-orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('/admin/distributor-replacement-forms/pending-orders')}}"><i
                                                        class="fa fa-clock-o"></i> Pending Orders</a>
                                        </li>
                                        <li class="{{ (request()->is('admin/distributor-replacement-forms/approved-orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('/admin/distributor-replacement-forms/approved-orders')}}"><i
                                                        class="fa fa-check"></i> Approved Orders</a>
                                        </li>
                                    </ul>
                                </li>
                            @endcan
                        </ul>
                    </li>
                    <li>
                        <a href="{{url('/admin/final-s-form-Scheme')}}"><i class="fa fa-tasks" aria-hidden="true"></i>
                            <span class="blinking">Scheme List </span></a>
                    </li>
                @endcan
                @if(Auth::user()->roles->first()->name !='Account Manager' && Auth::user()->roles->first()->name!='Senior Account Manager')
                    {{-- <li class="treeview  {{(request()->is('admin/rcform_pending_orders*')) ||(request()->is('admin/rcform_complete_orders*')) || (request()->is('admin/rform*')) || (request()->is('admin/sform*')) ||(request()->is('admin/pform*')) ||(request()->is('admin/p-form*')) ||  (request()->is('admin/s-form*'))|| (request()->is('admin/distributor-replacement-form*')) ? 'menu-open' : '' }} ">
                        <a href="#"><i class="fa fa-shopping-cart"></i> Orders <i
                                    class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu"
                            style="{{ (request()->is('admin/rcform_pending_orders*')) ||(request()->is('admin/rcform_complete_orders*')) ||(request()->is('admin/rform*')) || (request()->is('admin/sform*')) ||(request()->is('admin/pform*')) ||(request()->is('admin/p-form*')) || (request()->is('admin/s-form*'))|| (request()->is('admin/distributor-replacement-form*')) ? 'display:block' : '' }}">
                             --}}
                            
                            @can('view_sform')

                                <li class="treeview  {{ (request()->is('admin/sform*')) || (request()->is('admin/s-form*')) ? 'menu-open' : '' }} ">
                                    <a href="#"><i class="fa fa-shopping-cart"></i> S Form Orders <i
                                                class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu"
                                        style="{{ (request()->is('admin/sform*')) || (request()->is('admin/s-form*')) ? 'display:block' : '' }}">

                                        <li class="{{ (request()->is('admin/sform_pending_orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('admin/sform_pending_orders')}}"><i
                                                        class="fa fa-shopping-cart"></i>Pending Orders </a></li>

                                        <li class="{{ (request()->is('admin/sform_completed_orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('admin/sform_completed_orders')}}"><i
                                                        class="fa fa-shopping-cart"></i>Approved Orders </a></li>
                                        <li class="{{ (request()->is('admin/s-form/dispatched*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('admin/s-form/dispatched-orders')}}"><i
                                                        class="fa fa-check"></i> Dispatched Orders </a></li>
                                        <li class="{{ (request()->is('admin/sform/create*')) ? 'active' : '' }}">
                                            <a href="{{url('admin/sform/create')}}"><i class="fa fa-user-plus"
                                                                                       aria-hidden="true"></i> Create
                                                New
                                                Order </a>
                                        </li>


                                    </ul>
                                </li>
                                @if(Auth::user()->roles->first()->name !='Admin')
                                <li>
                                    <a href="{{url('/admin/final-s-form-Scheme')}}"><i class="fa fa-tasks"
                                                                                       aria-hidden="true"></i> <span
                                                class="blinking">Scheme List </span></a>
                                </li>
                                @endif
                            @endcan
                            @can('view_pform')
                                <li class="treeview  {{(request()->is('admin/p-form/cancelled*')) || (request()->is('admin/pform*')) ||(request()->is('admin/p-form*'))  ? 'menu-open' : '' }} ">
                                    <a href="#"><i class="fa fa fa-paypal	
                                        "></i> P Form Orders <i
                                                class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu"
                                        style="{{ (request()->is('admin/p-form/cancelled*')) ||(request()->is('admin/pform*')) ||(request()->is('admin/p-form*'))  ? 'display:block' : '' }}">

                                        <li class="{{ (request()->is('admin/pform_pending_orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('admin/pform_pending_orders')}}"><i
                                                        class="fa fa-clock-o"></i>Pending Orders</a></li>


                                        <li class="{{ (request()->is('admin/pform_completed_orders')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('admin/pform_completed_orders')}}"><i
                                                        class="fa fa-check"></i>Approved Orders</a></li>


                                        <li class="{{ (request()->is('admin/p-form/dispatched*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('admin/p-form/dispatched-orders')}}"><i
                                                        class="fa fa-truck"></i> Dispatched Orders </a></li>


                                        <li class="{{ (request()->is('admin/p-form/cancelled*')) ? 'active' : '' }}">
                                                            <a
                                                                    href="{{url('admin/p-form/cancelled-orders')}}"><i
                                                                        class="fa fa-window-close"></i> Cancelled Orders </a></li>

                                    </ul>
                                </li>
                            @endcan

                            @if(Auth::user()->roles->first()->name=='Service'  || Auth::user()->roles->first()->name=='Service Account Manager')

                                <li class="treeview  {{ (request()->is('admin/rform*'))  ? 'menu-open' : '' }} ">
                                    <a href="#"><i class="fa fa-bookmark"></i> R Form <i
                                                class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu"
                                        style="{{ (request()->is('admin/rform*'))  || (request()->is('admin/rform_pending_orders*')) ? 'display:block' : '' }}">
                                        <li class="{{ (request()->is('admin/rform_pending_orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('admin/rform_pending_orders')}}"><i
                                                        class="fa fa-clock-o"></i>Pending Orders</a></li>

                                        <li class="{{ (request()->is('admin/rform_complete_orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('admin/rform_complete_orders')}}"><i
                                                        class="fa fa-check"></i>Approved Orders</a></li>

                                    </ul>
                                </li>

                                <li class="treeview  {{ (request()->is('admin/rcform_complete_orders*')) || (request()->is('admin/rcform*'))  ? 'menu-open' : '' }} ">
                                    <a href="#"><i class="fa fa-bookmark-o"></i> RC Form <i
                                                class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu"
                                        style="{{ (request()->is('admin/rcform_complete_orders*')) ||(request()->is('admin/rcform*'))   ? 'display:block' : '' }}">
                                        <li class="{{ (request()->is('admin/rcform_pending_orders*')) ? 'active' : '' }}">
                                            <a 
                                                    href="{{url('admin/rcform_pending_orders')}}"><i
                                                        class="fa fa-clock-o"></i>RC Pending Orders</a></li>

                                        <li class="{{ (request()->is('admin/rcform_complete_orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('admin/rcform_complete_orders')}}"><i
                                                        class="fa fa-check"></i>Approved RC Orders</a></li>

                                    </ul>
                                </li>
                            @endif
                            @can('view_distributor-replacement-form')



                                <li class="treeview  {{ (request()->is('admin/admin/distributor-replacement*'))  ? 'menu-open' : '' }} ">

                                    <a href="#"><i class="fa fa fa-tripadvisor"></i> Query Form <i
                                                class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu"

                                        style="{{ (request()->is('admin/distributor-replacement-forms*'))  ? 'display:block' : '' }}"

                                    >
                                        <li class="{{ (request()->is('admin/distributor-replacement-forms/pending-orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('/admin/distributor-replacement-forms/pending-orders')}}"><i
                                                        class="fa fa fa-clock-o"></i> Pending Orders</a>
                                        </li>
                                        <li class="{{ (request()->is('admin/distributor-replacement-forms/approved-orders*')) ? 'active' : '' }}">
                                            <a
                                                    href="{{url('/admin/distributor-replacement-forms/approved-orders')}}"><i
                                                        class="fa fa-check"></i> Approved Orders</a>
                                        </li>
                                    </ul>
                                </li>
                            @endcan

                        {{-- </ul> --}}
                    {{-- </li> --}}
                @endif
            @endif


            @if(Auth::user()->categories_id != null)

                @if(Auth::user()->roles->first()->name=='Admin' || Auth::user()->roles->first()->name=='Store Keeper' || Auth::user()->roles->first()->name=='Production')

                    <li class="treeview  {{  (request()->is('admin/listingBarcodeInventory*')) || (request()->is('admin/upload_qrcode_nventory*')  || (request()->is('admin/balanced_goods_listing*'))) ? 'menu-open' : '' }} ">
                        <a href="#"><i class="fa fa-gift"></i>Product Scanned Goods <i
                                    class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu"
                            style="{{ ( request()->is('admin/upload_qrcode_nventory*') || request()->is('admin/listingBarcodeInventory*')  || request()->is('admin/balanced_goods_listing*')) ? 'display:block' : '' }}">
                            @can('scanned_goods')
                                <li class="{{ (request()->is('admin/upload_qrcode_nventory*')) ? 'active' : '' }}"><a
                                            href="{{url('/admin/upload_qrcode_nventory')}}"><i class="fa fa-qrcode"></i>
                                        Scanned Goods</a></li>
                            @endcan

                            @can('scanned_goods_listing')
                                <li class="{{ (request()->is('admin/listingBarcodeInventory*')) ? 'active' : '' }}"><a
                                            href="{{url('/admin/listingBarcodeInventory')}}"><i
                                                class="fa fa-qrcode"></i>
                                        Scanned Goods List</a></li>

                            @endcan

                            @can('balanced_goods_listing')
                                <li class="{{ (request()->is('admin/balanced_goods_listing*')) ? 'active' : '' }}"><a
                                            href="{{url('/admin/balanced_goods_listing')}}"><i class="fa fa-qrcode"></i>
                                        Balanced Goods List</a></li>
                            @endcan
                        </ul>
                    </li>
                @endif
            @endif
            @if(Auth::user()->roles->first()->name=='Admin' || Auth::user()->roles->first()->name=='Store Keeper')
                <li class="treeview  {{  (request()->is('admin/blockwiseinventory*')) || (request()->is('admin/current-stock-inventory/current*')) || (request()->is('admin/current-stock-inventory/old*')) ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Inventory Stock Details <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{  (request()->is('admin/blockwiseinventory*')) || ( request()->is('admin/current-stock-inventory/current*') || request()->is('admin/current-stock-inventory/old*')) ? 'display:block' : '' }}">

                        <li class="{{ (request()->is('admin/current-stock-inventory/current*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/current-stock-inventory/current')}}"><i class="fa fa-book"
                                                                                                aria-hidden="true"></i>
                                Current Inventory</a></li>

                        <li class="{{ (request()->is('admin/current-stock-inventory/old*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/current-stock-inventory/old')}}"><i class="fa fa-book"
                                                                                            aria-hidden="true"></i>
                                Sold Inventory</a></li>

                        <li class="{{ (request()->is('admin/blockwiseinventory*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/blockwiseinventory')}}"><i class="fa fa-book"
                                                                                   aria-hidden="true"></i>
                                Blockwise Inventory</a></li>


                    </ul>
                </li>
            @endif

            @if (Auth::user()->roles->first()->name == 'Production')


                <li class="treeview  {{ (request()->is('admin/pform*'))  ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-paypal"></i> P Form Order <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/pform*')) ? 'display:block' : '' }}">

                        <li class="{{ (request()->is('admin/pform_pending_orders*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/pform_pending_orders')}}"><i
                                        class="fa fa-clock-o"></i>Pending Orders</a></li>

                        <li class="{{ (request()->is('admin/pform_completed_orders')) ? 'active' : '' }}">
                            <a
                                    href="{{url('admin/pform_completed_orders')}}"><i
                                        class="fa fa-check"></i>Approved Orders</a></li>


                    </ul>
                </li>

                <li class="treeview  {{ (request()->is('admin/rform*'))  ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-bookmark"></i> R Form Order<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/rform*'))  || (request()->is('admin/rform_pending_orders*')) ? 'display:block' : '' }}">
                        <li class="{{ (request()->is('admin/rform_pending_orders*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/rform_pending_orders')}}"><i
                                        class="fa fa-clock-o"></i>Pending Orders</a></li>

                        <li class="{{ (request()->is('admin/rform_complete_orders*')) ? 'active' : '' }}">
                            <a
                                    href="{{url('admin/rform_complete_orders')}}"><i
                                        class="fa fa-check"></i>Approved Orders</a></li>

                    </ul>
                </li>


                <li class="treeview  {{ (request()->is('admin/rcform*'))  ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-bookmark-o"></i> RC Form Order<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/rcform*'))  ? 'display:block' : '' }}">
                        <li class="{{ (request()->is('admin/rcform_pending_orders*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/rcform_pending_orders')}}"><i
                                        class="fa fa-clock-o"></i>RC Pending Orders</a></li>

                        <li class="{{ (request()->is('admin/rcform_complete_orders*')) ? 'active' : '' }}">
                            <a
                                    href="{{url('admin/rcform_complete_orders')}}"><i
                                        class="fa fa-check"></i>Approved RC Orders</a></li>

                    </ul>
                </li>
                <li class="treeview  {{ (request()->is('admin/listingProductionReport*'))  ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-area-chart"></i> Reports<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/listingProductionReport*'))? 'display:block' : '' }}">
                        <li class="{{ (request()->is('admin/listingProductionReport*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/listingProductionReport')}}"><i
                                        class="fa fa-line-chart"></i>Production Report</a></li>
                    </ul>
                </li>
            @endif

        <!-- @can('view_sales-person-distributor')
            <li class="{{ (request()->is('admin/sales-person-distributor*')) ? 'active' : '' }}"><a href="{{url('/admin/sales-person-distributor')}}"><i class="fa fa-users"></i> Sales Person</a></li>
@endcan -->

            @can('view_retailers')
                <li class="{{ (request()->is('admin/retailers*')) ? 'active' : '' }}"><a
                            href="{{url('/admin/retailers')}}"><i class="fa fa-user"></i> Retailers</a></li>
            @endcan

            {{-- @can('view_retailersequences')
            <li class="{{ (request()->is('admin/retailer-sequences*')) ? 'active' : '' }}"><a href="{{url('/admin/retailer-sequences')}}"><i class="fa fa-user"></i> Retailers Sequence</a></li>
            @endcan --}}
            @if(Auth::user()->roles->first()->name=='Admin')
                <li class="{{ (request()->is('admin/reimbursements*')) ? 'active' : '' }}"><a
                            href="{{url('/admin/reimbursements')}}"><i class="fa fa-users"></i> Reimbursements</a></li>
                <li class=""><a href="{{url('admin/upload-master-inventory')}}"><i class="fa fa-book"></i>
                        Upload Inventory</a></li>
                <li class=""><a href="{{url('admin/upload-returned-inventory')}}"><i class="fa fa-book"></i>
                        Upload Returned Inventory</a></li>
                <li class=""><a href="{{url('admin/view-returned-inventory')}}"><i class="fa fa-book"></i>
                        View Returned Inventory</a></li>

                <li class=""><a target="_blank" href="http://rewards.orpatgroup.com/admin"><i class="fa fa-trophy"></i>
                        Rewards</a></li>
            @endif

            @can('view_bankuploads')
                <li class="{{ (request()->is('admin/bank-upload*')) ? 'active' : '' }}"><a
                            href="{{url('/admin/bank-upload')}}"><i class="fa fa-user"></i> Bank Uploads</a></li>
            @endcan

            @if(Auth::user()->roles->first()->name=='Pre Audit'  || Auth::user()->roles->first()->name=='Service Account Manager')

                <li class="treeview  {{ (request()->is('admin/rform*')) ||(request()->is('admin/r-form*'))   ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-bookmark"></i> R Form <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/r-form*'))   ||  (request()->is('admin/rform*')) ||(request()->is('admin/rform_pending_orders*')) ? 'display:block' : '' }}">
                        <li class="{{  (request()->is('admin/r-form*'))   ||  (request()->is('admin/rform_pending_orders*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/rform_pending_orders')}}"><i
                                        class="fa fa-clock-o"></i>Pending Orders</a></li>

                        <li class="{{ (request()->is('admin/rform_complete_orders*')) ? 'active' : '' }}">
                            <a
                                    href="{{url('admin/rform_complete_orders')}}"><i
                                        class="fa fa-check"></i>Approved Orders</a></li>

                    </ul>
                </li>

                <li class="treeview  {{ (request()->is('admin/rc-form*'))   ||  (request()->is('admin/rcform*'))  ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-bookmark-o"></i> RC Form <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/rc-form*'))   || (request()->is('admin/rcform*'))  ? 'display:block' : '' }}">
                        <li class="{{  (request()->is('admin/rc-form*'))   || (request()->is('admin/rcform_pending_orders*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/rcform_pending_orders')}}"><i
                                        class="fa fa-clock-o"></i>RC Pending Orders</a></li>

                        <li class="{{ (request()->is('admin/rcform_complete_orders*')) ? 'active' : '' }}">
                            <a
                                    href="{{url('admin/rcform_complete_orders')}}"><i
                                        class="fa fa-check"></i>Approved RC Orders</a></li>

                    </ul>
                </li>

                @if(Auth::user()->roles->first()->name=='Service Account Manager')

                    <li class="treeview  {{ (request()->is('admin/distributor-replacement-forms*'))   ? 'menu-open' : '' }} ">

                        <a href="#"><i class="fa fa fa-tripadvisor"></i> Query Form <i
                                    class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu"

                            style="{{ (request()->is('admin/distributor-replacement-forms*'))  ? 'display:block' : '' }}"

                        >
                            <li class="{{ (request()->is('admin/distributor-replacement-forms/pending-orders*')) ? 'active' : '' }}">
                                <a
                                        href="{{url('/admin/distributor-replacement-forms/pending-orders')}}"><i
                                            class="fa fa fa-clock-o"></i> Pending Orders</a>
                            </li>
                            <li class="{{ (request()->is('admin/distributor-replacement-forms/approved-orders*')) ? 'active' : '' }}">
                                <a
                                        href="{{url('/admin/distributor-replacement-forms/approved-orders')}}"><i
                                            class="fa fa-check"></i> Approved Orders</a>
                            </li>
                        </ul>
                    </li>

                @endif
            @endif

            @can('view_performance-report')
                <li class="{{ (request()->is('admin/performance-report*')) ? 'active' : '' }}"><a
                            href="{{url('/admin/performance-report')}}"><i class="fa fa-user"></i> Performance
                        Report</a></li>
            @endcan

            @if(Auth::user()->roles->first()->name=='Admin')
                <li class="treeview  {{  (request()->is('admin/product-catalogue-upload')) || (request()->is('admin/retailer-handiprize-upload')) || (request()->is('admin/mrp-dlp-upload')) || (request()->is('admin/mrp-clp-upload')) || (request()->is('admin/scheme-circular-upload')) || (request()->is('admin/warranty-circular-upload')) ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-upload"></i>Upload Catalogue <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/product-catalogue-upload')) || (request()->is('admin/retailer-handiprize-upload')) || (request()->is('admin/mrp-dlp-upload')) || (request()->is('admin/mrp-clp-upload')) || (request()->is('admin/scheme-circular-upload')) || (request()->is('admin/warranty-circular-upload')) ? 'display:block' : '' }}">
                        <li class="{{ (request()->is('admin/product-catalogue-upload')) ? 'active' : '' }}">
                            <a href="{{url('/admin/product-catalogue-upload')}}">
                                <i class="fa fa-file-pdf-o"></i>
                                Product Catalogue Upload
                            </a>
                        </li>
                        <li class="{{ (request()->is('admin/retailer-handiprize-upload')) ? 'active' : '' }}">
                            <a href="{{url('/admin/retailer-handiprize-upload')}}">
                                <i class="fa fa-file-pdf-o"></i>
                                Retailer Handiprize Upload
                            </a>
                        </li>
                        <li class="{{ (request()->is('admin/mrp-dlp-upload')) ? 'active' : '' }}">
                            <a href="{{url('/admin/mrp-dlp-upload')}}">
                                <i class="fa fa-file-pdf-o"></i>
                                MRP DLP Upload
                            </a>
                        </li>
                        <li class="{{ (request()->is('admin/mrp-clp-upload')) ? 'active' : '' }}">
                            <a href="{{url('/admin/mrp-clp-upload')}}">
                                <i class="fa fa-file-pdf-o"></i>
                                MRP CLP Upload
                            </a>
                        </li>
                        <li class="{{ (request()->is('admin/scheme-circular-upload')) ? 'active' : '' }}">
                            <a href="{{url('/admin/scheme-circular-upload')}}">
                                <i class="fa fa-file-pdf-o"></i>
                                Scheme Circular
                            </a>
                        </li>
                        <li class="{{ (request()->is('admin/warranty-circular-upload')) ? 'active' : '' }}">
                            <a href="{{url('/admin/warranty-circular-upload')}}">
                                <i class="fa fa-file-pdf-o"></i>
                                Warranty Circular
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            @if(Auth::user()->roles->first()->name=='Account User' || Auth::user()->roles->first()->name=='Junior Account')
                @if(Auth::user()->roles->first()->name=='Account User')
                    <li class="treeview  {{ (request()->is('admin/tallyproductssalesreports*')) || (request()->is('admin/tallypartssalesreports*')) ? 'menu-open' : '' }} ">
                        <a href="#"><i class="fa fa-file"></i> Daily Tally Reports <i
                                    class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu"
                            style="{{ (request()->is('admin/tallyproductssalesreports*'))  || (request()->is('admin/tallypartssalesreports*')) ? 'display:block' : '' }}">
                            <li class="{{ (request()->is('admin/tallyproductssalesreports*')) ? 'active' : '' }}"><a
                                        href="{{url('admin/tallyproductssalesreports')}}"><i
                                            class="fa fa-file"></i>S-Form Order</a></li>

                            <li class="{{ (request()->is('admin/tallypartssalesreports*')) ? 'active' : '' }}">
                                <a
                                        href="{{url('admin/tallypartssalesreports')}}"><i
                                            class="fa fa-file"></i>P-Form Order</a></li>

                            <li class="active">
                                <a href="{{url('admin/verify-tally-sheet')}}"><i class="fa fa-file"></i>Verify Tally
                                    Sheet</a>
                            </li>
                        </ul>
                    </li>
                @endif
                <li class="treeview  {{ (request()->is('admin/scheme-credit-notes*')) || (request()->is('admin/return-parts-credit-notes*')) || (request()->is('admin/scheme-credit-notes*')) ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-inr"></i> Generate Credit Note <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/rcform*'))  || (request()->is('admin/rform_pending_orders*')) ||  (request()->is('admin/category-masters*')) ? 'display:block' : '' }}">
                        @if(Auth::user()->roles->first()->name=='Account User')

                        <li class="treeview ">
                            <a href="#"><i class="fa fa-inr"></i> Credit Notes <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class=""><a
                                            href="{{url('admin/category-masters')}}"><i class="fa fa-bank" aria-hidden="true"></i> CN/DN Category Master</a>
                                </li>
                                <li class=""><a
                                            href="{{url('admin/manual-credit-notes')}}"><i class="fa fa-home" aria-hidden="true"></i> Manual Credit Note</a>
                                </li>


                                    <li class=""><a
                                                href="{{url('admin/journal-vouchers')}}"><i class="fa fa-home"
                                                                                            aria-hidden="true"></i>
                                            Journal Voucher</a>
                                    </li>

                                </ul>
                            </li>
                        @endif
                        <li class="treeview ">
                            <a href="#"><i class="fa fa-inr"></i> Scheme Credit Notes <i
                                        class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li class=""><a
                                            href="{{url('admin/scheme-credit-notes/current')}}"><i class="fa fa-bank"
                                                                                                   aria-hidden="true"></i>
                                        Current Scheme</a>
                                </li>

                                <li class=""><a
                                            href="{{url('admin/scheme-credit-notes/expire')}}"><i class="fa fa-home"
                                                                                                  aria-hidden="true"></i>
                                        Expire Scheme</a>
                                </li>

                            </ul>
                        </li>
                        @if(Auth::user()->roles->first()->name=='Account User')

                            <li class="treeview ">
                                <a href="#"><i class="fa fa-inr"></i> Return Parts Credit Note <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">
                                    <li class="{{ (request()->is('admin/return-parts-credit-notes*')) ? 'active' : '' }}">
                                        <a href="{{url('admin/return-parts-credit-notes')}}"><i
                                                    class="fa fa-inr"></i> Pending Credit Note
                                        </a>
                                    </li>
                                    <li class="{{ (request()->is('admin/completed-return-parts-credit-notes*')) ? 'active' : '' }}">
                                        <a href="{{url('admin/completed-return-parts-credit-notes')}}"><i
                                                    class="fa fa-inr"></i> Completed Credit Note
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li class="treeview ">
                                <a href="#"><i class="fa fa-inr"></i> RC Form Credit Note <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">
                                    <li class="{{ (request()->is('admin/rc-credit-notes*')) ? 'active' : '' }}">
                                        <a href="{{url('admin/rc-credit-notes')}}"><i
                                                    class="fa fa-inr"></i> Pending Credit Note
                                        </a>
                                    </li>
                                    <li class="{{ (request()->is('admin/completed-rc-credit-notes*')) ? 'active' : '' }}">
                                        <a href="{{url('admin/completed-rc-credit-notes')}}"><i
                                                    class="fa fa-inr"></i> Completed Credit Note
                                        </a>
                                    </li>

                                </ul>
                            </li>

                        @endif

                    </ul>

                </li>
            @endif
            @if(Auth::user()->roles->first()->name=='Account User' || Auth::user()->roles->first()->name=='Senior Account Manager')


                <li class="treeview ">
                    <a href="#"><i class="fa fa-book"></i> Corporate Sform Order <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class=""><a
                                    href="{{url('admin/corporate-pending-sform-order')}}"><i class="fa fa-bank"
                                                                                             aria-hidden="true"></i>
                                Pending Order</a>
                        </li>

                        <li class=""><a
                                    href="{{url('admin/corporate-approved-sform-order')}}"><i class="fa fa-home"
                                                                                              aria-hidden="true"></i>
                                Approved Order</a>
                        </li>

                        <li class=""><a
                                    href="{{url('admin/corporate-dispatch-sform-order')}}"><i class="fa fa-home"
                                                                                              aria-hidden="true"></i>
                                Dispatched Order</a>
                        </li>

                    </ul>
                </li>

            @endif
            @if(Auth::user()->roles->first()->name=='Dispatch' || Auth::user()->roles->first()->name=='Store Keeper' || Auth::user()->roles->first()->name=='Admin' || Auth::user()->roles->first()->name =='Finance')


                <li class="treeview {{(request()->is('admin/daily-dispatch-details*'))  ||(request()->is('admin/rg1-category-wise-summary-report*'))  || (request()->is('admin/rg1-summary-report*')) || (request()->is('admin/hsn-summary-report*'))  || (request()->is('admin/mrp-wise-stock-report*'))  ||  (request()->is('admin/model-wise-stock-report*'))  || (request()->is('admin/gst-sub-catagories-wise-report*'))  || (request()->is('admin/daily-sales-report-details*'))  ||(request()->is('admin/cu-capacity-sales-report*'))  ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-file"></i> Sales Reports<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{(request()->is('admin/daily-dispatch-details*'))  ||(request()->is('admin/rg1-category-wise-summary-report*'))  || (request()->is('admin/rg1-summary-report*')) || (request()->is('admin/hsn-summary-report*'))  || (request()->is('admin/mrp-wise-stock-report*'))  ||  (request()->is('admin/model-wise-stock-report*'))  || (request()->is('admin/gst-sub-catagories-wise-report*'))  || (request()->is('admin/daily-sales-report-details*')) || (request()->is('admin/cu-capacity-sales-report*')) ? 'display:block' : '' }}">
                        <li class="{{ (request()->is('admin/daily-sales-report-details*')) ? 'active' : '' }}">
                            <a href="{{url('admin/daily-sales-report-details')}}"><i class="fa fa-file"
                                                                                     aria-hidden="true"></i> Daily Sales
                                Report
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/daily-dispatch-details*')) ? 'active' : '' }}">
                            <a href="{{url('admin/daily-dispatch-details')}}"><i class="fa fa-file"
                                                                                     aria-hidden="true"></i> Daily Dispatch Details
                            </a>
                        </li>
                        

                        <li class="{{ (request()->is('admin/cu-capacity-sales-report*')) ? 'active' : '' }}">
                            <a href="{{url('admin/cu-capacity-sales-report')}}"><i class="fa fa-file"
                                                                                   aria-hidden="true"></i> Cu.Capacity
                                Sales Report
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/gst-sub-catagories-wise-report*')) ? 'active' : '' }}">
                            <a href="{{url('admin/gst-sub-catagories-wise-report')}}"><i class="fa fa-file"
                                                                                         aria-hidden="true"></i> GST Sub
                                Catagories wise
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/model-wise-stock-report*')) ? 'active' : '' }}">
                            <a href="{{url('admin/model-wise-stock-report')}}"><i class="fa fa-file"
                                                                                  aria-hidden="true"></i> Model Wise
                                Stock Report
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/mrp-wise-stock-report*')) ? 'active' : '' }}">
                            <a href="{{url('admin/mrp-wise-stock-report')}}"><i class="fa fa-file"
                                                                                aria-hidden="true"></i> MRP Wise Stock
                                Report
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/hsn-summary-report*')) ? 'active' : '' }}">
                            <a href="{{url('admin/hsn-summary-report')}}"><i class="fa fa-file" aria-hidden="true"></i>
                                HSN Summary Report
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/rg1-summary-report*')) ? 'active' : '' }}">
                            <a href="{{url('admin/rg1-summary-report')}}"><i class="fa fa-file" aria-hidden="true"></i>
                                RG1 Summary Report
                            </a>
                        </li>

                        <li class="{{ (request()->is('admin/rg1-category-wise-summary-report*')) ? 'active' : '' }}">
                            <a href="{{url('admin/rg1-category-wise-summary-report')}}"><i class="fa fa-file" aria-hidden="true"></i>RG1 Category Summary Report
                            </a>
                        </li>    
                    </ul>
                </li>

                @if(Auth::user()->roles->first()->name=='Store Keeper' || Auth::user()->roles->first()->name=='Admin')  
                <li class="{{ (request()->is('admin/live-status-report*')) ? 'active' : '' }}">
                            <a href="{{url('admin/live-status-report')}}"><i class="fa fa-file" aria-hidden="true"></i> Live Goods Status </a>
                        </li>
                @endif
            @endif

            @if(Auth::user()->roles->first()->name=='Admin' || Auth::user()->roles->first()->name =='Finance')
                <li class="treeview  {{(request()->is('admin/overall_production_and_sale*')) || (request()->is('admin/overall_production_and_sell*')) ||  (request()->is('admin/partwise-bsr*')) || (request()->is('admin/opening_stock_quantity*')) || (request()->is('admin/finished_goods_bsr_report*')) || (request()->is('admin/input_production_quantity*')) || (request()->is('admin/input_salesreturn_quantity*')) || (request()->is('admin/inputopeningstock*')) || (request()->is('admin/partwise-bsr*')) ? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>Quantity Intake & BSR<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{(request()->is('admin/overall_production_and_sale*')) || (request()->is('admin/overall_production_and_sell*')) || (request()->is('admin/partwise-bsr*')) ||(request()->is('admin/opening_stock_quantity*')) ||(request()->is('admin/finished_goods_bsr_report*')) || (request()->is('admin/input_production_quantity*')) || (request()->is('admin/input_salesreturn_quantity*')) ? 'display:block' : '' }}">
                        <li class="{{ (request()->is('admin/input_production_quantity*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/input_production_quantity')}}"><i class="fa fa-book"
                                                                                        aria-hidden="true"></i>Intake
                                Production Quantity</a></li>
                        <li class="{{ (request()->is('admin/input_salesreturn_quantity*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/input_salesreturn_quantity')}}"><i class="fa fa-book"
                                                                                         aria-hidden="true"></i>Intake
                                Sales Return Quantity</a></li>
                        <li class="{{ (request()->is('admin/opening_stock_quantity*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/opening_stock_quantity')}}"><i class="fa fa-book"
                                                                                  aria-hidden="true"></i>Intake Opening
                                Stock</a></li>

                        <li class="{{ (request()->is('admin/finished_goods_bsr_report')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/finished_goods_bsr_report')}}"><i class="fa fa-book" aria-hidden="true"></i>BSR
                                Report Finished goods</a></li>
                        <li class="{{ (request()->is('admin/partwise-bsr*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/partwise-bsr')}}"><i class="fa fa-book" aria-hidden="true"></i>BSR
                                Report Parts</a></li>

                                <li class="{{ (request()->is('admin/overall_production_and_sale*')) ? 'active' : '' }}"><a
                                    href="{{url('/admin/overall_production_and_sale')}}"><i class="fa fa-joomla" aria-hidden="true"></i>Overall Production And Sale</a></li>
                    </ul>
                </li>
            @endif

            @if(Auth::user()->roles->first()->name=='Report Admin')    
                <li class="{{ (request()->is('admin/mobile-report/division-wise-revenue-report*')) ? 'active' : '' }}"><a href="{{url('/admin/mobile-report/division-wise-revenue-report')}}"><i class="fa fa-book" aria-hidden="true"></i>Division Wise Revenue</a></li>
                <li class="{{ (request()->is('admin/mobile-report/category-wise-report*')) ? 'active' : '' }}"><a href="{{url('/admin/mobile-report/category-wise-report')}}"><i class="fa fa-book" aria-hidden="true"></i>Category Wise Revenue</a></li>
                <li class="{{ (request()->is('admin/mobile-report/distributor-wise-revenue*')) ? 'active' : '' }}"><a href="{{url('/admin/mobile-report/distributor-wise-revenue')}}"><i class="fa fa-book" aria-hidden="true"></i>Distributor Wise Revenue</a></li>
                <li class="{{ (request()->is('admin/live-inventory-status-report*')) ? 'active' : '' }}"><a href="{{url('/admin/live-inventory-status-report')}}"><i class="fa fa-book" aria-hidden="true"></i>Low Inventory Status</a></li>
            @endif



            @if(Auth::user()->roles->first()->name=='Admin' || Auth::user()->roles->first()->name =='Finance')
                <li class="treeview  {{ (request()->is('admin/listing-exports-orders*'))  || (request()->is('admin/export-pending-orders*'))  || (request()->is('admin/create-exports-orders*')) || (request()->is('admin/listing-exports-orders*'))? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-shopping-cart"></i> Export Orders<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/export-pending-orders*'))  ||(request()->is('admin/listing-exports-orders*'))  || (request()->is('admin/saved-invoices-final*')) ? 'display:block' : '' }}">

                        <li class="{{ (request()->is('admin/export-pending-orders*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/export-pending-orders')}}"><i
                                        class="fa fa-clock-o"></i> Pending Orders</a>
                        </li>


                        <li class="{{ (request()->is('admin/listing-exports-orders')) ? 'active' : '' }}">
                            <a href="{{url('admin/listing-exports-orders')}}"><i
                                        class="fa fa-list"></i> Export Order Details</a>
                        </li>


                    </ul>
                </li>

                <li class="treeview  {{ (request()->is('admin/nepal-pending-orders*'))  || (request()->is('admin/nepal-exports-orders*')) || (request()->is('admin/listing-nepal-orders*'))? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-shopping-cart"></i>  Nepal Orders<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu"
                        style="{{ (request()->is('admin/nepal-pending-orders*'))  || (request()->is('admin/listing-nepal-orders*')) || (request()->is('admin/saved-invoices-final*')) ? 'display:block' : '' }}">

                        <li class="{{ (request()->is('admin/nepal-pending-orders*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/nepal-pending-orders')}}"><i
                                        class="fa fa-clock-o"></i> Pending Orders</a>
                        </li>


                        <li class="{{ (request()->is('admin/listing-nepal-orders')) ? 'active' : '' }}">
                            <a href="{{url('admin/listing-nepal-orders')}}"><i
                                        class="fa fa-list"></i> Nepal Order Details</a>
                        </li>
                    </ul>
                </li>

            @endcan
            @if(Auth::user()->roles->first()->name =='Export Account Manager')

                <li class="{{ (request()->is('admin/exportdistributors')) ? 'active' : '' }}">
                    <a href="{{url('admin/exportdistributors')}}"><i
                                class="fa fa-users"></i> Distributor Master</a>
                </li>

                <li class="treeview  {{ (request()->is('admin/exp-pending-order*'))  || (request()->is('admin/exp-approved-order*')) || (request()->is('admin/exp-dispatched-order*'))? 'menu-open' : '' }} ">
                    <a href="#"><i class="fa fa-pencil-square-o"></i> Export Orders<i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ (request()->is('admin/exp-pending-order*')) ? 'active' : '' }}"><a
                                    href="{{url('admin/exp-pending-order')}}"><i
                                        class="fa fa-shopping-cart"></i> Pending Orders</a>
                        </li>
                        <li class="{{ (request()->is('admin/exp-approved-order')) ? 'active' : '' }}">
                            <a href="{{url('admin/exp-approved-order')}}"><i
                                        class="fa fa-shopping-cart"></i> Approved Order</a>
                        </li>

                        <li class="{{ (request()->is('admin/exp-dispatched-order')) ? 'active' : '' }}">
                            <a href="{{url('admin/exp-dispatched-order')}}"><i
                                        class="fa fa-shopping-cart"></i> Dispatched Order</a>
                        </li>
                    </ul>
                </li>

            <li class="treeview  {{ (request()->is('admin/nep-pending-order*'))  || (request()->is('admin/nep-approved-order*')) || (request()->is('admin/nep-dispatched-order*'))? 'menu-open' : '' }} ">
                <a href="#"><i class="fa fa-pencil-square-o"></i> Nepal Orders<i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ (request()->is('admin/nep-pending-order*')) ? 'active' : '' }}"><a
                                href="{{url('admin/nep-pending-order')}}"><i
                                    class="fa fa-shopping-cart"></i> Pending Orders</a>
                    </li>


                    <li class="{{ (request()->is('admin/nep-approved-order')) ? 'active' : '' }}">
                        <a href="{{url('admin/nep-approved-order')}}"><i
                                    class="fa fa-shopping-cart"></i> Approved Order</a>
                    </li>

                    <li class="{{ (request()->is('admin/nep-dispatched-order')) ? 'active' : '' }}">
                        <a href="{{url('admin/nep-dispatched-order')}}"><i
                                    class="fa fa-shopping-cart"></i> Dispatched Order</a>
                    </li>
                </ul>
            </li>
                @endif
        </ul>
    </section>
</aside>