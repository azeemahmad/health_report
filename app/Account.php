<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Account extends Model
{
     use SoftDeletes;
     /* The database table used by the model.
     *
     * @var string
     */
     use  HasRoles;
    protected $table = 'accounts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date','sales_person_name', 'distance', 'advanced_amount', 'total_amount','remark','total_lead'];

    
}
