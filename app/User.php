<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'subcategories_id', 'name', 'email', 'password','image','city','state','mobile','user_name',
       'region_id','categories_id','cart','p_cart','r_cart','new_password','report_login_otp','report_login_flag'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function region()
    {
        return $this->belongsTo('App\Region', 'region_id');
    }

 public function SalesPersonDistributor(){
        return $this->hasMany('App\SalesPersonDistributor', 'field_staff_monitor_id');
    }


 public function production_parts(){
    return $this->hasMany('App\Part', 'production_user_id');
}
}
