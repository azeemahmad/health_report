<?php

namespace App\Helpers;

use App\AnnualWiseScheme;
use App\Category;
use App\Categoryscheme;
use App\Coupon;
use App\ForeignWiseScheme;
use App\InvoiceOrderList;
use App\InvoiceOrderProductList;
use App\InvoicewiseScheme;
use App\MixedCategoryScheme;
use App\MixedModelScheme;
use App\Models\SFormOrderProductLists;
use App\MonthlyewiseScheme;
use App\MonthlyModelWiseScheme;
use App\PartCategorySchemes;
use App\Product;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use App\Distributor;
use Intervention\Image\Facades\Image as Image;
use App\Models\ReportData;
use App\DealerCoupon;


class DiscountDetails
{
    public static function distributorDiscount($id, $quantity, $distributor_id,$perbox=null)
    {


        $product = Product::findOrFail($id);
        $discount = Coupon::where('status', 1)->get();
        $distributor = Distributor::find($distributor_id);

//            $discount->map(function ($q) {
//              //  $q->product_ids = explode(',', $q->product_ids);
//                $q->category_ids = explode(',', $q->category_ids);
//                $q->sub_category_ids = explode(',', $q->sub_category_ids);
//                $q->child_category_ids = explode(',', $q->child_category_ids);
//                $q->distributor_ids = explode(',', $q->distributor_ids);
//                $q->state_ids = explode(',', $q->state_ids);
//                $q->zone_ids = explode(',', $q->zone_ids);
//            });


        // $taxablePrice = $product->mrp;
        //  $discountPercent = 0;

        $productDiscounts = $discount->pluck('product_ids', 'id');
        $childCategoryDiscounts = $discount->pluck('child_category_ids', 'id');
        $subCategoryDiscounts = $discount->pluck('sub_category_ids', 'id');
        //  $categoryDiscounts = $discount->pluck('category_ids', 'id');
        $distributorDetails=$discount->pluck('distributor_ids','id');
        //  $stateDiscounts = $discount->pluck('state_ids', 'id');

        /*--------START  PRODUCT WISE DISCOUNT----------------------*/
         foreach ($distributorDetails as $disk => $disv){
             $distridetails=array_filter(explode(',', $disv));
             if (!empty($distridetails)) {
                 if (in_array($distributor_id, $distridetails)) {
                     $coupon = Coupon::find($disk);

                     $productDiscountsCorp = array_filter(explode(',', $coupon->product_ids));
                     $childCategoryDiscountsCorp = array_filter(explode(',', $coupon->child_category_ids));
                     $subCategoryDiscountsCorp = array_filter(explode(',', $coupon->sub_category_ids));

                     if (!empty($productDiscountsCorp)) {
                         if (in_array($product->id, $productDiscountsCorp)) {
                             if ($coupon->discount_type == 'percentage') {
                                 $discountPercent = $coupon->discount;
                                 $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                 if(isset($perbox) && $perbox != null){
                                     $totalOrderQuantity = $perbox * $quantity;
                                 }
                                 else{
                                     $totalOrderQuantity = $product->product_per_box * $quantity;
                                 }

                                 $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                 $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                 $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                 $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                 $totalProductCost = $totalTaxablePrice + $totalTax;
                                 $weight = $quantity * $product->product_weight;
                                 $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                 $data = [
                                     'total_mrp' => $totalOrderQuantity * $product->mrp,
                                     'taxable_amount' => $totalTaxablePrice,
                                     'discount_percent' => $discountPercent,
                                     'total_discount' => $totalDiscount,
                                     'total_tax' => $totalTax,
                                     'total_item_cost' => $totalProductCost,
                                     'weight' => $weight,
                                     'volume' => $volume,
                                     'total_item_quantities' => $totalOrderQuantity,
                                     'order_quantity' => $quantity
                                 ];
                                 return $data;
                             } else {
                                 $taxablePrice = $product->mrp - $coupon->discount;
                                 $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                 if(isset($perbox) && $perbox != null){
                                     $totalOrderQuantity = $perbox * $quantity;
                                 }
                                 else{
                                     $totalOrderQuantity = $product->product_per_box * $quantity;
                                 }
                                 $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                 $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                 $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                 $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                 $totalProductCost = $totalTaxablePrice + $totalTax;
                                 $weight = $quantity * $product->product_weight;
                                 $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                 $data = [
                                     'total_mrp' => $totalOrderQuantity * $product->mrp,
                                     'taxable_amount' => $totalTaxablePrice,
                                     'discount_percent' => $discountPercent,
                                     'total_discount' => $totalDiscount,
                                     'total_tax' => $totalTax,
                                     'total_item_cost' => $totalProductCost,
                                     'weight' => $weight,
                                     'volume' => $volume,
                                     'total_item_quantities' => $totalOrderQuantity,
                                     'order_quantity' => $quantity
                                 ];
                                 return $data;
                             }

                         }
                     }
                     elseif (!empty($childCategoryDiscountsCorp)) {
                         if (in_array($product->child_category_id, $childCategoryDiscountsCorp)) {
                             if ($coupon->discount_type == 'percentage') {
                                 $discountPercent = $coupon->discount;
                                 $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                 if(isset($perbox) && $perbox != null){
                                     $totalOrderQuantity = $perbox * $quantity;
                                 }
                                 else{
                                     $totalOrderQuantity = $product->product_per_box * $quantity;
                                 }
                                 $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                 $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                 $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                 $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                 $totalProductCost = $totalTaxablePrice + $totalTax;
                                 $weight = $quantity * $product->product_weight;
                                 $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                 $data = [
                                     'total_mrp' => $totalOrderQuantity * $product->mrp,
                                     'taxable_amount' => $totalTaxablePrice,
                                     'discount_percent' => $discountPercent,
                                     'total_discount' => $totalDiscount,
                                     'total_tax' => $totalTax,
                                     'total_item_cost' => $totalProductCost,
                                     'weight' => $weight,
                                     'volume' => $volume,
                                     'total_item_quantities' => $totalOrderQuantity,
                                     'order_quantity' => $quantity
                                 ];
                                 return $data;
                             } else {
                                 $taxablePrice = $product->mrp - $coupon->discount;
                                 $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                 if(isset($perbox) && $perbox != null){
                                     $totalOrderQuantity = $perbox * $quantity;
                                 }
                                 else{
                                     $totalOrderQuantity = $product->product_per_box * $quantity;
                                 }
                                 $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                 $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                 $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                 $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                 $totalProductCost = $totalTaxablePrice + $totalTax;
                                 $weight = $quantity * $product->product_weight;
                                 $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                 $data = [
                                     'total_mrp' => $totalOrderQuantity * $product->mrp,
                                     'taxable_amount' => $totalTaxablePrice,
                                     'discount_percent' => $discountPercent,
                                     'total_discount' => $totalDiscount,
                                     'total_tax' => $totalTax,
                                     'total_item_cost' => $totalProductCost,
                                     'weight' => $weight,
                                     'volume' => $volume,
                                     'total_item_quantities' => $totalOrderQuantity,
                                     'order_quantity' => $quantity
                                 ];
                                 return $data;
                             }
                         }
                     }
                     elseif (!empty($subCategoryDiscountsCorp)) {
                         if (in_array($product->subcategory_id, $subCategoryDiscountsCorp)) {
                             if ($coupon->discount_type == 'percentage') {
                                 $discountPercent = $coupon->discount;
                                 $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                 if(isset($perbox) && $perbox != null){
                                     $totalOrderQuantity = $perbox * $quantity;
                                 }
                                 else{
                                     $totalOrderQuantity = $product->product_per_box * $quantity;
                                 }
                                 $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                 $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                 $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                 $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                 $totalProductCost = $totalTaxablePrice + $totalTax;
                                 $weight = $quantity * $product->product_weight;
                                 $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                 $data = [
                                     'total_mrp' => $totalOrderQuantity * $product->mrp,
                                     'taxable_amount' => $totalTaxablePrice,
                                     'discount_percent' => $discountPercent,
                                     'total_discount' => $totalDiscount,
                                     'total_tax' => $totalTax,
                                     'total_item_cost' => $totalProductCost,
                                     'weight' => $weight,
                                     'volume' => $volume,
                                     'total_item_quantities' => $totalOrderQuantity,
                                     'order_quantity' => $quantity
                                 ];
                                 return $data;
                             } else {
                                 $taxablePrice = $product->mrp - $coupon->discount;
                                 $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                 if(isset($perbox) && $perbox != null){
                                     $totalOrderQuantity = $perbox * $quantity;
                                 }
                                 else{
                                     $totalOrderQuantity = $product->product_per_box * $quantity;
                                 }
                                 $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                 $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                 $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                 $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                 $totalProductCost = $totalTaxablePrice + $totalTax;
                                 $weight = $quantity * $product->product_weight;
                                 $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                 $data = [
                                     'total_mrp' => $totalOrderQuantity * $product->mrp,
                                     'taxable_amount' => $totalTaxablePrice,
                                     'discount_percent' => $discountPercent,
                                     'total_discount' => $totalDiscount,
                                     'total_tax' => $totalTax,
                                     'total_item_cost' => $totalProductCost,
                                     'weight' => $weight,
                                     'volume' => $volume,
                                     'total_item_quantities' => $totalOrderQuantity,
                                     'order_quantity' => $quantity
                                 ];
                                 return $data;
                             }

                         }
                     }

                 }
             }

         }

        foreach ($productDiscounts as $pk => $pv) {

            $productDetails = array_filter(explode(',', $pv));

            if (!empty($productDetails)) {
                if (in_array($product->id, $productDetails)) {

                    $coupon = Coupon::find($pk);
                    $stateIds = explode(',', $coupon->state_ids);
                    $checkDistributor = array_filter(explode(',', $coupon->distributor_ids));

                    if (!empty($checkDistributor) && in_array($distributor->id, $checkDistributor)) {

                        if (in_array($distributor->state_id, $stateIds)) {

                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    } else {

                        if (in_array($distributor->state_id, $stateIds)) {

                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    }

                }
            }
        }

        /*--------END  PRODUCT WISE DISCOUNT----------------------*/


        /*--------START  CHILD CATEGORY WISE DISCOUNT----------------------*/
        foreach ($childCategoryDiscounts as $pk => $pv) {
            $productDetails = array_filter(explode(',', $pv));
            if (!empty($productDetails)) {
                if (in_array($product->child_category_id, $productDetails)) {
                    $coupon = Coupon::find($pk);
                    $stateIds = explode(',', $coupon->state_ids);
                    $checkDistributor = array_filter(explode(',', $coupon->distributor_ids));
                    if (!empty($checkDistributor) && in_array($distributor->id, $checkDistributor)) {
                        if (in_array($distributor->state_id, $stateIds)) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    } else {
                        if (in_array($distributor->state_id, $stateIds)) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    }

                }
            }
        }
        /*--------END  CHILD CATEGORY WISE DISCOUNT----------------------*/


        /*--------START  SUB CATEGORY WISE DISCOUNT----------------------*/
        foreach ($subCategoryDiscounts as $pk => $pv) {
            $productDetails = array_filter(explode(',', $pv));
            if (!empty($productDetails)) {
                if (in_array($product->subcategory_id, $productDetails)) {
                    $coupon = Coupon::find($pk);
                    $stateIds = explode(',', $coupon->state_ids);
                    $checkDistributor = array_filter(explode(',', $coupon->distributor_ids));
                    if (!empty($checkDistributor) && in_array($distributor->id, $checkDistributor)) {
                        if (in_array($distributor->state_id, $stateIds)) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    } else {
                        if (in_array($distributor->state_id, $stateIds)) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    }

                }
            }
        }
        /*--------END  SUB CATEGORY WISE DISCOUNT----------------------*/


        /*--------START  CATEGORY WISE DISCOUNT----------------------*/
//        foreach ($categoryDiscounts as $pk => $pv) {
//            $productDetails = array_filter(explode(',', $pv));
//            if (!empty($productDetails)) {
//                if (in_array($product->category_id, $productDetails)) {
//                    $coupon = Coupon::find($pk);
//                    $stateIds=explode(',',$coupon->state_ids);
//                    if(in_array($distributor->state_id,$stateIds)) {
//                        if ($coupon->discount_type == 'percentage') {
//                            $discountPercent = $coupon->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                            $totalOrderQuantity = $product->product_per_box * $quantity;
//                            $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
//                            $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
//                            $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
//                            $totalTax = ($taxRate * $totalTaxablePrice) / 100;
//                            $totalProductCost = $totalTaxablePrice + $totalTax;
//                            $weight = $quantity * $product->product_weight;
//                            $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
//                            $data = [
//                                'total_mrp' => $totalOrderQuantity * $product->mrp,
//                                'taxable_amount' => $totalTaxablePrice,
//                                'discount_percent' => $discountPercent,
//                                'total_discount' => $totalDiscount,
//                                'total_tax' => $totalTax,
//                                'total_item_cost' => $totalProductCost,
//                                'weight' => $weight,
//                                'volume' => $volume,
//                                'total_item_quantities' => $totalOrderQuantity,
//                                'order_quantity' => $quantity
//                            ];
//                            return $data;
//                        } else {
//                            $taxablePrice = $product->mrp - $coupon->discount;
//                            $discountPercent = ($coupon->discount / $product->mrp) * 100;
//                            $totalOrderQuantity = $product->product_per_box * $quantity;
//                            $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
//                            $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
//                            $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
//                            $totalTax = ($taxRate * $totalTaxablePrice) / 100;
//                            $totalProductCost = $totalTaxablePrice + $totalTax;
//                            $weight = $quantity * $product->product_weight;
//                            $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
//                            $data = [
//                                'total_mrp' => $totalOrderQuantity * $product->mrp,
//                                'taxable_amount' => $totalTaxablePrice,
//                                'discount_percent' => $discountPercent,
//                                'total_discount' => $totalDiscount,
//                                'total_tax' => $totalTax,
//                                'total_item_cost' => $totalProductCost,
//                                'weight' => $weight,
//                                'volume' => $volume,
//                                'total_item_quantities' => $totalOrderQuantity,
//                                'order_quantity' => $quantity
//                            ];
//                            return $data;
//                        }
//                    }
//                }
//            }
//        }
        /*--------END  CATEGORY WISE DISCOUNT----------------------*/

//
//
//
//        if (isset($discount) && $discount->isNotEmpty()) {
//            foreach ($discount as $dk => $dv) {
//
//                if ($dv->distributor_ids && !empty(array_filter($dv->distributor_ids)) && $dv->product_ids && !empty(array_filter($dv->product_ids))) {
//                    if ((in_array($product->id, $dv->product_ids) || in_array('all', $dv->product_ids)) && in_array($distributor_id, $dv->distributor_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->distributor_ids && !empty(array_filter($dv->distributor_ids)) && $dv->child_category_ids && !empty(array_filter($dv->child_category_ids)) && empty(array_filter($dv->product_ids))) {
//                    if ((in_array($product->child_category_id, $dv->child_category_ids) || in_array('all', $dv->child_category_ids)) && in_array($distributor_id, $dv->distributor_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->distributor_ids && !empty(array_filter($dv->distributor_ids)) && $dv->sub_category_ids && !empty(array_filter($dv->sub_category_ids)) && empty(array_filter($dv->product_ids)) && empty(array_filter($dv->child_category_ids))) {
//                    if ((in_array($product->subcategory_id, $dv->sub_category_ids) || in_array('all', $dv->sub_category_ids)) && in_array($distributor_id, $dv->distributor_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->distributor_ids && !empty(array_filter($dv->distributor_ids)) && $dv->category_ids && !empty(array_filter($dv->category_ids)) && empty(array_filter($dv->product_ids)) && empty(array_filter($dv->child_category_ids)) && empty(array_filter($dv->sub_category_ids))) {
//                    if ((in_array($product->category_id, $dv->category_ids) || in_array('all', $dv->category_ids)) && in_array($distributor_id, $dv->distributor_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->product_ids && !empty(array_filter($dv->product_ids)) && empty(array_filter($dv->distributor_ids))) {
//                    if (in_array($product->id, $dv->product_ids) || in_array('all', $dv->product_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->child_category_ids && !empty(array_filter($dv->child_category_ids)) && empty(array_filter($dv->distributor_ids)) && empty(array_filter($dv->product_ids))) {
//                    if (in_array($product->child_category_id, $dv->child_category_ids) || in_array('all', $dv->child_category_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->sub_category_ids && !empty(array_filter($dv->sub_category_ids)) && empty(array_filter($dv->distributor_ids)) && empty(array_filter($dv->product_ids)) && empty(array_filter($dv->child_category_ids))) {
//                    if (in_array($product->subcategory_id, $dv->sub_category_ids) || in_array('all', $dv->sub_category_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->category_ids && !empty(array_filter($dv->category_ids)) && empty(array_filter($dv->distributor_ids)) && empty(array_filter($dv->product_ids)) && empty(array_filter($dv->child_category_ids)) && empty(array_filter($dv->sub_category_ids))) {
//                    if (in_array($product->category_id, $dv->category_ids) || in_array('all', $dv->category_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } else {
//                    $taxablePrice = $product->mrp;
//                    $discountPercent = 0;
//                }
//
//            }
//        } else {
//            $taxablePrice = $product->mrp;
//            $discountPercent = 0;
//        }

        $taxablePrice = $product->mrp;
        $discountPercent = 0;
        if(isset($perbox) && $perbox != null){
            $totalOrderQuantity = $perbox * $quantity;
        }
        else{
            $totalOrderQuantity = $product->product_per_box * $quantity;
        }
        $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
        $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
        $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
        $totalTax = ($taxRate * $totalTaxablePrice) / 100;
        $totalProductCost = $totalTaxablePrice + $totalTax;
        $weight = $quantity * $product->product_weight;
        $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
        $data = [
            'total_mrp' => $totalOrderQuantity * $product->mrp,
            'taxable_amount' => $totalTaxablePrice,
            'discount_percent' => $discountPercent,
            'total_discount' => $totalDiscount,
            'total_tax' => $totalTax,
            'total_item_cost' => $totalProductCost,
            'weight' => $weight,
            'volume' => $volume,
            'total_item_quantities' => $totalOrderQuantity,
            'order_quantity' => $quantity
        ];
        return $data;
    }


    public static function convertToIndianCurrency($number)
    {
        $no = round($number);
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(
            0 => '',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Forty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore', 'Arab', 'Kharab');
        while ($i < $digits_length) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural;
            } else {
                $str [] = null;
            }
        }

        $Rupees = implode(' ', array_reverse($str));
        $paise = ($decimal) ? "And " . ($words[$decimal - $decimal % 10]) . " " . ($words[$decimal % 10]) . " Paise " : '';
        return ($Rupees ? 'Indian Rupees ' . $Rupees : '') . $paise . "Only";
    }

    public static function IndianRupees($number)
    {
        $decimal = (string)($number - floor($number));
        $money = floor($number);
        $length = strlen($money);
        $delimiter = '';
        $money = strrev($money);

        for ($i = 0; $i < $length; $i++) {
            if (($i == 3 || ($i > 3 && ($i - 1) % 2 == 0)) && $i != $length) {
                $delimiter .= ',';
            }
            $delimiter .= $money[$i];
        }

        $result = strrev($delimiter);
        $decimal = preg_replace("/0\./i", ".", $decimal);
        $decimal = substr($decimal, 0, 3);

        if ($decimal != '0') {
            $result = $result . $decimal;
        }
        else{
            $result =$result.'.'.str_pad($decimal,2,'0',STR_PAD_LEFT);
        }

        return $result;
    }

    public static function checkProducts()
    {
        $category = Category::pluck('id')->toArray();
        $schemeCategory = Categoryscheme::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->latest()->whereIn('category_ids', $category)->get(['product_ids']);
        $partCategoryScheme = PartCategorySchemes::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->latest()->whereIn('category_ids', $category)->get(['product_ids']);
        $invoicewiseScheme = InvoicewiseScheme::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->latest()->whereIn('category_ids', $category)->get(['product_ids']);
        $monthlywiseScheme = MonthlyewiseScheme::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->latest()->whereIn('category_ids', $category)->get(['product_ids']);
        $monthlyModelwiseScheme = MonthlyModelWiseScheme::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->latest()->whereIn('category_ids', $category)->get(['product_ids']);
        $mixedModelScheme = MixedModelScheme::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->latest()->whereIn('category_ids', $category)->get(['product_ids']);
        $mixedSchemeCategory = MixedCategoryScheme::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->latest()->whereIn('category_ids', $category)->get(['product_ids']);
      //  $annualwiseScheme = AnnualWiseScheme::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->latest()->whereIn('category_ids', $category)->get(['product_ids']);
     //   $foreiginwiseScheme = ForeignWiseScheme::where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->latest()->whereIn('category_ids', $category)->get(['product_ids']);
        $schemeCategory->map(function ($q) {
            $q->product_ids = unserialize($q->product_ids);
        });
        if (isset($schemeCategory) && $schemeCategory->isNotEmpty()) {
            $scheme_cat = $schemeCategory->pluck('product_ids')->toArray();
        } else {
            $scheme_cat = [];
        }
        $mixedSchemeCategory->map(function ($q) {
            $q->product_ids = unserialize($q->product_ids);
        });
        if (isset($mixedSchemeCategory) && $mixedSchemeCategory->isNotEmpty()) {
            $scheme_cat_mixed = $mixedSchemeCategory->pluck('product_ids')->toArray();
        } else {
            $scheme_cat_mixed = [];
        }

        $partCategoryScheme->map(function ($q) {
            $q->product_ids = unserialize($q->product_ids);
        });
        if (isset($partCategoryScheme) && $partCategoryScheme->isNotEmpty()) {
            $scheme_part = $partCategoryScheme->pluck('product_ids')->toArray();
        } else {
            $scheme_part = [];
        }
        $invoicewiseScheme->map(function ($q) {
            $q->product_ids = unserialize($q->product_ids);
        });
        if (isset($invoicewiseScheme) && $invoicewiseScheme->isNotEmpty()) {
            $scheme_invoice = $invoicewiseScheme->pluck('product_ids')->toArray();
        } else {
            $scheme_invoice = [];
        }
        $monthlywiseScheme->map(function ($q) {
            $q->product_ids = unserialize($q->product_ids);
        });
        if (isset($monthlywiseScheme) && $monthlywiseScheme->isNotEmpty()) {
            $scheme_monthly = $monthlywiseScheme->pluck('product_ids')->toArray();
        } else {
            $scheme_monthly = [];
        }
        $monthlyModelwiseScheme->map(function ($q) {
            $q->product_ids = unserialize($q->product_ids);
        });
        if (isset($monthlyModelwiseScheme) && $monthlyModelwiseScheme->isNotEmpty()) {
            $scheme_monthly_model = $monthlyModelwiseScheme->pluck('product_ids')->toArray();
        } else {
            $scheme_monthly_model = [];
        }
        $mixedModelScheme->map(function ($q) {
            $q->product_ids = unserialize($q->product_ids);
        });
        if (isset($mixedModelScheme) && $mixedModelScheme->isNotEmpty()) {
            $scheme_mixed_model_model = $mixedModelScheme->pluck('product_ids')->toArray();
        } else {
            $scheme_mixed_model_model = [];
        }
//        $annualwiseScheme->map(function ($q) {
//            $q->product_ids = unserialize($q->product_ids);
//        });
//        if (isset($annualwiseScheme) && $annualwiseScheme->isNotEmpty()) {
//            $scheme_annual = $annualwiseScheme->pluck('product_ids')->toArray();
//        } else {
//            $scheme_annual = [];
//        }
//        $foreiginwiseScheme->map(function ($q) {
//            $q->product_ids = unserialize($q->product_ids);
//        });
//        if (isset($foreiginwiseScheme) && $foreiginwiseScheme->isNotEmpty()) {
//            $scheme_foreign = $foreiginwiseScheme->pluck('product_ids')->toArray();
//        } else {
//            $scheme_foreign = [];
//        }
      //  $mergeArray = array_merge($scheme_cat, $scheme_part, $scheme_invoice, $scheme_monthly, $scheme_annual, $scheme_foreign,$scheme_monthly_model);
        $mergeArray = array_merge($scheme_cat, $scheme_part, $scheme_invoice,$scheme_monthly,$scheme_monthly_model,$scheme_mixed_model_model,$scheme_cat_mixed);
        if(!empty($mergeArray)){
            $d = call_user_func_array('array_merge', $mergeArray);
            $finalProductList = array_unique($d);
        }
        else{
            $finalProductList = [];
        }

        return $finalProductList;

    }


    public static function lastThreeMonthOrder($id)
    {
        $distributorUser = Distributor::findOrFail($id);
        $division = unserialize($distributorUser->division);
        if (!empty($division)) {
            $category = Category::where('status',1)->whereIn('division_id', $division)->pluck('id');
            $subcategories = Category::where('status',1)->orderBy('parent_id', 'ASC')->whereIn('parent_id', $category)->groupBy('parent_id')->get();
        }
        $html = '        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close btn btn-danger btn-xs" data-dismiss="modal"><span style="background:red;color:white">Close</span></button>
                    <h4 class="modal-title" style="color: green">Last Three Month Order List</h4>
                    <h4 style="color:red"><b>Note : The Amount includes Scheme CN value</b></h4>
                    <h4 style="color:#5810e6"><b>Party Name : '.$distributorUser->distributor_firm_name.' - '.$distributorUser->city.'</b></h4>
                </div>
                <div class="modal-body modalTableCategory"><table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">Sr.no</th>
                            <th scope="col">Division</th>
                            <th scope="col">Category</th>
                            <th scope="col">Sub-Category</th>
                            <td style="color:indianred" colspan="2">Confirmed Order <br>(Invoice Pending)</td>
                            <td style="color:green" colspan="2">'.strtoupper(date('F')).' Order</td>
                            <td style="color:saddlebrown" colspan="2">'.strtoupper(date('F',strtotime("last day of -1 month"))).' Order</td>
                            <td style="color:red" colspan="2">'.strtoupper(date('F',strtotime("last day of -2 month"))).' Order</td>
                        </tr>
                        <tr>
                            <th colspan="4" rowspan="2"></th>
                            <td rowspan="1" style="color: darkblue"><b>Amount(Rs)</b></td>
                            <td style="color: #5b9909"><b>QTY</b></td>
                            <td rowspan="1" style="color: darkblue"><b>Amount(Rs)</b></td>
                            <td style="color: #5b9909"><b>QTY</b></td>
                            <td rowspan="1" style="color: darkblue"><b>Amount(Rs)</b></td>
                            <td style="color: #5b9909"><b>QTY</b></td>
                            <td rowspan="1" style="color: darkblue"><b>Amount(Rs)</b></td>
                            <td style="color: #5b9909"><b>QTY</b></td>
                        </tr>
                        </thead>  <tbody>';
        if (isset($subcategories) && $subcategories->isNotEmpty()) {
            $count = 0;
            foreach ($subcategories as $skk => $svv) {
                $checkcurrnetMonth = InvoiceOrderProductList::whereHas('invoiceOrderList',function ($q) use($id){
                    $q->where('distributor_id', $id)->where('invoice_status', 1)->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'));
                })->where('category_id', $svv->parent_id)->get();
                $grandTotalPendingAmount=SFormOrderProductLists::whereHas('sformOrder',function ($sform) use($id){
                    return  $sform->where('created_by', $id)->whereIn('order_status', [1,2,3,4]);
                })->where('category_id',  $svv->parent_id)->get();
                $checkLastMonth = InvoiceOrderProductList::whereHas('invoiceOrderList',function ($q) use($id){
                    $q->where('distributor_id', $id)->where('invoice_status', 1)->whereMonth('created_at', date('m', strtotime("last day of -1 month")))->whereYear('created_at', date('Y', strtotime("last day of -1 month")));
                })->where('category_id', $svv->parent_id)->get();
                $checkLastTwoMonth = InvoiceOrderProductList::whereHas('invoiceOrderList',function ($q) use($id){
                    $q->where('distributor_id', $id)->where('invoice_status', 1)->whereMonth('created_at', date('m', strtotime("last day of -2 month")))->whereYear('created_at', date('Y', strtotime("last day of -2 month")));
                })->where('category_id', $svv->parent_id)->get();
                $catparent = Category::where('status',1)->where('parent_id', $svv->parent_id)->orderBy('name', 'ASC')->get();
                foreach ($catparent as $sk => $sv) {
                    $checkcurrnetMonthProdcut = InvoiceOrderProductList::whereHas('invoiceOrderList',function ($q) use($id){
                        $q->where('distributor_id', $id)->where('invoice_status', 1)->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'));
                    })->where('sub_category_id', $sv->id)->get();
                    $checkLastMonthProdcut = InvoiceOrderProductList::whereHas('invoiceOrderList',function ($q) use($id){
                        $q->where('distributor_id', $id)->where('invoice_status', 1)->whereMonth('created_at', date('m', strtotime("last day of -1 month")))->whereYear('created_at', date('Y', strtotime("last day of -1 month")));
                    })->where('sub_category_id', $sv->id)->get();
                    $checkLastTwoMonthProdcut = InvoiceOrderProductList::whereHas('invoiceOrderList',function ($q) use($id){
                        $q->where('distributor_id', $id)->where('invoice_status', 1)->whereMonth('created_at', date('m', strtotime("last day of -2 month")))->whereYear('created_at', date('Y', strtotime("last day of -2 month")));
                    })->where('sub_category_id', $sv->id)->get();
                    $totalPendingAmount=SFormOrderProductLists::whereHas('sformOrder',function ($sform) use($id){
                       return  $sform->where('created_by', $id)->whereIn('order_status', [1,2,3,4]);
                    })->where('sub_category_id', $sv->id)->get();

                    $html .= '<tr>
                                        <td>' . ++$count . '</td>
                                        <td>' . $sv->subcategory->divisionname->name . '</td>
                                        <td>' . $sv->subcategory->name . '</td>
                                        <td><a href="'.url('/admin/pending-and-completed-order-model-wise/'.base64_encode($distributorUser->id).'/'.base64_encode($sv->id)).'" target="_blank">'.$sv->name.'</a></td>                                     
                                        <td><strong>';
                    if ($totalPendingAmount->isEmpty()) {
                        $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                    } else {
                        $html .= '<b style = "color:green" >' . $totalPendingAmount->sum('total_cost') . '</b >';
                    }
                    $html .= '</strong></td><td><strong>';
                    if ($totalPendingAmount->isEmpty()) {
                        $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                    } else {
                        $html .= '<b style = "color:green" >' . $totalPendingAmount->sum('total_item_quantities') . '</b >';
                    }
                    $html .= '</strong></td><td><strong>';


                    if ($checkcurrnetMonthProdcut->isEmpty()) {
                        $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                    } else {
                        $html .= '<b style = "color:green" >' . $checkcurrnetMonthProdcut->sum('total_cost') . '</b >';
                    }

                    $html .= '</strong></td><td><strong>';
                    if ($checkcurrnetMonthProdcut->isEmpty()) {
                        $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                    } else {
                        $html .= '<b style = "color:green" >' . $checkcurrnetMonthProdcut->sum('total_item_quantities') . '</b >';
                    }
                    $html .= '</strong></td>';

                    $html .= '<td><strong>';
                    if ($checkLastMonthProdcut->isEmpty()) {
                        $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                    } else {
                        $html .= '<b style = "color:green" >' . $checkLastMonthProdcut->sum('total_cost') . '</b >';
                    }
                    $html .= '</strong></td><td><strong>';
                    if ($checkLastMonthProdcut->isEmpty()) {
                        $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                    } else {
                        $html .= '<b style = "color:green" >' . $checkLastMonthProdcut->sum('total_item_quantities') . '</b >';
                    }
                    $html .= '</strong></td>';
                    $html .= '<td><strong>';
                    if ($checkLastTwoMonthProdcut->isEmpty()) {
                        $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                    } else {
                        $html .= '<b style = "color:green" >' . $checkLastTwoMonthProdcut->sum('total_cost') . '</b >';
                    }
                    $html .= '</strong></td><td><strong>';
                    if ($checkLastTwoMonthProdcut->isEmpty()) {
                        $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                    } else {
                        $html .= '<b style = "color:green" >' . $checkLastTwoMonthProdcut->sum('total_item_quantities') . '</b >';
                    }
                    $html .= '</strong></td>';


                    $html .= '</tr>';
                }
                $html .= '<tr style="background: yellow">
                                    <td colspan="4" class="text-center"> <strong>Total Transaction Amounts</strong></td>';
                $html .= '<td><strong>';
                if ($grandTotalPendingAmount->isEmpty()) {
                    $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                } else {
                    $html .= '<b style = "color:blue" >' . $grandTotalPendingAmount->sum('total_cost') . '</b >';
                }
                $html .= '</strong></td><td><strong>';
                if ($grandTotalPendingAmount->isEmpty()) {
                    $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                } else {
                    $html .= '<b style = "color:#666633" >' . $grandTotalPendingAmount->sum('total_item_quantities') . '</b >';
                }
                $html .= '</strong></td>';
                $html .= '<td><strong>';
                if ($checkcurrnetMonth->isEmpty()) {
                    $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                } else {
                    $html .= '<b style = "color:blue" >' . $checkcurrnetMonth->sum('total_cost') . '</b >';
                }
                $html .= '</strong></td><td><strong>';
                if ($checkcurrnetMonth->isEmpty()) {
                    $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                } else {
                    $html .= '<b style = "color:#666633" >' . $checkcurrnetMonth->sum('total_item_quantities') . '</b >';
                }
                $html .= '</strong></td>';
                $html .= '<td><strong>';
                if ($checkLastMonth->isEmpty()) {
                    $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                } else {
                    $html .= '<b style = "color:blue" >' . $checkLastMonth->sum('total_cost') . '</b >';
                }
                $html .= '</strong></td><td><strong>';
                if ($checkLastMonth->isEmpty()) {
                    $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                } else {
                    $html .= '<b style = "color:#666633" >' . $checkLastMonth->sum('total_item_quantities') . '</b >';
                }
                $html .= '</strong></td>';
                $html .= '<td><strong>';
                if ($checkLastTwoMonth->isEmpty()) {
                    $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                } else {
                    $html .= '<b style = "color:blue" >' . $checkLastTwoMonth->sum('total_cost') . '</b >';
                }
                $html .= '</strong></td><td><strong>';
                if ($checkLastTwoMonth->isEmpty()) {
                    $html .= '<i class="fa fa-times" aria-hidden="true" style="color:red"></i>';
                } else {
                    $html .= '<b style = "color:#666633" >' . $checkLastTwoMonth->sum('total_item_quantities') . '</b >';
                }
                $html .= '</strong></td>';
                $html .= '</tr>';
            }
        }
        $html.='</tbody>
              </table>               
              </div>
            </div>
        </div>';
        return $html;

    }

    public static function resizeImage(Request $request){

        $image = $request->file('image');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images/thumbnail_image');
        $img = Image::make($image->getRealPath());
        $img->resize(50, 50, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('png', 75)->save($destinationPath.'/'.$input['imagename']);
    }

    public static function corporateOrder($id, $quantity, $discountPercent,$perbox=null){
        $product = Product::findOrFail($id);
        $discount = $discountPercent;

        $discountPercent = $discount;
        $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
        if(isset($perbox) && $perbox != null){
            $totalOrderQuantity = $perbox * $quantity;
        }
        else{
            $totalOrderQuantity = $product->product_per_box * $quantity;
        }
        $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
        $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
        $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
        $totalTax = ($taxRate * $totalTaxablePrice) / 100;
        $totalProductCost = $totalTaxablePrice + $totalTax;
        $weight = $quantity * $product->product_weight;
        $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
        $data = [
            'total_mrp' => $totalOrderQuantity * $product->mrp,
            'taxable_amount' => $totalTaxablePrice,
            'discount_percent' => $discountPercent,
            'total_discount' => $totalDiscount,
            'total_tax' => $totalTax,
            'total_item_cost' => $totalProductCost,
            'weight' => $weight,
            'volume' => $volume,
            'total_item_quantities' => $totalOrderQuantity,
            'order_quantity' => $quantity
        ];
        return $data;
    }

    public static function dealerDiscount($id, $quantity, $distributor_id,$perbox=null)
    {


        $product = Product::findOrFail($id);
        $discount = DealerCoupon::where('status', 1)->get();
        $distributor = Distributor::find($distributor_id);

//            $discount->map(function ($q) {
//              //  $q->product_ids = explode(',', $q->product_ids);
//                $q->category_ids = explode(',', $q->category_ids);
//                $q->sub_category_ids = explode(',', $q->sub_category_ids);
//                $q->child_category_ids = explode(',', $q->child_category_ids);
//                $q->distributor_ids = explode(',', $q->distributor_ids);
//                $q->state_ids = explode(',', $q->state_ids);
//                $q->zone_ids = explode(',', $q->zone_ids);
//            });


        // $taxablePrice = $product->mrp;
        //  $discountPercent = 0;

        $productDiscounts = $discount->pluck('product_ids', 'id');
        $childCategoryDiscounts = $discount->pluck('child_category_ids', 'id');
        $subCategoryDiscounts = $discount->pluck('sub_category_ids', 'id');
        //  $categoryDiscounts = $discount->pluck('category_ids', 'id');
        $distributorDetails=$discount->pluck('distributor_ids','id');
        //  $stateDiscounts = $discount->pluck('state_ids', 'id');

        /*--------START  PRODUCT WISE DISCOUNT----------------------*/
        foreach ($distributorDetails as $disk => $disv){
            $distridetails=array_filter(explode(',', $disv));
            if (!empty($distridetails)) {
                if (in_array($distributor_id, $distridetails)) {
                    $coupon = DealerCoupon::find($disk);

                    $productDiscountsCorp = array_filter(explode(',', $coupon->product_ids));
                    $childCategoryDiscountsCorp = array_filter(explode(',', $coupon->child_category_ids));
                    $subCategoryDiscountsCorp = array_filter(explode(',', $coupon->sub_category_ids));

                    if (!empty($productDiscountsCorp)) {
                        if (in_array($product->id, $productDiscountsCorp)) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }

                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }

                        }
                    }
                    elseif (!empty($childCategoryDiscountsCorp)) {
                        if (in_array($product->child_category_id, $childCategoryDiscountsCorp)) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    }
                    elseif (!empty($subCategoryDiscountsCorp)) {
                        if (in_array($product->subcategory_id, $subCategoryDiscountsCorp)) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }

                        }
                    }

                }
            }

        }

        foreach ($productDiscounts as $pk => $pv) {

            $productDetails = array_filter(explode(',', $pv));

            if (!empty($productDetails)) {
                if (in_array($product->id, $productDetails)) {

                    $coupon = DealerCoupon::find($pk);
                    $stateIds = explode(',', $coupon->state_ids);
                    $checkDistributor = array_filter(explode(',', $coupon->distributor_ids));

                    if (!empty($checkDistributor) && in_array($distributor->id, $checkDistributor)) {

                        if (in_array($distributor->state_id, $stateIds) || empty(array_filter($stateIds))) {

                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    } else {

                        if (in_array($distributor->state_id, $stateIds) || empty(array_filter($stateIds))) {

                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    }

                }
            }
        }

        /*--------END  PRODUCT WISE DISCOUNT----------------------*/


        /*--------START  CHILD CATEGORY WISE DISCOUNT----------------------*/
        foreach ($childCategoryDiscounts as $pk => $pv) {
            $productDetails = array_filter(explode(',', $pv));
            if (!empty($productDetails)) {
                $coupon = DealerCoupon::find($pk);
                if (in_array($product->child_category_id, $productDetails) && $coupon->product_ids=='') {
                    $stateIds = explode(',', $coupon->state_ids);
                    $checkDistributor = array_filter(explode(',', $coupon->distributor_ids));
                    if (!empty($checkDistributor) && in_array($distributor->id, $checkDistributor)) {
                        if (in_array($distributor->state_id, $stateIds)  || empty(array_filter($stateIds))) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    } else {
                        if (in_array($distributor->state_id, $stateIds) || empty(array_filter($stateIds))) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    }

                }
            }
        }
        /*--------END  CHILD CATEGORY WISE DISCOUNT----------------------*/


        /*--------START  SUB CATEGORY WISE DISCOUNT----------------------*/
        foreach ($subCategoryDiscounts as $pk => $pv) {
            $productDetails = array_filter(explode(',', $pv));
            if (!empty($productDetails)) {
                $coupon = DealerCoupon::find($pk);
                if (in_array($product->subcategory_id, $productDetails) && $coupon->product_ids=='' && $coupon->child_category_ids=='') {
                    $stateIds = explode(',', $coupon->state_ids);
                    $checkDistributor = array_filter(explode(',', $coupon->distributor_ids));
                    if (!empty($checkDistributor) && in_array($distributor->id, $checkDistributor)) {
                        if (in_array($distributor->state_id, $stateIds)  || empty(array_filter($stateIds))) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    } else {
                        if (in_array($distributor->state_id, $stateIds) || empty(array_filter($stateIds))) {
                            if ($coupon->discount_type == 'percentage') {
                                $discountPercent = $coupon->discount;
                                $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            } else {
                                $taxablePrice = $product->mrp - $coupon->discount;
                                $discountPercent = ($coupon->discount / $product->mrp) * 100;
                                if(isset($perbox) && $perbox != null){
                                    $totalOrderQuantity = $perbox * $quantity;
                                }
                                else{
                                    $totalOrderQuantity = $product->product_per_box * $quantity;
                                }
                                $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
                                $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
                                $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
                                $totalTax = ($taxRate * $totalTaxablePrice) / 100;
                                $totalProductCost = $totalTaxablePrice + $totalTax;
                                $weight = $quantity * $product->product_weight;
                                $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
                                $data = [
                                    'total_mrp' => $totalOrderQuantity * $product->mrp,
                                    'taxable_amount' => $totalTaxablePrice,
                                    'discount_percent' => $discountPercent,
                                    'total_discount' => $totalDiscount,
                                    'total_tax' => $totalTax,
                                    'total_item_cost' => $totalProductCost,
                                    'weight' => $weight,
                                    'volume' => $volume,
                                    'total_item_quantities' => $totalOrderQuantity,
                                    'order_quantity' => $quantity
                                ];
                                return $data;
                            }
                        }
                    }

                }
            }
        }
        /*--------END  SUB CATEGORY WISE DISCOUNT----------------------*/


        /*--------START  CATEGORY WISE DISCOUNT----------------------*/
//        foreach ($categoryDiscounts as $pk => $pv) {
//            $productDetails = array_filter(explode(',', $pv));
//            if (!empty($productDetails)) {
//                if (in_array($product->category_id, $productDetails)) {
//                    $coupon = Coupon::find($pk);
//                    $stateIds=explode(',',$coupon->state_ids);
//                    if(in_array($distributor->state_id,$stateIds)) {
//                        if ($coupon->discount_type == 'percentage') {
//                            $discountPercent = $coupon->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                            $totalOrderQuantity = $product->product_per_box * $quantity;
//                            $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
//                            $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
//                            $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
//                            $totalTax = ($taxRate * $totalTaxablePrice) / 100;
//                            $totalProductCost = $totalTaxablePrice + $totalTax;
//                            $weight = $quantity * $product->product_weight;
//                            $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
//                            $data = [
//                                'total_mrp' => $totalOrderQuantity * $product->mrp,
//                                'taxable_amount' => $totalTaxablePrice,
//                                'discount_percent' => $discountPercent,
//                                'total_discount' => $totalDiscount,
//                                'total_tax' => $totalTax,
//                                'total_item_cost' => $totalProductCost,
//                                'weight' => $weight,
//                                'volume' => $volume,
//                                'total_item_quantities' => $totalOrderQuantity,
//                                'order_quantity' => $quantity
//                            ];
//                            return $data;
//                        } else {
//                            $taxablePrice = $product->mrp - $coupon->discount;
//                            $discountPercent = ($coupon->discount / $product->mrp) * 100;
//                            $totalOrderQuantity = $product->product_per_box * $quantity;
//                            $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
//                            $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
//                            $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
//                            $totalTax = ($taxRate * $totalTaxablePrice) / 100;
//                            $totalProductCost = $totalTaxablePrice + $totalTax;
//                            $weight = $quantity * $product->product_weight;
//                            $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
//                            $data = [
//                                'total_mrp' => $totalOrderQuantity * $product->mrp,
//                                'taxable_amount' => $totalTaxablePrice,
//                                'discount_percent' => $discountPercent,
//                                'total_discount' => $totalDiscount,
//                                'total_tax' => $totalTax,
//                                'total_item_cost' => $totalProductCost,
//                                'weight' => $weight,
//                                'volume' => $volume,
//                                'total_item_quantities' => $totalOrderQuantity,
//                                'order_quantity' => $quantity
//                            ];
//                            return $data;
//                        }
//                    }
//                }
//            }
//        }
        /*--------END  CATEGORY WISE DISCOUNT----------------------*/

//
//
//
//        if (isset($discount) && $discount->isNotEmpty()) {
//            foreach ($discount as $dk => $dv) {
//
//                if ($dv->distributor_ids && !empty(array_filter($dv->distributor_ids)) && $dv->product_ids && !empty(array_filter($dv->product_ids))) {
//                    if ((in_array($product->id, $dv->product_ids) || in_array('all', $dv->product_ids)) && in_array($distributor_id, $dv->distributor_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->distributor_ids && !empty(array_filter($dv->distributor_ids)) && $dv->child_category_ids && !empty(array_filter($dv->child_category_ids)) && empty(array_filter($dv->product_ids))) {
//                    if ((in_array($product->child_category_id, $dv->child_category_ids) || in_array('all', $dv->child_category_ids)) && in_array($distributor_id, $dv->distributor_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->distributor_ids && !empty(array_filter($dv->distributor_ids)) && $dv->sub_category_ids && !empty(array_filter($dv->sub_category_ids)) && empty(array_filter($dv->product_ids)) && empty(array_filter($dv->child_category_ids))) {
//                    if ((in_array($product->subcategory_id, $dv->sub_category_ids) || in_array('all', $dv->sub_category_ids)) && in_array($distributor_id, $dv->distributor_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->distributor_ids && !empty(array_filter($dv->distributor_ids)) && $dv->category_ids && !empty(array_filter($dv->category_ids)) && empty(array_filter($dv->product_ids)) && empty(array_filter($dv->child_category_ids)) && empty(array_filter($dv->sub_category_ids))) {
//                    if ((in_array($product->category_id, $dv->category_ids) || in_array('all', $dv->category_ids)) && in_array($distributor_id, $dv->distributor_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->product_ids && !empty(array_filter($dv->product_ids)) && empty(array_filter($dv->distributor_ids))) {
//                    if (in_array($product->id, $dv->product_ids) || in_array('all', $dv->product_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->child_category_ids && !empty(array_filter($dv->child_category_ids)) && empty(array_filter($dv->distributor_ids)) && empty(array_filter($dv->product_ids))) {
//                    if (in_array($product->child_category_id, $dv->child_category_ids) || in_array('all', $dv->child_category_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->sub_category_ids && !empty(array_filter($dv->sub_category_ids)) && empty(array_filter($dv->distributor_ids)) && empty(array_filter($dv->product_ids)) && empty(array_filter($dv->child_category_ids))) {
//                    if (in_array($product->subcategory_id, $dv->sub_category_ids) || in_array('all', $dv->sub_category_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } elseif ($dv->category_ids && !empty(array_filter($dv->category_ids)) && empty(array_filter($dv->distributor_ids)) && empty(array_filter($dv->product_ids)) && empty(array_filter($dv->child_category_ids)) && empty(array_filter($dv->sub_category_ids))) {
//                    if (in_array($product->category_id, $dv->category_ids) || in_array('all', $dv->category_ids)) {
//                        if ($dv->discount_type == 'percentage') {
//                            $discountPercent = $dv->discount;
//                            $taxablePrice = $product->mrp - ($product->mrp * ($discountPercent / 100));
//                        } else {
//                            $taxablePrice = $product->mrp - $dv->discount;
//                            $discountPercent = ($dv->discount / $product->mrp) * 100;
//                        }
//                        break;
//                    }
//                } else {
//                    $taxablePrice = $product->mrp;
//                    $discountPercent = 0;
//                }
//
//            }
//        } else {
//            $taxablePrice = $product->mrp;
//            $discountPercent = 0;
//        }

        $taxablePrice = $product->mrp;
        $discountPercent = 0;
        if(isset($perbox) && $perbox != null){
            $totalOrderQuantity = $perbox * $quantity;
        }
        else{
            $totalOrderQuantity = $product->product_per_box * $quantity;
        }
        $taxRate = isset($product->hsnCode->gst->total_gst) ? $product->hsnCode->gst->total_gst : 1;
        $totalDiscount = ($totalOrderQuantity * $product->mrp) - ($taxablePrice * $totalOrderQuantity);
        $totalTaxablePrice = $taxablePrice * $totalOrderQuantity;
        $totalTax = ($taxRate * $totalTaxablePrice) / 100;
        $totalProductCost = $totalTaxablePrice + $totalTax;
        $weight = $quantity * $product->product_weight;
        $volume = $totalOrderQuantity * $product->product_volume * 35.3146667;
        $data = [
            'total_mrp' => $totalOrderQuantity * $product->mrp,
            'taxable_amount' => $totalTaxablePrice,
            'discount_percent' => $discountPercent,
            'total_discount' => $totalDiscount,
            'total_tax' => $totalTax,
            'total_item_cost' => $totalProductCost,
            'weight' => $weight,
            'volume' => $volume,
            'total_item_quantities' => $totalOrderQuantity,
            'order_quantity' => $quantity
        ];
        return $data;
    }


    public static  function is_url($uri){
        if(preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$uri)){
            return true;
        }
        else{
            return false;
        }
    }


    

}