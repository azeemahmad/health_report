<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Auth;

class SendSMS 
{
    
    public static function send_sms($orderIds,$distributorNumber,$distributorName,$totalBoxes,$totalQuantity,$transportData)
    {   
        $msg = "";       
        $transportedBy = $transportData['transported_by'];
        $LrNo = $transportData['lr_no'];
        $date =  date('d-m-Y', strtotime($transportData['date']));
        $mvNo = $transportData['mv_no'];
        $driverName = $transportData['driver_name'];
        $driverMobile = $transportData['mobile'];     

        $msg = "Dear ". $distributorName.",%0aYour purchases bearing the following order ID's has been dispatched from our facility.";
        
        foreach($orderIds as $id)
        {
            $msg .=  "%0aORDER- " . str_pad($id ,6,'0',STR_PAD_LEFT);
        }
        
        $msg .= "%0a%0aTotal Boxes (S Form):" . $totalBoxes . "%0aTotal Part Qty (P Form):" . $totalQuantity .  "%0a%0aTruck Details:%0aTransported By :" . $transportedBy . "%0aLR No & Date :" . $LrNo . "/" . $date . "%0aM.V. No. :" . $mvNo . "%0aDriver Name :" . $driverName . "%0aMobile:" . $driverMobile . "%0a-Ajanta LLP| ORPAT Group.";

        $curl = curl_init();       

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"sender\": \"ORPATG\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \"  $msg  \", \"to\": [ \"$distributorNumber\"] } ] }",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => array(
                "authkey: 299535ABPW0525L735da95471",
                "content-type: application/json"
             ),
            ));   
            
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) 
            { 
               echo "cURL Error #:" . $err;
            } 
            else 
            { 
                echo $response;
            }
    } 

    public static function dailyRevenueSendSMS($number, $link1, $link2, $date)
    {   
        try
        {
            $msg = "";      
            $msg = "Hi, %0aPlease Find Below Daily Reports. %0a Date : " . $date;
            
            $msg .= "%0a1) Division Wise Revenue : %0a" . $link2 . "%0a2) Category Wise Revenue: %0a" . $link1 . "%0a%0a-Ajanta LLP| ORPAT Group.";   
        
            $curl = curl_init();  

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"sender\": \"ORPATG\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \"  $msg  \", \"to\": [ \"$number\"] } ] }",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => array(
                "authkey: 299535ABPW0525L735da95471",
                "content-type: application/json"
             ),
            ));   
            
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) 
            { 
               echo "cURL Error #:" . $err;
            } 
            else 
            { 
                echo $response;
            } 
        }
        catch(\Exception $e)
        {
             dd($e);
        }      
    }
    


    public static function send_otp_for_report_user_login($number,$otp)
    {   
        try
        {
            $msg = "";      
            $msg = "Hi, %0a Your report admin login verification code is ". $otp;
                    
            $curl = curl_init();  

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{ \"sender\": \"ORPATG\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \"  $msg  \", \"to\": [ \"$number\"] } ] }",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => array(
                "authkey: 299535ABPW0525L735da95471",
                "content-type: application/json"
             ),
            ));   
            
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            $result = json_decode($response, true);

            if ($result ['type']=='success') 
            { 
               return 1;
            } 
            else 
            { 
                return 0;

            } 
        }
        catch(\Exception $e)
        {
             dd($e);
        }      
    }
  //gets the data from a URL  
    public static function get_tiny_url($url)  {  
        $ch = curl_init();  
        $timeout = 5;  
        curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);  
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);  
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);  
        $data = curl_exec($ch);  
        curl_close($ch);  
        return $data;  
    }
}