<?php

namespace App\Helpers;
use App\Models\DistributorSaleToOrpatTransatiction;
use Str;
use Storage;
use App\InvoiceOrderList;
use App\Models\PForm\InvoicePFormOrder;
use App\DistributorCreditNote;
use App\Distributor;
use App\Models\SFormOrder;
use App\Models\PForm\PFormOrder;
use App\BankUpload;


class ImageS3Upload {
    /**
     * @param int $user_id User-id
     * 
     * @return array
     */
    public static function distributorBalanceAmount($id){
       try{
           
            $accountBalance=BankUpload::where('distributor_id',$id)->where('is_verified',1)->sum('amount');
            $sformOrder=InvoiceOrderList::where('distributor_id',$id)->where('invoice_status',1)->sum('grand_total');
            $pformOrder=InvoicePFormOrder::where('created_by',$id)->where('invoice_status',1)->sum('grand_total');
            $creditNote=DistributorCreditNote::where('distributor_id',$id)->where('type',0)->sum('cn_amount');
            $debitNote=DistributorCreditNote::where('distributor_id',$id)->where('type',1)->sum('cn_amount');
            $saleToOrpatTransatiction = DistributorSaleToOrpatTransatiction::where('distributor_id',$id)->sum('grand_total');

           $holdAmountsform=SFormOrder::where('created_by',$id)->whereIn('order_status',[2,3,4])->sum('grand_total');
           $holdAmountPForm=PFormOrder::where('created_by',$id)->whereIn('order_status',[2,3,4,5])->sum('grand_total');
           $totalHoldAmount = $holdAmountsform + $holdAmountPForm;

           $balanceAmount = ($saleToOrpatTransatiction+$accountBalance+$creditNote) - ($pformOrder+$sformOrder+$debitNote+$totalHoldAmount);
            
            return DiscountDetails::IndianRupees($balanceAmount);
       }
        catch(\Exception $e){
          return 0;
        }


    }
    public static function distributorHoldAmount($id){
        try{


            $holdAmountsform=SFormOrder::where('created_by',$id)->whereIn('order_status',[2,3,4])->sum('grand_total');
            $holdAmountPForm=PFormOrder::where('created_by',$id)->whereIn('order_status',[2,3,4,5])->sum('grand_total');
            $totalHoldAmount = $holdAmountsform + $holdAmountPForm;
            return DiscountDetails::IndianRupees($totalHoldAmount);
        }
        catch(\Exception $e){
            return 0;
        }


    }
    public static function image_upload_s3($file,$path) {
        
        
      $filename =  Str::random().now(). '.' .$file->getClientOriginalExtension();    
      $check_upload =  Storage::disk('s3')->put($path.$filename, file_get_contents($file, 'public'), 'public');  

      if( $check_upload ==true){
        $file->move(public_path($path), $filename);
      }else{
          return redirect()->back()->with('error_message','No connection Please check your internet connectivity and try again...');
      }

        return $filename;
    }

    public static function convertToIndianCurrency($number)
    {
        try{
        $no = round($number);

        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(
            0 => '',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen',
            20 => 'Twenty',
            30 => 'Thirty',
            40 => 'Forty',
            50 => 'Fifty',
            60 => 'Sixty',
            70 => 'Seventy',
            80 => 'Eighty',
            90 => 'Ninety',
            100=>"",
            );
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_length) {
            $divider = ($i == 2) ? 10 : 100;
            //dd($divider);
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural;
            } else {
                $str [] = null;
            }
        }

        //dd($words[$decimal - $decimal % 10]);
        $Rupees = implode(' ', array_reverse($str));
        $paise = ($decimal) ? "And ".($words[$decimal - $decimal % 10]) . " " . ($words[$decimal % 10])." Paise " : '';
        return ($Rupees ? 'Indian Rupees ' . $Rupees : '') . $paise . "Only";
        }catch(Exception $e){
            dd($e);
        }
    }

    public static function IndianRupees($number){
        $decimal = (string)($number - floor($number));
        $money = floor($number);
        $length = strlen($money);
        $delimiter = '';
        $money = strrev($money);

        for($i=0;$i<$length;$i++){
            if(( $i==3 || ($i>3 && ($i-1)%2==0) )&& $i!=$length){
                $delimiter .=',';
            }
            $delimiter .=$money[$i];
        }

        $result = strrev($delimiter);

        $decimal = preg_replace("/0\./i", ".", $decimal);
        $decimal = substr($decimal, 0, 3);

        if ($decimal != '0') {
            $result = $result . $decimal;
        }
        else{
            $result =$result.'.'.str_pad($decimal,2,'0',STR_PAD_LEFT);
        }
        return $result;

    }

    public static function varify_gst($gst_number){

       

            $data = array("gstNo" => $gst_number, "key_secret" => env('GST_APPLEFLOW_KEY', 'LH3Zlnh5OPYh9nzxQZBBSRqX2u82'));
            $data_string = json_encode($data);
            $ch = curl_init('https://appyflow.in/api/verifyGST');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            $result = curl_exec($ch);
            if ($result != false && $result != null) {
                $result_arr = json_decode($result, TRUE);
               if(isset($result_arr['error'])){
                return $result_arr['error'] ? false : true;
               }else{
                return true;
               }
              
            }
           return $result;
       
    }
}