<?php

namespace App\Helpers;
use App\Category;
use App\Product;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use App\Distributor;
use App\Models\ReportData;

class ReportsDataHelper
{
    public static function report_data($distributor,$product,$productDetails,$flag){

        $report_data=[];    
        $last_inserted_data                        = ReportData::where('is_part',0)->where('product_id',$product->id)->orderBy('date', 'DESC')->latest()->first();
        $closing_quantity                          = isset($last_inserted_data) ? $last_inserted_data->closing_quantity : 0 ;
        $closing_quantity                          -= $productDetails['total_item_quantities'];

        if($flag=='sform'){

             $report_data =  ReportData::firstOrNew([
                'date' => date('Y-m-d'),
                'product_id'=> $product->id,
                'is_part'=>0
            ]);

            $report_data->volume                   = $report_data->volume +$productDetails['volume'];  
            $report_data->weight                   = $report_data->weight +$productDetails['weight'];  
            $report_data->closing_quantity         = $closing_quantity ;  
            $report_data->sub_category_id          = $product->subcategory_id; 
            $report_data->mrp                      = $productDetails['unit_cost_per_quantity']; 
            
        	
        }else{
            $report_data =  ReportData::firstOrNew([
                'date' => date('Y-m-d'),
                'part_id'=> $product->id,
                'is_part'=>1,
            ]);

            $report_data->production_quantity      = $report_data->production_quantity + $productDetails['total_item_quantities'] ;  
            $report_data->product_id               = $product->product_id; 
            $report_data->closing_quantity         = 0;  
            $report_data->sub_category_id          = $product->sub_category_id; 
            $report_data->mrp                      = $productDetails['unit_cost_per_quantity']; 


        }
       


        $report_data->category_id                  = $product->category_id; 
        $report_data->child_category_id            = $product->child_category_id; 

        if($distributor->state_id==12){
            $report_data->state_quantity           = $report_data->state_quantity + $productDetails['total_item_quantities'];
            $report_data->state_taxable_value      = $report_data->state_taxable_value + $productDetails['taxable_amount'];  
            $report_data->state_tax                = $report_data->state_tax + $productDetails['tax'];   
        }else{
            $report_data->integrated_quantity      = $report_data->integrated_quantity + $productDetails['total_item_quantities']; 
            $report_data->integrated_taxable_value = $report_data->integrated_taxable_value + $productDetails['taxable_amount'];  
            $report_data->integrated_tax           = $report_data->integrated_tax + $productDetails['tax'];  

        }

        $report_data->total_quantity =  $report_data->total_quantity + $productDetails['total_item_quantities'];
        $report_data->total_taxable_amount =  $report_data->total_taxable_amount + $productDetails['taxable_amount'];
        $report_data->total_tax =  $report_data->total_tax + $productDetails['tax'];

        $report_data->total_cost                   = $report_data->total_cost + $productDetails['total_cost'];  
        $report_data->discount_percent             = $productDetails['discount_percent'];  
        $report_data->discount                     = $report_data->discount + $productDetails['discount'];  

      
        $report_data->save();
        return 1;
    }

    
    public static function export_and_nepal_report_data($product,$productDetails, $date,$flag){
        
        $report_data =  ReportData::firstOrNew([
            'date' => $date,
            'product_id'=> $product->id,
            'is_part'=>0
        ]);

        if($flag=='export'){
            $report_data->export_taxable_value          = $report_data->export_taxable_value + $productDetails['taxable_amount'];  
            $report_data->export_quantity               = $report_data->export_quantity + $productDetails['total_product_quantity'];  
        }else{
            $report_data->nepal_taxable_value           = $report_data->nepal_taxable_value + $productDetails['taxable_amount'];  
            $report_data->nepal_quantity                = $report_data->nepal_quantity + $productDetails['total_product_quantity'];     
        }


        $report_data->total_taxable_amount          = $report_data->total_taxable_amount + $productDetails['taxable_amount'];  
        $report_data->total_quantity                = $report_data->total_quantity + $productDetails['total_product_quantity'];  
        $report_data->total_cost                    = $report_data->total_cost + $productDetails['taxable_amount'];

        $report_data->category_id                   = $product->category_id; 
        $report_data->sub_category_id               = $product->subcategory_id; 
        $report_data->child_category_id             = $product->child_category_id; 
        $report_data->mrp                           = $product->mrp; 
        $report_data->save();

        $closing_quantity =0;
        $products =  ReportData::where('is_part',0)->orderBy('date')->where('product_id',$product->id)->get();

        foreach($products as $key => $update_products){
         $closing_quantity += $update_products->production_quantity;
         $closing_quantity += $update_products->sales_return_quantity;
         $closing_quantity -= $update_products->integrated_quantity;
         $closing_quantity -= $update_products->online_quantity;
         $closing_quantity -= $update_products->state_quantity;
         $closing_quantity -= $update_products->export_quantity;
         $closing_quantity -= $update_products->nepal_quantity;
        
         $update_products->update(['closing_quantity'=>$closing_quantity]);

        }

 
        return 1;

    }


    public static function update_export_and_nepal_report_data($product,$newProductDetails,$orderDetials,$invoice_date,$flag){
        $date =$invoice_date;
        if($newProductDetails['total_product_quantity'] != $orderDetials->total_product_quantity){

            $diff_quantity = abs($newProductDetails['total_product_quantity'] - $orderDetials->total_product_quantity); // FIND DIFFRENCE
            $diff_taxable_value = abs($newProductDetails['taxable_amount'] - $orderDetials->taxable_amount); // FIND DIFFRENCE
  
            $report_data =  ReportData::firstOrNew([
                'date' =>  $date ,
                'product_id'=> $product->id,
                'is_part'=>0
            ]);
           
            if($flag=='export'){

                if($newProductDetails['total_product_quantity'] > $orderDetials->total_product_quantity) {

                    $report_data->export_taxable_value      = $report_data->export_taxable_value + $diff_taxable_value ;  
                    $report_data->export_quantity           = $report_data->export_quantity + $diff_quantity;  
                    $report_data->total_taxable_amount      = $report_data->total_taxable_amount + $diff_quantity;  
                    $report_data->total_quantity            = $report_data->total_quantity + $diff_quantity;  
                    $report_data->total_cost                = $report_data->total_cost + $diff_taxable_value;  
        
                }else{

                    $report_data->export_taxable_value      = $report_data->export_taxable_value - $diff_taxable_value ;  
                    $report_data->export_quantity           = $report_data->export_quantity - $diff_quantity;  
                    $report_data->total_taxable_amount      = $report_data->total_taxable_amount - $diff_quantity;  
                    $report_data->total_quantity            = $report_data->total_quantity - $diff_quantity;  
                    $report_data->total_cost                = $report_data->total_cost - $diff_taxable_value;  

                }

            
            }else{
      

                if($newProductDetails['total_product_quantity'] > $orderDetials->total_product_quantity) {
                    $report_data->nepal_taxable_value      = $report_data->nepal_taxable_value  + $diff_taxable_value ;    
                    $report_data->nepal_quantity           = $report_data->nepal_quantity + $diff_quantity;  
                    $report_data->total_cost               = $report_data->total_cost + $diff_taxable_value;  
                    $report_data->total_taxable_amount     = $report_data->total_taxable_amount + $diff_quantity;  
                    $report_data->total_quantity           = $report_data->total_quantity + $diff_quantity;  

                }else{
                    $report_data->nepal_taxable_value      = $report_data->nepal_taxable_value - $diff_taxable_value ;    
                    $report_data->nepal_quantity           = $report_data->nepal_quantity  - $diff_quantity; 
                    $report_data->total_cost               = $report_data->total_cost -  $diff_taxable_value;  
                    $report_data->total_taxable_amount     = $report_data->total_taxable_amount - $diff_quantity;  
                    $report_data->total_quantity           = $report_data->total_quantity - $diff_quantity;  
                    
                }

            }

            $report_data->category_id                      = $product->category_id; 
            $report_data->sub_category_id                  = $product->subcategory_id; 
            $report_data->child_category_id                = $product->child_category_id; 
            $report_data->mrp                              = $product->mrp;           
            $report_data->save();

            $closing_quantity =0;
            $products =  ReportData::where('is_part',0)->orderBy('date')->where('product_id',$product->id)->get();
    
            foreach($products as $key => $update_products){
             $closing_quantity += $update_products->production_quantity;
             $closing_quantity += $update_products->sales_return_quantity;
             $closing_quantity -= $update_products->integrated_quantity;
             $closing_quantity -= $update_products->online_quantity;
             $closing_quantity -= $update_products->state_quantity;
             $closing_quantity -= $update_products->export_quantity;
             $closing_quantity -= $update_products->nepal_quantity;
            
             $update_products->update(['closing_quantity'=>$closing_quantity]);
    
            }

        }
        return 1;

    }

}