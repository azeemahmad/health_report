public function importExcel(Request $request){
if ($request->hasFile('import_file')) {
$data=Excel::toArray(new ProductImport, $request->file('import_file'));
if (!empty($data)) {
foreach ($data as $key => $value) {
foreach ($value as $vk => $vv) {

if ($vk != 0) {

try {

$retailer['company'] = $vv[0];
$retailer['retailer_name'] = $vv[1];
$retailer['email'] = $vv[2];
$retailer['mobile'] = $vv[3];
$retailer['whatsapp_number'] = $vv[4];
$retailer['landline_number'] = $vv[5];
$retailer['country_id'] = 101;
$retailer['state_id'] = $vv[7];
$retailer['city_id'] = $vv[8];
$retailer['postal_code'] = $vv[9];
$retailer['address_line1'] = $vv[10];
$retailer['address_line2'] = $vv[11];
$retailer['address_line3'] = $vv[12];
$retailer['vat_number'] = $vv[13];
$retailer['gst_number'] = $vv[14];
$retailer['pan_number'] = $vv[15];
$retailer['aadhar_number'] = $vv[16];
$retailer['date_of_joining'] = $vv[17];
$retailer['date_of_birth'] = $vv[18];
$retailer['wedding_anniversary'] = $vv[19];
$retailer['created_by'] = $vv[20];
$retailer['user_type'] = $vv[21];
$retailer['sales_person_id'] = $vv[22];
$retailer['distributor_id'] = 12;
$retailer['credit_period'] = $vv[24];
$retailer['status'] = $vv[25];
$retailer['approved_by'] = $vv[26];
$retailer['approved_date'] = $vv[27];
$retailer['image'] = $vv[28];
$retailer['shop_banner'] = $vv[29];
$retailer['area_id'] = $vv[30];
$retailer['retailer_sequence'] = $vv[31];
$retailer['retailer_sequence_distributor'] = $vv[32];

Retailer::create($retailer);

} catch (\Illuminate\Database\QueryException $ex) {
dd($ex->getMessage());
}

}
}
}
return redirect()->back()->with('success', 'Insert Record successfully.');
}

}
return back()->with('error', 'Please Check your file, Something is wrong there.');

}
