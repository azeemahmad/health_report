<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
       '/distributor/ajax/changeSformRate',
        '/distributor/ajax/changeTotalAmount',
       '/distributor/ajax/changePformRate',
        '/distributor/ajax/changeTotalPAmount',
       '/distributor/verifyOrder',
        '/distributor/addNewProd',
     '/distributor/ajax/editsformDetails',
        '/distributor/ajax/changeSalesOrderRate',
        '/distributor/ajax/changeSalesOrderTotalAmount',
        '/distributor/ajax/editSalesOrderDetails',
        '/distributor/ajax/editPformOrderDetails',
        'sales/ajax/changeSalesOrderRate',
        'sales/ajax/editSalesOrderDetails',
        'sales/ajax/changeSalesOrderTotalAmount',
        '/distributor/createOrder',
        '/distributor/fetchOrder',
        'distributor/sales-order/accept-order',
        'distributor/sales-order/reject-order',
        '/distributor/deleteRow', '/distributor/deleteOrderList',
        '/distributor/fetchCreatedOrder',
        '/distributor/sales-order/retrivePaymentInfo',
        '/distributor/sales-order/updatePayment',
        '/distributor/qrgetsubChildCategoryId',

        '/admin/dispatch/approveDispatch',
        '/admin/distributorArea',
        '/admin/ajax/changeSformRate',
        '/admin/ajax/changeTotalAmount',
        '/admin/ajax/changePformRate',
        '/admin/ajax/changeTotalPAmount',
        '/admin/ajax/editsformDetails',
        '/admin/ajax/changeSalesOrderRate',
        '/admin/ajax/changeSalesOrderTotalAmount',
        '/admin/ajax/editSalesOrderDetails',
        '/admin/ajax/editPformOrderDetails',
        'admin/ajax/changeSalesOrderRate',
        'admin/ajax/editSalesOrderDetails',
        'admin/ajax/changeSalesOrderTotalAmount',
        'admin/ajax/coupon_type_onchange',
        'admin/ajax/coupon_retailer_type_onchange',
        'admin/ajax/fetch_product',
        'admin/ajax/fetch_product_retailer',
        'admin/ajax/product_ids_update',
        'admin/ajax/product_ids_update_retailer',
        'admin/inwardInsert',
        'admin/verify-scanned-barcodes',
        'distributor/fetchReport',
        'admin/getstatelist',
        'admin/getpincodes',
        'admin/getretailers',
        'admin/pincodes',
        'distributor/pincodes',
        'sales/pincodes',
        'admin/getcity',
        '/distributor/ajax/expensecategory_on_change',
        '/distributor/ajax/editRformOrderDetails',
        '/distributor/ajax/getGstOrderReport',
        '/admin/verify-gst',
        '/admin/update_print_status',
        '/admin/getProductChildCategoryId',
        'admin/ajax/editRformOrderDetails',
        'admin/getopeningstockdetails',
        'admin/finishedgoodslist',
        'admin/productlistforsalesreturn',
        'admin/settle-creditnote',
        'admin/settle-credit',
        'admin/settle-debit',
        'admin/getInvoices',
        'admin/getInvoicesProducts',
        'admin/getpartwisebsrreport',
        'admin/deleteBankAccount',
        'admin/finished_goods_list',
        'admin/product_list_for_salesreturn',
        'admin/getopeningstockdetailsBSR',
        'admin/fetchLivestatusreport'
    ];
}



