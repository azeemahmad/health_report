<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class AuthenticateSales
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::guard('sales')->check()) {
            return redirect('/sales/login');
        }
        return $next($request);
    }
}
