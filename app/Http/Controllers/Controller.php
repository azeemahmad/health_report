<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Jobs\SendEmailJob;
use App\Jobs\UploadInventoryJob;
use Illuminate\Support\Facades\Artisan;
use Twilio\Rest\Client;

use App\Charts\UserChart;
use App\Division;
use App\Category;
use App\Models\ReportData;
use Illuminate\Support\Facades\DB;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendemail($data)
    {
        try {
            dispatch(new SendEmailJob($data));
            // Artisan::call('QueueRun:ListenCommand');
            return 1;
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function uploadInventoryData($singlefileK,$data){
        try {
            dispatch(new UploadInventoryJob($singlefileK,$data));
            // Artisan::call('QueueRun:ListenCommand');
            return 1;
        } catch (\Exception $e) {
            return 0;
        }
    }


    public function SendWhatsappMessage($receiver_number, $message)
    {
        $sid = env('TWILIO_SID', NULL);
        $token = env('TWILIO_TOKEN', NULL);
        $sender = env('TWILIO_FROM', NULL);

        if ($sid != NULL && $token != NULL && $sender != NULL) {
            $twilio = new Client($sid, $token);
            $message = $twilio->messages
                ->create("whatsapp:" . $receiver_number, // to
                    array(
                        "from" => "whatsapp:" . $sender,
                        "body" => $message
                    )
                );

            if (!empty($message->sid) && $message->sid != false && $message->sid != null) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function cwrr($id)
    {
        $date = base64_decode($id);
        $path = public_path('ReportsPDFFiles/');        
        $fileName =  'CWReport_'. $date . '.' . 'pdf' ;  
        return response()->file($path . $fileName);
    }

    public function dwrr($id)
    {  
        $date = base64_decode($id);
        $divisions=Division::where('status',1)->get();
        $recordsDetails = [];
    
        $start_date = $date;
        $end_date = $date;
        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"

        ];

        $categories = Category::where('status',1)->where('parent_id', 0)->pluck('name', 'id')->toArray();
        $d = ReportData::whereDate('date','>=',$start_date)->whereDate('date','<=',$end_date)->select(DB::raw('SUM(total_cost) as total_cost'),'category_id as category_id')->groupBy('category_id')->get();

        $d->map(function ($q) use($categories){           
                    $q->category_id=  $categories[$q->category_id];
                    $q->total_cost=  round($q->total_cost,2);
        });

        foreach($divisions as $division)
        {            
           $category_ids=  $division->categories->pluck('name')->toArray();
           $recordsDetails[ $division->name]= $d->whereIn('category_id',$category_ids)->sum('total_cost');
        }

        $division_wise_revenue_report_chart = new UserChart;
        $division_wise_revenue_report_chart->labels(array_keys($recordsDetails));
        $division_wise_revenue_report_chart->dataset('Division Wise Revenue Report', 'pie', array_values($recordsDetails))->color($borderColors)->backgroundcolor($fillColors);
       //dd($division_wise_revenue_report_chart, $recordsDetails);
        return view('shared.dvrr', compact('division_wise_revenue_report_chart','recordsDetails'));
    }
}
