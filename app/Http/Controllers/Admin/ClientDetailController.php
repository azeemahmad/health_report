<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\ClientDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use QrCodePackage;
use DB;
use Barryvdh\DomPDF\Facade as PDF;

class ClientDetailController extends Controller
{

    public function index(Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $client = ClientDetail::where('name', 'LIKE', "%$keyword%")
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $client = ClientDetail::latest()->paginate($perPage);
        }

        return view('admin.client-details.index', compact('client'));
    }

    public function create()
    {
        $lastData = ClientDetail::where('is_india',1)->orderBy('id','DESC')->first();
        return view('admin.client-details.create',compact('lastData'));
    }

    public function dubaiReport(){

        $lastData = ClientDetail::where('is_india',0)->orderBy('id','DESC')->first();
        return view('admin.client-details.create-dubai',compact('lastData'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
//            'passport_no' => 'required|unique:clients',
            'passport_no' => 'required',

        ]);
        $requestData = $request->all();
        if ($request->hasFile('logo')) {
            $filename = $requestData['passport_no'].'.'.$request->logo->extension();
            $request->logo->move(base_path('public/RML/Design/Circular'), $filename);
            $requestData['logo'] =$filename;
        }
        $requestData['logo']=$requestData['passport_no'].'.pdf';
        $client = ClientDetail::create($requestData);
        $first_date = new \DateTime($client->created_at);
        $second_date = new \DateTime(date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $client->dob))));
        $interval = $first_date->diff($second_date);
        $client->age = $interval->y;
        $client->full_age = $interval->y.' Yrs '.$interval->m.' Month '.$interval->m.' Days/'.ucwords($requestData['gender']);
        $client->is_india=1;
        $client->save();

        $fileQrcode = public_path() . '/Qrcode/'.$requestData['name'].'-'.$requestData['passport_no'].'.png';
        $destinationPath = public_path() . "/Qrcode/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        if($requestData['country']=='india'){
            if($requestData['gender']=='male'){
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/RML/Design/Circular/labreportnew.php?Phead=1&LabNo='.base64_encode($request['passport_no']);
                }

            }
            else{
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/RML/Design/Circular/labreportnew.php?Phead=1&LabNo='.base64_encode($request['passport_no']);
                }
            }
            if (!is_file($fileQrcode)) {
                QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
            }

            $client->url = $ewayQrcode;
            $client->save();
//            $destinationPath = public_path() . "/PassportDetails/".date('d-m-Y');
//            if (!is_dir($destinationPath)) {
//                mkdir($destinationPath, 0777, true);        }
//            $file = $destinationPath.'/'.$requestData['passport_no'].'.pdf';
//            $pdf = PDF::loadView('rml',compact('client'));
//            if(\File::exists($file)){
//                \File::delete($file);
//            }
//            $pdf->save($file);
        }
        elseif($requestData['country']=='lucknow'){

            if($requestData['gender']=='male'){
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/nldclab/upload1/NLDC1Report'.$requestData['passport_no'].'.pdf';
                }

            }
            else{
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/nldclab/upload1/NLDC1Report'.$requestData['passport_no'].'.pdf';
                }
            }
            if (!is_file($fileQrcode)) {
                QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
            }

        }
        elseif($requestData['country']=='heli'){

            if($requestData['gender']=='male'){
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/DOWNLOAD/PatientreportDirectview.php?UserIDPassword='.base64_encode($requestData['passport_no']);
                }

            }
            else{
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/DOWNLOAD/PatientreportDirectview.php?UserIDPassword='.base64_encode($requestData['passport_no']);
                }
            }
            if (!is_file($fileQrcode)) {
                QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
            }

        }
        else{
            if($requestData['gender']=='male'){
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/medsol-diagnostics/reports/'.$requestData['passport_no'].'.pdf';
                }

            }
            else{
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/medsol-diagnostics/reports/'.$requestData['passport_no'].'.pdf';
                }
            }
            if (!is_file($fileQrcode)) {
                QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
            }

        }

        $client->url = $ewayQrcode;
        $client->save();
        return redirect('admin/client-details')->with('flash_message', 'ClientDetail added!');
    }

    public function show($id)
    {
        $client = ClientDetail::findOrFail($id);

        return view('admin.client-details.show', compact('client'));
    }


    public function edit($id)
    {
        $client = ClientDetail::findOrFail($id);
        $lastData = ClientDetail::orderBy('id','DESC')->first();
        $updateRequestData = 1;
        if($client->is_india==1){
            return view('admin.client-details.edit', compact('client','lastData','updateRequestData'));
        }
        else{

            return view('admin.client-details.edit-dubai', compact('client','lastData','updateRequestData'));
        }

    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',

        ]);
        $requestData = $request->all();
        $client = ClientDetail::findOrFail($id);
        $client->update($requestData);
        $first_date = new \DateTime($client->created_at);
        $second_date = new \DateTime(date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $client->dob))));
        $interval = $first_date->diff($second_date);
        $client->age = $interval->y;
        $client->save();

        $fileQrcode = public_path() . '/Qrcode/'.$requestData['name'].'-'.$requestData['passport_no'].'.png';
        $destinationPath = public_path() . "/Qrcode/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        if($requestData['country']=='india'){
            if($requestData['gender']=='male'){
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/RML/Design/Circular/'.$requestData['passport_no'].'.pdf';
                }

            }
            else{
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/RML/Design/Circular/'.$requestData['passport_no'].'.pdf';
                }
            }
            if (!is_file($fileQrcode)) {
                QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
            }
            $client->url = $ewayQrcode;
            $client->save();
//
//
//            $destinationPath = public_path() . "/PassportDetails/".date('d-m-Y');
//            if (!is_dir($destinationPath)) {
//                mkdir($destinationPath, 0777, true);        }
//            $file = $destinationPath.'/'.$requestData['passport_no'].'.pdf';
//            if(\File::exists($file)){
//                \File::delete($file);
//            }
//            $html=$this->htmlData($client->id);
//            PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->save($file);

        }
        elseif($requestData['country']=='lucknow'){

            if($requestData['gender']=='male'){
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/nldclab/upload1/NLDC1Report'.$requestData['passport_no'].'.pdf';
                }

            }
            else{
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/nldclab/upload1/NLDC1Report'.$requestData['passport_no'].'.pdf';
                }
            }
            if (!is_file($fileQrcode)) {
                QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
            }

        }
        elseif($requestData['country']=='heli'){

            if($requestData['gender']=='male'){
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/DOWNLOAD/PatientreportDirectview.php?UserIDPassword='.base64_encode($requestData['passport_no']);
                }

            }
            else{
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/DOWNLOAD/PatientreportDirectview.php?UserIDPassword='.base64_encode($requestData['passport_no']);
                }
            }
            if (!is_file($fileQrcode)) {
                QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
            }

        }
        else{
            if($requestData['gender']=='male'){
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MR ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/medsol-diagnostics/reports/'.$requestData['passport_no'].'.pdf';
                }

            }
            else{
                if($requestData['type']=='normal'){
                    if($requestData['report_type']=='negative'){
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. NOT DETECTED";
                    }
                    else{
                        $ewayQrcode = "MS ".$requestData['name']."\n".$requestData['passport_no']." \nRT-PCR. DETECTED";
                    }

                }
                else{
                    $ewayQrcode='http://13.127.255.38/medsol-diagnostics/reports/'.$requestData['passport_no'].'.pdf';
                }
            }
            if (!is_file($fileQrcode)) {
                QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
            }

        }

        $client->url = $ewayQrcode;
        $client->save();
        return redirect('admin/client-details')->with('flash_message', 'ClientDetail updated!');
    }

    public function destroy($id)
    {
        ClientDetail::destroy($id);

        return redirect('admin/client-details')->with('flash_message', 'ClientDetail deleted!');
    }

    public function print($id){
        $client=ClientDetail::find($id);
        if($client->is_india==1){
            return view('rml',compact('client'));
        }
        else{
            return view('admin.dubai',compact('client'));
        }

    }

    public function htmlData($id){
        $client=ClientDetail::find($id);
        if($client->gender=='female'){
            $nameTitle = 'Ms';
            $gender = 'FEMALE';

        }
        else{
            $nameTitle = 'Mr';
            $gender='MALE';
        }
        $html='';

        $html.='<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Report</title>

    <style type="text/css">
     
         *{
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        @font-face {
            font-family: "times new roman", serif;
            src: url('.asset('times new roman.ttf').');
        }

        @font-face {
            font-family: Arial, sans-serif;
            src: url('.asset('ARIAL.TTF').');
        }

        body {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-size: 14px;
            /*            font-family: \'Roboto\', sans-serif;*/
            font-family: "times new roman", serif;
        }

        page {
            background: white;
            display: block;
            margin: 0 auto;
            margin-bottom: 0.5cm;
        }

        page[size="A4"] {
            width: 27cm;
            height: 29.7cm;
        }

        .text-center {
            text-align: center;
        }

        .table {
            width: 100%;
            max-width: 100%;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        table {
            background-color: transparent;
            border-spacing: 0;
            border-collapse: collapse;
        }

        .table>tbody>tr>td,
        .table>tbody>tr>th,
        .table>tfoot>tr>td,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>thead>tr>th {
            padding: 4px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        th {
            text-align: left;
        }


        p {
            font-size: 13px;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        .fnt-1 {
            margin: 10px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .table {
            margin: 0px;
        }

        .v-mdl {
            vertical-align: middle !important;
        }

        .v-top {
            vertical-align: top !important;
        }

        .no-vorder {
            border: none !important;
        }

        .bd-right-none {
            border-right: none !important;
        }

        .bd-left-none {
            border-left: none !important;
        }

        .invoice-wrapper {
            min-width: 680px;
        }

        .table-bordered,
        .table-bordered>tbody>tr>td,
        .table-bordered>tbody>tr>th,
        .table-bordered>tfoot>tr>td,
        .table-bordered>tfoot>tr>th,
        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border: 1px solid #000;
        }

        .table-bordered>thead>tr>td,
        .table-bordered>thead>tr>th {
            border-top: 0;
            border: 1px solid #000;
        }

        .s-table thead tr th {
            vertical-align: middle;
        }

        .s-table thead tr th:nth-child(5),
        .s-table tbody tr td:nth-child(5) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(6),
        .s-table tbody tr td:nth-child(6) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(7),
        .s-table tbody tr td:nth-child(7) {
            width: 75px;
        }

        .s-table thead tr th:nth-child(8),
        .s-table tbody tr td:nth-child(8) {
            width: 75px;
        }

        .s-table tbody.fnt-invoce tr td {
            font-size: 11px;
        }

        .igst-table {}

        .igst-table tbody tr th:nth-child(3) {
            width: 120px;
        }

        .igst-table tbody tr th {
            font-weight: 400;
        }

        .img-th {
            width: 150px;
        }

        .img-th img {
            max-width: 150px;
        }

        .first-table p {
            margin-bottom: 5px;
        }

        .first-table h4 {
            font-size: 14px;
            margin: 0;
        }

        .party-div {
            padding: 5px;
        }

        .fnt-invoce td {
            font-weight: 600;
        }

        .fnt-invoce td.nrml {
            font-weight: 400;
        }

        .first-table p span.caps {
            text-transform: uppercase;
        }

        .first-table h4 {
            color: #333;
        }

        .first-table h4.city {
            margin-right: 20px;
            display: inline-block;
            float: left;
        }

        .first-table h4.city strong {
            font-weight: 700;
            color: #000;
        }

        .first-table h3 {
            display: block;
            width: 100%;
            float: left;
            margin: 0 0 5px;
            font-size: 13px;
        }

        .right p {
            width: 50%;
            display: inline-block;
            margin-bottom: 5px;
            float: left;
        }

        .first-table tr h1 {
            margin: 5px 0px;
            text-transform: uppercase;
        }

        .top-table tr {
            padding: 2px 8px;
        }

        .top-table tr h2,
        .top-table tr h1 {
            margin: 0px 0px;
            font-size: 20px;
            text-transform: uppercase;
        }

        .top-table tr h2 {
            font-size: 15px;
            color: #606060;
        }

        .top-table tr td {
            border: none !important;
            padding: 0px !important;
        }

        .top-table tr:nth-child(1) {
            border: 1px solid #000 !important;
        }

        .top-table tr:nth-child(1) p {
            width: 33.33%;
            float: left;
            display: inline-block;
            font-size: 13px;
            text-align: left;
        }

        .top-table tr:nth-child(1) p.text-right {
            text-align: right;
        }

        .igst-table tbody>tr>td:nth-child(1) {
            width: 60%;
            float: left;
        }

        .igst-table tbody>tr>td {
            width: 40%;
            float: left;
        }

        .ten {
            border: 1px dashed #000;
            top: -8px;
            bottom: -8px;
            left: -8px;
            right: -8px;
            width: 80%;
            position: relative;
        }
    </style>

</head>

<body>
<page size="A4" id="area-cv-resume" width="1000" style="margin: 0 auto;display: block;position: relative;">
    <div class="logoWrapper" style="margin-bottom: 10px;">
        <img src="'.asset('header.jpeg').'" style="width: 100%" alt="">
    </div>

    <div class="patient-detail">
        <table style="border: 2px solid #000;width: 100%;">
            <tbody>
            <tr>
                <td style="padding: 3px 10px !important;width: 16%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Patient Name </p>
                </td>

                <td style="padding: 3px 10px !important;width: 45%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:600;font-size:16px;">: '.$nameTitle.'. '.$client->name.'</p>
                </td>

                <td style="padding: 3px 10px !important;width: 16%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Registered </p>
                </td>

                <td style="padding: 3px 10px !important;width: 45%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:16px;">: '.$client->registered_date.'</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 3px 10px !important;width: 16%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Refering Doctor </p>
                </td>

                <td style="padding: 3px 10px !important;width: 45%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:600;font-size:16px;">: Dr. SELF</p>
                </td>

                <td style="padding: 3px 10px !important;width: 16%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">SampleCollection </p>
                </td>

                <td style="padding: 3px 10px !important;width: 45%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:16px;">: '.$client->sample_date.'</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 3px 10px !important;width: 16%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Age/Sex </p>
                </td>

                <td style="padding: 3px 10px !important;width: 45%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:600;font-size:16px;">: '.$client->age.' YEARS/'.$gender.'</p>
                </td>

                <td style="padding: 3px 10px !important;width: 16%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Reported </p>
                </td>

                <td style="padding: 3px 10px !important;width: 45%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:16px;">: '.$client->reported_date.'</p>

                </td>
            </tr>

            <tr>
                <td style="padding: 3px 10px !important;width: 16%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Lab No. </p>
                </td>

                <td style="padding: 3px 10px !important;width: 45%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0px;font-weight:600;font-size:16px;">: '.$client->passport_no.'</p>
                </td>

                <td style="padding: 3px 10px !important;width: 16%;margin:0  1%;text-align: left; ">
                    <p style="margin: 0px;font-weight:normal;font-size:15px;">Sample No </p>
                </td>

                <td style="padding: 3px 10px !important;width: 45%;margin:0  1%;text-align: left; ">
                    <p class="bd-btm" style="margin: 0;font-weight:600;font-size:16px;">: '.$client->sample_no.'</p>

                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="tab-head" style="margin: 0px 0;width:100%; ">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <table style="border: 2px solid #000;width: 100%;margin: 20px 0px;">
                        <tbody>
                        <tr>
                            <td style="padding:0px !important;margin:0  auto;text-align: center; ">
                                <h2 style="margin: 0px;font-size: 18px;padding: 5px 0px;">RT-PCR High Throughput Advance Molecular Lab</h2>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>


            <tr>
                <td>

                    <table style="border: 0px solid #000;width: 100%;">
                        <tbody>
                        <tr>
                            <td style="padding: 0px 10px !important;margin:0  1%;text-align: left; ">
                                <p style="margin: 0px;border-bottom: 2px solid #000;width: max-content;font-weight: 700;font-size: 18px;">RT-PCR COVID-19 Virus Qualitative Test*</p>
                            </td>

                            <td colspan="2" style="padding: 0px 0px !important;width: 40%;margin:0  1%;text-align: left; ">
                                <p class="bd-btm" style="margin: 0px;font-weight: 700;font-size: 18px;">Passport No : {{$client->passport_no}}</p>
                            </td>


                        </tr>

                        <tr>
                            <td style="padding: 10px 5px  0!important;margin:0  1%;text-align: left; ">
                                <p style="margin: 0px;font-weight: 700;font-size: 18px;padding-left:20px;">RT-PCR COVID-19 Virus Qualitative Test</p>
                            </td>


                        </tr>
                        <tr>
                            <td style="padding: 0px 5px !important;width: 60%;margin:0  1%;text-align: left; ">
                                <p style="margin: 0px;font-size:16px;font-weight:500;padding-left:20px;font-family: Arial, sans-serif;">E-Sarbeco Gene : </p>
                            </td>

                            <td style="padding: 0px 5px !important;width: 40%;margin:0  1%;text-align: left; ">
                                <p class="bd-btm" style="margin: 0px;font-weight: 700;font-size: 18px;">NEGATIVE</p>
                            </td>


                        </tr>
                        <tr>
                            <td style="padding: 0px 5px !important;width: 60%;margin:0  1%;text-align: left; ">
                                <p style="margin: 0px;font-size:16px;font-weight:500;padding-left:20px;font-family: Arial, sans-serif;">RdRp Gene + N Gene</p>
                            </td>

                            <td style="padding: 0px 5px !important;width: 40%;margin:0  1%;text-align: left; ">
                                <p class="bd-btm" style="margin: 0;font-weight: 700;font-size: 18px;">NEGATIVE</p>

                            </td>


                        </tr>

                        <tr>

                            <td style="padding: 0px 5px !important;width: 60%;margin:0  1%;text-align: left; ">
                                <p style="margin: 0px;font-weight: 700;font-size: 18px;padding-left:20px;">RESULT (Conclusion)</p>
                            </td>

                            <td style="padding: 0px 5px !important;width: 40%;margin:0  1%;text-align: left; ">
                                <p class="bd-btm" style="margin: 0;font-weight: 700;font-size: 18px;">NEGATIVE</p>

                            </td>
                        </tr>
                        </tbody>
                    </table>



                </td>
            </tr>




            </tbody>
        </table>
    </div>


    <div class="tab-head" style="margin: 30px 0;width:1000px; ">
        <div class="ten"></div>
        <p style="border-bottom: 1px solid #000;width: max-content;margin: 0;font-size: 15px;font-weight: 600;">Interpretation</p>

        <div class="water-img">
            <img src="'.asset('watermark.png').'" style="margin-top: -8%;width: 700px;margin-left: 18%;">
        </div>

        <table style="margin-top: 00px;width: 800px;border-collapse:collapse;position: relative;margin-top: -17%;font-size: 17.5px">
            <thead>
            <tr>
                <td style="border:2px solid #000;padding:2px 5px;font-weight:600;">Observation</td>
                <td style="border:2px solid #000;padding:2px 5px;font-weight:600;">Interpretation</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="border:2px solid #000;padding:2px 5px;">
                    If only internal Control (IC) positive then
                </td>
                <td style="border:2px solid #000;padding:2px 5px;">Negative for COVID-19 Virus</td>
            </tr>

            <tr>
                <td style="border:2px solid #000;padding:2px 5px;">
                    If Positive for E-Sarbeco,N Gene RdRp & IC then
                </td>
                <td style="border:2px solid #000;padding:2px 5px;">Positive for COVID-19 Virus</td>
            </tr>


            </tbody>
        </table>


    </div>

    <div class="table-data-1" style="width:100%;font-size: 18px;">
        <p style="font-weight: 600;font-size: 15px;padding-bottom: 5px;">ICMR No.: RMLPL001</p>
        <span style="border-bottom: 1px solid #000;width: max-content;margin: 0;font-weight: 600;">Note</span>
        <ul style="text-align: justify">
            <li>Negative results does not rule out the possibility of COVID-19 infection espicially if clinical suspecion is high. The combination of
                clinical symptoms, typical CT imaging features, and dynamic changes must be considered and test repeated after few days.Rates of
                Positive PCR may be affected by Stage of the disease and /or its severity, Presence of inhibitors, mutations & insufficient organism
                RNA can influence the result.</li>
            <li>Covid-19 Test conducted as per protocol ICMR/GOI.</li>
            <li>Kindly consult referring Physician/Authorized Govt. hospital for appropriate follow-up.</li>
            <li>Inconclusive result specimen will be repeated.</li>
        </ul>
    </div>

    <div class="table-data-2" style="width:100%;font-size: 17.5px;">
        <span style="border-bottom: 1px solid #000;width: max-content;margin: 0;font-weight: 600;">Comments</span>
        <ul style="padding-left: 5px;text-align: justify">
            <li style="list-style:none;" ><span style="font-weight:bold;">Nasopharyngeal sample :</span> This is the most practical and readily means of confirming Covid-19 diagnosis, with positive rates of ~75% during
                the first 2 weeks of illness in patients with to have severe disease. For patients with mild Covid-19, a positive PCR rate of 72% has been
                reported during the 1st week, dropping to 54% during the 2nd week.</li>
            <li style="list-style:none;"><span style="font-weight:bold;">Oropharyngeal sample :</span> Lower positive PCR rates have been observed with throat swabs, as low as ~30% in mild Covid-19 during the 2nd
                week of the illness and ~60% in severe disease during the first week of illness.</li>
        </ul>
        <h2 style="text-align: center;font-size: 18px;">*** End Of Report ***</h2>
    </div>

    <div class="footer" style="position: relative;width: 100%;margin-top: -11px;">
        <table>
            <tbody>
            <tr>
                <td>
                    <img src="'.url('Qrcode/'.$client->name.'-'.$client->passport_no.'.png').'" style="position: absolute;left: 5%;bottom: 41%;">
                    <img src="'.asset('footer.png').'" style="width: 100%;">
                    <img src="'.asset('logo.png').'" style="position: absolute;left: 85%;bottom: 35%;width: 140px;">
                </td>
            </tr>
            </tbody>
        </table>
    </div>


</page>
</body>

</html>';


        return $html;
    }

    public function dubaiReportStore(Request $request){
        $requestData = $request->all();
        if ($request->hasFile('logo')) {
            $filename = $requestData['passport_no'].'.'.$request->logo->extension();
            $request->logo->move(base_path('public/RML/Design/Circular'), $filename);
            $requestData['logo'] =$filename;
        }
        $requestData['logo']=$requestData['passport_no'].'.pdf';
        $client = ClientDetail::create($requestData);
        $first_date = new \DateTime($client->created_at);
        $second_date = new \DateTime(date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $client->dob))));
        $interval = $first_date->diff($second_date);
        $client->age = $interval->y;
        $client->is_india=0;
        $client->full_age = $interval->y.' Yrs '.$interval->m.' Month '.$interval->d.' Days/'.ucwords($requestData['gender']);
        $client->save();
        $fileQrcode = public_path() . '/Qrcode/'.$requestData['name'].'-'.$requestData['passport_no'].'.png';
        $destinationPath = public_path() . "/Qrcode/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        $ewayQrcode='http://13.127.255.38/medsol-diagnostics/reports/'.$requestData['passport_no'].'.pdf';
        if (!is_file($fileQrcode)) {
            QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
        }
        $client->url = $ewayQrcode;
        $client->save();
        return redirect('admin/client-details')->with('flash_message', 'ClientDetail added!');
    }

    public function dubaiReportupdate(Request $request,$id){

        $this->validate($request, [
            'name' => 'required',

        ]);
        $requestData = $request->all();
        $client = ClientDetail::findOrFail($id);
        $client->update($requestData);
        $first_date = new \DateTime($client->created_at);
        $second_date = new \DateTime(date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $client->dob))));
        $interval = $first_date->diff($second_date);
        $client->age = $interval->y;
        $client->is_india=0;
        $client->full_age = $interval->y.' Yrs '.$interval->m.' Month '.$interval->d.' Days/'.ucwords($requestData['gender']);
        $client->save();
        $fileQrcode = public_path() . '/Qrcode/'.$requestData['name'].'-'.$requestData['passport_no'].'.png';
        $destinationPath = public_path() . "/Qrcode/";
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        $ewayQrcode='http://13.127.255.38/medsol-diagnostics/reports/'.$requestData['passport_no'].'.pdf';
        if (!is_file($fileQrcode)) {
            QrCodePackage::format('png')->size(100)->margin(0)->generate($ewayQrcode, $fileQrcode);
        }
        $client->url = $ewayQrcode;
        $client->save();
        return redirect('admin/client-details')->with('flash_message', 'ClientDetail Updated!');

    }

}
