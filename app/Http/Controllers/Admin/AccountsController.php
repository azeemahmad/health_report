<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Account;
use App\SalesPerson;
use Illuminate\Http\Request;


class AccountsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $accounts = Account::where('sales_person_name', 'LIKE', "%$keyword%")
                ->orWhere('distance', 'LIKE', "%$keyword%")
                ->orWhere('advanced_amount', 'LIKE', "%$keyword%")
                ->orWhere('total_amount', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $accounts = Account::latest()->paginate($perPage);
        }

        return view('admin.accounts.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $salesPerson = SalesPerson::pluck('sales_person_name', 'sales_person_name');
        return view('admin.accounts.create',compact('salesPerson'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'sales_person_name' => 'required'
		]);
        $requestData = $request->all();
        
        Account::create($requestData);

        return redirect('admin/accounts')->with('flash_message', 'Account added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $account = Account::findOrFail($id);

        return view('admin.accounts.show', compact('account'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $salesPerson = SalesPerson::pluck('sales_person_name', 'sales_person_name');
        $account = Account::findOrFail($id);

        return view('admin.accounts.edit', compact('account','salesPerson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'sales_person_name' => 'required'
		]);
        $requestData = $request->all();
        
        $account = Account::findOrFail($id);
        $account->update($requestData);

        return redirect('admin/accounts')->with('flash_message', 'Account updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Account::destroy($id);

        return redirect('admin/accounts')->with('flash_message', 'Account deleted!');
    }
}
