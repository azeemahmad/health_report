<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SalesPerson;
use Illuminate\Http\Request;


class SalesPersonsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $salespersons = SalesPerson::where('sales_person_name', 'LIKE', "%$keyword%")
                ->orWhere('is_active', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $salespersons = SalesPerson::latest()->paginate($perPage);
        }

        return view('admin.sales-persons.index', compact('salespersons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.sales-persons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'sales_person_name' => 'required'
		]);
        $requestData = $request->all();
        
        SalesPerson::create($requestData);

        return redirect('admin/sales-persons')->with('flash_message', 'SalesPerson added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $salesperson = SalesPerson::findOrFail($id);

        return view('admin.sales-persons.show', compact('salesperson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $salesperson = SalesPerson::findOrFail($id);

        return view('admin.sales-persons.edit', compact('salesperson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'sales_person_name' => 'required'
		]);
        $requestData = $request->all();
        
        $salesperson = SalesPerson::findOrFail($id);
        $salesperson->update($requestData);

        return redirect('admin/sales-persons')->with('flash_message', 'SalesPerson updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        SalesPerson::destroy($id);

        return redirect('admin/sales-persons')->with('flash_message', 'SalesPerson deleted!');
    }
}
