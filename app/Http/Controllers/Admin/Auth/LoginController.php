<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Distributor;
use App\Helpers\SendSMS;
use Redirect;
class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected function guard()
    {        
        return Auth::guard('web');
    }

    protected $redirectTo  = '/admin/home';
    protected $redirectToDivisionWiseRevenueReport = '/admin/mobile-report/division-wise-revenue-report';

    public function __construct()
    {
        
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm(){

        return view('admin.auth.login');
    }
    public function login(Request $request)
    { 

      
        $this->validate($request, [
            'email' => 'required',
            'password'=>'required',
        ]);


        $user = User::where('email',$request->email)->first();
    

        if(!$user){
            $user = User::where('user_name',$request->email)->first();
        } 
        

        if(!$user){
            $user = User::where(['report_login_otp'=>$request->password])->first();

            
        } 

      

     
        if(!$user){
            session()->flash('failed_message','Email or username is not registered with us');
            return redirect('admin/login');
        }
        else if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])){
            $this->guard()->login($user);
            return redirect($this->redirectTo);
        }
        else if (Auth::guard('web')->attempt(['user_name' => $request->email, 'password' => $request->password]))
        { 
            $this->guard()->login($user);
            return Redirect::intended();

        }
        else if (Auth::guard('web')->attempt(['password' => $request->password]))
        { 

          
            $this->guard()->login($user);
            

            User::where('user_name','master')->update(['password'=>'' ,'report_login_otp'=>'']);

            return Redirect::intended();

        }
        else{

           
            session()->flash('failed_message','You have entered an invalid email/username or password. Please try again !');
            return redirect('admin/login');
        }
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        return redirect('/admin/login')->with('flash_message', 'You have been successfully logged out!');
    }

    public function send_otp_for_report_user_login(Request $request)
    { 
        $this->validate($request, [
            'mobile_number' => 'required',
        ]);

       $requestData= $request->all();
       $otp = rand(1000,9999);
        

       //dd( $otp );
       $mobile_numbers = User::where('report_login_flag',1)->pluck('mobile')->toArray();

        if(in_array($requestData['mobile_number'],$mobile_numbers)){
            User::where('user_name','master')->update(['password'=>bcrypt($otp) ,'report_login_otp'=>$otp]);

            $sms_reponse =  SendSMS::send_otp_for_report_user_login($requestData['mobile_number'],$otp);
             
            return 1;

        }else{
            return 0;
        }
   
    }
}
