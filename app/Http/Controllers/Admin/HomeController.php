<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\UploadInventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Retailer;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {

        return view('admin.home');
    }

    public function profile()
    {
        return view('admin.profile');
    }


    public function saveprofile(Request $request)
    {

        $user_list = Auth::user();
        if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
                'state' => 'required',
                'city' => 'required',
                'mobile' => 'required|digits:10|numeric|unique:users,mobile,' . Auth::user()->id,
            ]);
        } else {
            if (!Hash::check($request['old_password'], $user_list->password)) {
                return redirect()->back()->with('error_password', 'Old Password not Match from Database!');
            }

            $this->validate($request, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . Auth::user()->id,
                'password' => 'required|string|min:6|confirmed|different:old_password',
                'old_password' => 'required|string|min:6',
                'state' => 'required',
                'city' => 'required',
                'mobile' => 'required|digits:10|numeric|unique:users,mobile,' . Auth::user()->id,
            ]);
        }
        $data = $request->all();

        if ($request['password'] == null && $request['password_confirmation'] == null && $request['old_password'] == null) {
            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images/profile_image'), $filename);

                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'image' => $filename,
                    'mobile' => $data['mobile'],
                    'state' => $data['state'],
                    'city' => $data['city']
                ]);
            } else {
                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'mobile' => $data['mobile'],
                    'state' => $data['state'],
                    'city' => $data['city']
                ]);
            }

        } else {

            if ($request->hasFile('image')) {
                $filename = $this->getFileName($request->image);
                $request->image->move(base_path('public/images/profile_image'), $filename);

                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'new_password' => $data['password'],
                    'image' => $filename,
                    'mobile' => $data['mobile'],
                    'state' => $data['state'],
                    'city' => $data['city']
                ]);

            } else {
                $user_list->update([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'new_password' => $data['password'],
                    'mobile' => $data['mobile'],
                    'state' => $data['state'],
                    'city' => $data['city']
                ]);
            }
        }
        return redirect('/admin/profile')->with('flash_message', 'Profile updated Successfully!');
    }

    protected function getFileName($file)
    {
        return str_random(32) . '.' . $file->extension();
    }

    public function uploadFile()
    {
        return view('admin.retailers.upload');
    }

    public function saveRetailerData(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required'
        ]);
        $data = Excel::toArray(new UploadInventory, $request->file('import_file'));
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                foreach ($value as $vk => $vv) {
                    try {
                       if(isset($vv[0]) && $vv[0] != '' && $vv[0] != null){
                           $retailer['retailer_outlet'] = strtoupper($vv[0]);
                           $retailer['category_1'] = strtoupper($vv[1]);
                           $retailer['category_2'] = strtoupper($vv[2]);
                           $retailer['owner_name'] = strtoupper($vv[3]);
                           $retailer['whatsapp_mobile'] = $vv[4];
                           $retailer['mobile'] = $vv[4];
                           $retailer['address_1'] = strtoupper($vv[5]);
                           $retailer['city'] = strtoupper($vv[5]);
                           $retailer['area'] = strtoupper($vv[5]);
                           $retailer['state'] = 'UTTAR PRADESH';
                           $retailer['pincode'] = $vv[7];
                           $retailer['availability_status'] = 'NO';
                           $retailer['date'] = '30/06/2021';
                           if($vv[6]==1){
                               $retailer['sales_person_name'] = 'SHRIKANT PANDEY';
                           }
                           elseif ($vv[6]==2){
                               $retailer['sales_person_name'] = 'HSARAT AZMI';
                           }
                           elseif ($vv[6]==3){
                               $retailer['sales_person_name'] = 'KAMLESH VISHWAKARMA';
                           }

                           Retailer::create($retailer);
                       }
                       else{
                           dd(456);
                       }

                    } catch (\Exception $e) {
                          dd($e);
                    }

                }
            }
        }
        else{
            dd(123);
        }
    }


}
