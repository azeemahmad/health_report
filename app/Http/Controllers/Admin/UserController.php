<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;
use App\Permission;
use App\Authorizable;
use App\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use App\Category;
use Artisan;

class UserController extends Controller
{
    use Authorizable;
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 500;

        if (!empty($keyword)) {
            $result = User::orWhereHas('roles', function (Builder $query) use($keyword){
                $query->where('name', 'like', "%$keyword%");
            })->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('user_name', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $result = User::latest()->paginate($perPage);
        }
        return view('admin.user.index', compact('result'));
    }

    public function create()
    {
        $subcategory=[];
        $regions=Region::where('status',1)->get();
        $roles = Role::where('guard_name','web')->where('name','!=','Admin')->get();
        $category=Category::where('parent_id',0)->pluck('name','id');
        return view('admin.user.new', compact('roles','regions','category','subcategory'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'user_name' => 'required|unique:users',
            'state' => 'required',
            'city' => 'required',
//            'mobile' => 'required|digits:10|numeric|unique:users',
            'roles' => 'required|min:1',
//            'image' => 'mimes:jpeg,jpg,png,gif|max:10000'
        ]);
        $datas=$request->all();
        $request->merge(['password' => bcrypt($request->get('password'))]);


       $request['categories_id']= isset($request['categories_id']) ?  serialize($request['categories_id']) : null;
        $request['subcategories_id']= isset($request['subcategories_id']) ?  serialize($request['subcategories_id']) :null;
        
     
       
        if ( $user = User::create($request->except('roles', 'permissions')) ) {
            $id=$user->id;
            $user->new_password=$datas['password'];
            $user->save();

            if ($request->hasFile('image')) {         
                $distriImage['image'] =  \ImageS3Upload::image_upload_s3($request->image,'images/profile_image/');
                $user->update($distriImage);
            }


            $this->syncPermissions($request, $user);

            $url = URL::signedRoute('verification.verify',['id' => $id], now()->addMinutes(30));
            $data['link'] = $url;
            $data['to_email'] = $user->email;
            $data['from_email'] = 'admin@orpat.com';
            $data['subject']='Orpat Admin Reset password.';
            $data['title']='Orpat Admin';
            $data['view']='admin.email.resendverificationmail';
            $this->sendemail($data);
            return redirect('admin/users')->with('flash_message','User has been created.');

        } else {
            return redirect('admin/users')->with('error_message','Unable to create user.');

        }

    }

    public function show($id)
    {
        $user_list = User::find($id);
        return view('admin.user.show', compact('user_list'));
    }

    public function edit($id)
    {
        $regions=Region::where('status',1)->get();
        $subcategory=[];
        $user = User::find($id);
        $roles = Role::where('guard_name','web')->where('name','!=','Admin')->get();
        $rol = Role::where('guard_name','web')->where('name','!=','Admin')->pluck('name', 'id');
        $permissions = Permission::where('guard_name','web')->get();
        $category=Category::where('parent_id',0)->pluck('name','id');
       
        if(isset($user->categories_id)){
            $subcategory_ids = unserialize($user->categories_id);  
            $subcategory=Category::wherein('parent_id',$subcategory_ids)->pluck('name','id'); 
              
        }
        //$category=Category::whereIn('parent_id',0)->pluck('name','id');

     
        return view('admin.user.edit', compact('user','subcategory', 'roles', 'permissions','rol','regions','category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $id,
            'user_name' => 'required|unique:users,user_name,' . $id,
            'state' => 'required',
            'city' => 'required',
//            'mobile' => 'required|digits:10|numeric|unique:users,mobile,' . $id,
            'roles' => 'required|min:1',
//            'image' => 'mimes:jpeg,jpg,png,gif|max:10000'
        ]);

        $user = User::findOrFail($id);

        if ($request->hasFile('image')) {
            $distriImage['image'] =  \ImageS3Upload::image_upload_s3($request->image,'images/profile_image/');
            $user->update($distriImage);
        }


        $request['categories_id']= isset($request['categories_id']) ?  serialize($request['categories_id']) : null;
        $request['subcategories_id']= isset($request['subcategories_id']) ?  serialize($request['subcategories_id']) :null;
        
     
        $user->fill($request->except('roles', 'permissions', 'password'));

        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
            $user->new_password=$request->get('password');
        }
        $this->syncPermissions($request, $user);
      //  $user->categories_id=serialize($request['categories_id']);

    
        $user->save();
        return redirect('admin/users')->with('flash_message','User has been updated.');
    }
    public function destroy($id)
    {

        if ( Auth::user()->id == $id) {
            return redirect()->back()->with('error_message', 'Deletion of currently logged in user is not allowed :');
        }

        if( User::findOrFail($id)->delete() ) {
            return redirect('admin/users')->with('flash_message', 'User delete successfully');
        } else {
            return redirect('admin/users')->with('error_message', 'User not deleted. Try again !');
        }
    }

    private function syncPermissions(Request $request, $user)
    {
        // Get the submitted roles
        $roles = $request->get('roles', []);
        $permissions = $request->get('permissions', []);

        // Get the roles
        $roles = Role::where('guard_name','web')->find($roles);

        // check for current role changes
        if( ! $user->hasAllRoles( $roles ) ) {
            // reset all direct permissions for user
            $user->permissions()->sync([]);
        } else {
            // handle permissions
            $user->syncPermissions($permissions);
        }

        $user->syncRoles($roles);

        return $user;
    }


    public function login_as_user(Request $request,$id)
    {

      //  $this->guard()->logout();
    //    $request->session()->flush();

    if(Auth::user()->roles->implode('name', ', ')=='Admin'){

        $user = User::find($id);
        Auth::login($user);
        return redirect('admin/home');

    }
    abort(404);
  
   
    }

}
