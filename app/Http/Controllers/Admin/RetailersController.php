<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Retailer;
use App\SalesPerson;
use Illuminate\Http\Request;


class RetailersController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $retailers = Retailer::where('date', 'LIKE', "%$keyword%")
                ->orWhere('owner_name', 'LIKE', "%$keyword%")
                ->orWhere('retailer_outlet', 'LIKE', "%$keyword%")
                ->orWhere('address_1', 'LIKE', "%$keyword%")
                ->orWhere('address_2', 'LIKE', "%$keyword%")
                ->orWhere('address_3', 'LIKE', "%$keyword%")
                ->orWhere('address_4', 'LIKE', "%$keyword%")
                ->orWhere('city', 'LIKE', "%$keyword%")
                ->orWhere('state', 'LIKE', "%$keyword%")
                ->orWhere('pincode', 'LIKE', "%$keyword%")
                ->orWhere('whatsapp_mobile', 'LIKE', "%$keyword%")
                ->orWhere('mobile', 'LIKE', "%$keyword%")
                ->orWhere('land_line_number', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('area', 'LIKE', "%$keyword%")
                ->orWhere('classification', 'LIKE', "%$keyword%")
                ->orWhere('category_1', 'LIKE', "%$keyword%")
                ->orWhere('category_2', 'LIKE', "%$keyword%")
                ->orWhere('category_3', 'LIKE', "%$keyword%")
                ->orWhere('availability_status', 'LIKE', "%$keyword%")
                ->orWhere('territory_code', 'LIKE', "%$keyword%")
                ->orWhere('date_of_birth', 'LIKE', "%$keyword%")
                ->orWhere('wedding_year', 'LIKE', "%$keyword%")
                ->orWhere('pan_number', 'LIKE', "%$keyword%")
                ->orWhere('aadhar_number', 'LIKE', "%$keyword%")
                ->orWhere('gst_number', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $retailers = Retailer::latest()->paginate($perPage);
        }

        return view('admin.retailers.index', compact('retailers'));
    }

    public function create()
    {
        $salesPerson = SalesPerson::pluck('sales_person_name', 'sales_person_name');
        $category = Category::pluck('name', 'code');
        return view('admin.retailers.create', compact('salesPerson', 'category'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => 'required',
            'address_1' => 'required'
        ]);
        $requestData = $request->all();
        Retailer::create($requestData);
        return redirect('admin/retailers')->with('flash_message', 'Retailer added!');
    }
    public function show($id)
    {
        $retailer = Retailer::findOrFail($id);

        return view('admin.retailers.show', compact('retailer'));
    }
    public function edit($id)
    {
        $retailer = Retailer::findOrFail($id);
        $salesPerson = SalesPerson::pluck('sales_person_name', 'sales_person_name');
        $category = Category::pluck('name', 'code');

        return view('admin.retailers.edit', compact('retailer', 'salesPerson', 'category'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date' => 'required',
            'address_1' => 'required'
        ]);
        $requestData = $request->all();

        $retailer = Retailer::findOrFail($id);
        $retailer->update($requestData);

        return redirect('admin/retailers')->with('flash_message', 'Retailer updated!');
    }
    public function destroy($id)
    {
        Retailer::destroy($id);

        return redirect('admin/retailers')->with('flash_message', 'Retailer deleted!');
    }
}
