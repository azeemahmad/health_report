<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\SendSMS;
use App\InvoiceOrderList;
use App\InvoiceOrderProductList;
use App\Models\PForm\InvoicePFormOrder;
use App\Models\PForm\InvoicePFormOrderProductList;

class SendSMSCommand extends Command
{

    protected $signature = 'SendSMSDetails:SendSMSDetails';

    protected $description = 'Send SMS to Distributor Command';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {  
        $truckDetails1  = InvoiceOrderList::whereNotNull('invoice_no')->where('is_sms_sent', 0)->groupBy('truck_number')->pluck('truck_number');
        $truckDetails2  = InvoicePFormOrder::whereNotNull('invoice_no')->where('is_sms_sent', 0)->groupBy('truck_number')->pluck('truck_number');
        
        $truckDetails = $truckDetails1->merge($truckDetails2);
        $truckDetails = $truckDetails->unique();     

        try
        {
            if(isset($truckDetails) && $truckDetails->isNotEmpty())
            {
                foreach($truckDetails as $truck_number)
                {
                    $distributorIds1 = InvoiceOrderList::whereNotNull('invoice_no')->where('truck_number', $truck_number)->where('is_sms_sent', 0)->groupBy('distributor_id')->pluck('distributor_id');
                    $distributorIds2 = InvoicePFormOrder::whereNotNull('invoice_no')->where('truck_number', $truck_number)->where('is_sms_sent', 0)->groupBy('created_by')->pluck('created_by');
                    
                    $distributorIds = $distributorIds1->merge($distributorIds2);
                    $distributorIds = $distributorIds->unique();

                    if(isset($distributorIds) && $distributorIds->isNotEmpty())
                    {
                        foreach($distributorIds as $distributor_id)
                        {
                            $orderDetails1 = InvoiceOrderList::where('distributor_id', $distributor_id)->where('truck_number', $truck_number)->where('is_sms_sent', 0)->get();
                            $orderDetails2 = InvoicePFormOrder::where('created_by', $distributor_id)->where('truck_number', $truck_number)->where('is_sms_sent', 0)->get();                           
                            
                            $totalBoxes    = 0;
                            $totalQuantity = 0;
                            $orderIds1 = collect([]);
                            $orderIds2 = collect([]);

                            if(isset($orderDetails1) && $orderDetails1->isNotEmpty())
                            {
                                $ids1 = $orderDetails1->pluck('id');
                                $orderIds1  = $orderDetails1->pluck('order_id');                                
                                $distributorNumber = $orderDetails1[0]->distributor->personal_mobile;
                                $distributorName = $orderDetails1[0]->distributor->distributor_firm_name;
                                $totalBoxes = InvoiceOrderProductList::whereIn('invoice_order_list_id', $ids1)->sum('order_quantity');
                                $transportData = unserialize($orderDetails1[0]->truck_details);    
                            }

                            if(isset($orderDetails2) && $orderDetails2->isNotEmpty())
                            {
                                $ids2 = $orderDetails2->pluck('id');
                                $orderIds2  = $orderDetails2->pluck('order_id');                                
                                $distributorNumber = $orderDetails2[0]->distributor->personal_mobile;
                                $distributorName = $orderDetails2[0]->distributor->distributor_firm_name;
                                $totalQuantity = $totalQuantity + InvoicePFormOrderProductList::whereIn('invoice_pform_order_id', $ids2)->sum('order_quantity');
                                $transportData = unserialize($orderDetails2[0]->truck_details);
                            }
                           
                             $orderIds = $orderIds1->merge($orderIds2);  
                            
                             SendSMS::send_sms($orderIds,$distributorNumber,$distributorName,$totalBoxes, $totalQuantity,$transportData);
                             InvoiceOrderList::where('distributor_id', $distributor_id)->where('truck_number', $truck_number)->where('is_sms_sent' , 0)->update(['is_sms_sent' => 1]);
                             InvoicePFormOrder::where('created_by', $distributor_id)->where('truck_number', $truck_number)->where('is_sms_sent' , 0)->update(['is_sms_sent' => 1]);

                            sleep(3);
                        }
                    }
                }
            }
        }
        catch (\Exception $e)
        { 
           // dd($e);
        }
    }
}
