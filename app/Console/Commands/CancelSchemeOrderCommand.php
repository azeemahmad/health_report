<?php

namespace App\Console\Commands;

use App\MixedModelScheme;
use App\Models\SalesOrderProductLists;
use Illuminate\Console\Command;
use App\Models\SFormOrder;
use App\Models\ParentSFormTransaction;
use App\Models\PForm\ParentPFormTransaction;
use App\Models\PForm\PFormOrder;
use App\AnnualWiseScheme;
use App\Categoryscheme;
use App\MonthlyewiseScheme;
use App\Scheme;
use App\PartCategorySchemes;
use App\ForeignWiseScheme;
use App\InvoicewiseScheme;
use App\MonthlyModelWiseScheme;

use App\Models\PForm\InvoicePFormOrder;
use App\Models\PForm\InvoicePFormOrderProductList;
use App\Models\PForm\InvoiceParentPFormTransaction;
class CancelSchemeOrderCommand extends Command
{

    protected $signature = 'CancelSchemeOrderDetails:CancelSchemeOrderDetails';

    protected $description = 'Cancel Scheme Order Command';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $scheme = Scheme::where('end_date', date('Y-m-d', strtotime("-1 days")))->get();
        if (isset($scheme) && $scheme->isNotEmpty()) {
            foreach ($scheme as $key => $value) {
                $sformOrder = SFormOrder::whereIn('order_status', [0, 1, 2, 3])->whereIn('scheme_order_type', ['modelwise', 'modelwiseparts'])->where('scheme_id', $value->id)->get();
                if (isset($sformOrder) && $sformOrder->isNotEmpty()) {
                    foreach ($sformOrder as $sk => $sv) {
                        try {
                            ParentSFormTransaction::where('id', $sv->parent_sform_id)->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'modelwise',
                                'expire_scheme_id' => $value->id
                            ]);
                            $sv->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'modelwise',
                                'expire_scheme_id' => $value->id,
                                'scheme_order_type' => null,
                                'scheme_id' => null,
                                'scheme_type' => null,
                                'parent_order_id' => 0
                            ]);

                        } catch (\Exception $e) {

                        }

                    }
                }
            }
        }

        $annualScheme = AnnualWiseScheme::where('end_date', date('Y-m-d', strtotime("-1 days")))->get();
        if (isset($annualScheme) && $annualScheme->isNotEmpty()) {
            foreach ($annualScheme as $key => $value) {
                $sformOrder = SFormOrder::whereIn('order_status', [0, 1, 2, 3])->where('scheme_id', $value->id)->where('scheme_order_type', 'annually')->get();
                if (isset($sformOrder) && $sformOrder->isNotEmpty()) {
                    foreach ($sformOrder as $sk => $sv) {
                        try {
                            ParentSFormTransaction::where('id', $sv->parent_sform_id)->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'annually',
                                'expire_scheme_id' => $value->id
                            ]);
                            $sv->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'annually',
                                'expire_scheme_id' => $value->id,
                                'scheme_order_type' => null,
                                'scheme_id' => null,
                                'scheme_type' => null,
                                'parent_order_id' => 0
                            ]);

                        } catch (\Exception $e) {

                        }

                    }
                }
            }
        }

        $categoryScheme = Categoryscheme::where('end_date', date('Y-m-d', strtotime("-1 days")))->get();
        if (isset($categoryScheme) && $categoryScheme->isNotEmpty()) {
            foreach ($categoryScheme as $key => $value) {
                $sformOrder = SFormOrder::whereIn('order_status', [0, 1, 2, 3])->where('scheme_id', $value->id)->where('scheme_order_type', 'categorywise')->get();

                if (isset($sformOrder) && $sformOrder->isNotEmpty()) {
                    foreach ($sformOrder as $sk => $sv) {
                        try {
                            if($sv->parent_order_id==0){
                                ParentSFormTransaction::where('id', $sv->parent_sform_id)->update([
                                    'expire_scheme_status' => 1,
                                    'order_status' => 0,
                                    'approved_by_account_manager' => 0,
                                    'payment_status' => 0,
                                    'approved_by_sr_account_manger' => 0,
                                    'approved_date_account_manager' => null,
                                    'approved_date_sr_account_manager' => null,
                                    'expire_scheme_type' => 'categorywise',
                                    'expire_scheme_id' => $value->id
                                ]);
                                $sv->update([
                                    'expire_scheme_status' => 1,
                                    'order_status' => 0,
                                    'approved_by_account_manager' => 0,
                                    'payment_status' => 0,
                                    'approved_by_sr_account_manger' => 0,
                                    'approved_date_account_manager' => null,
                                    'approved_date_sr_account_manager' => null,
                                    'expire_scheme_type' => 'categorywise',
                                    'expire_scheme_id' => $value->id,
                                    'scheme_order_type' => null,
                                    'scheme_id' => null,
                                    'scheme_type' => null,
                                    'parent_order_id' => 0
                                ]);
                            }
                            else{
                                $sv->sFormOrderProductLists()->delete();
                                $sv->delete();
                            }


                        } catch (\Exception $e) {

                        }

                    }
                }

            }
        }

        $mixedModelScheme = MixedModelScheme::where('end_date', date('Y-m-d', strtotime("-1 days")))->get();
        if (isset($mixedModelScheme) && $mixedModelScheme->isNotEmpty()) {
            foreach ($mixedModelScheme as $key => $value) {
                $sformOrder = SFormOrder::whereIn('order_status', [0, 1, 2, 3])->where('scheme_id', $value->id)->where('scheme_order_type', 'mixed_model')->get();

                if (isset($sformOrder) && $sformOrder->isNotEmpty()) {
                    foreach ($sformOrder as $sk => $sv) {
                        try {
                            if($sv->parent_order_id==0){
                                ParentSFormTransaction::where('id', $sv->parent_sform_id)->update([
                                    'expire_scheme_status' => 1,
                                    'order_status' => 0,
                                    'approved_by_account_manager' => 0,
                                    'payment_status' => 0,
                                    'approved_by_sr_account_manger' => 0,
                                    'approved_date_account_manager' => null,
                                    'approved_date_sr_account_manager' => null,
                                    'expire_scheme_type' => 'mixed_model',
                                    'expire_scheme_id' => $value->id
                                ]);
                                $sv->update([
                                    'expire_scheme_status' => 1,
                                    'order_status' => 0,
                                    'approved_by_account_manager' => 0,
                                    'payment_status' => 0,
                                    'approved_by_sr_account_manger' => 0,
                                    'approved_date_account_manager' => null,
                                    'approved_date_sr_account_manager' => null,
                                    'expire_scheme_type' => 'mixed_model',
                                    'expire_scheme_id' => $value->id,
                                    'scheme_order_type' => null,
                                    'scheme_id' => null,
                                    'scheme_type' => null,
                                    'parent_order_id' => 0
                                ]);
                            }
                            else{
                                $sv->sFormOrderProductLists()->delete();
                                $sv->delete();
                            }


                        } catch (\Exception $e) {

                        }

                    }
                }

            }
        }

        $monthlyScheme = MonthlyewiseScheme::where('end_date', date('Y-m-d', strtotime("-1 days")))->get();
        if (isset($monthlyScheme) && $monthlyScheme->isNotEmpty()) {
            foreach ($monthlyScheme as $key => $value) {
                $sformOrder = SFormOrder::whereIn('order_status', [0, 1, 2, 3])->where('scheme_id', $value->id)->where('scheme_order_type', 'monthly')->get();
                if (isset($sformOrder) && $sformOrder->isNotEmpty()) {
                    foreach ($sformOrder as $sk => $sv) {
                        try {
                            ParentSFormTransaction::where('id', $sv->parent_sform_id)->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'monthly',
                                'expire_scheme_id' => $value->id
                            ]);
                            $sv->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'monthly',
                                'expire_scheme_id' => $value->id,
                                'scheme_order_type' => null,
                                'scheme_id' => null,
                                'scheme_type' => null,
                                'parent_order_id' => 0
                            ]);

                        } catch (\Exception $e) {

                        }

                    }
                }
            }
        }


        $monthlyModelScheme = MonthlyModelWiseScheme::where('end_date', date('Y-m-d', strtotime("-1 days")))->get();
        if (isset($monthlyModelScheme) && $monthlyModelScheme->isNotEmpty()) {
            foreach ($monthlyModelScheme as $key => $value) {
                $sformOrder = SFormOrder::whereIn('order_status', [0, 1, 2, 3])->where('scheme_id', $value->id)->where('scheme_order_type', 'monthlyModels')->get();
                if (isset($sformOrder) && $sformOrder->isNotEmpty()) {
                    foreach ($sformOrder as $sk => $sv) {
                        try {
                            ParentSFormTransaction::where('id', $sv->parent_sform_id)->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'monthlyModels',
                                'expire_scheme_id' => $value->id
                            ]);
                            $sv->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'monthlyModels',
                                'expire_scheme_id' => $value->id,
                                'scheme_order_type' => null,
                                'scheme_id' => null,
                                'scheme_type' => null,
                                'parent_order_id' => 0
                            ]);

                        } catch (\Exception $e) {

                        }

                    }
                }
            }
        }

        $partCategory = PartCategorySchemes::where('end_date', date('Y-m-d', strtotime("-1 days")))->get();
        if (isset($partCategory) && $partCategory->isNotEmpty()) {
            foreach ($partCategory as $key => $value) {
                $sformOrder = SFormOrder::whereIn('order_status', [0, 1, 2, 3])->where('scheme_id', $value->id)->where('scheme_order_type', 'categorywiseparts')->get();
                if (isset($sformOrder) && $sformOrder->isNotEmpty()) {
                    foreach ($sformOrder as $sk => $sv) {
                        try {
                            ParentSFormTransaction::where('id', $sv->parent_sform_id)->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'categorywiseparts',
                                'expire_scheme_id' => $value->id
                            ]);
                            $sv->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'categorywiseparts',
                                'expire_scheme_id' => $value->id,
                                'scheme_order_type' => null,
                                'scheme_id' => null,
                                'scheme_type' => null,
                                'parent_order_id' => 0
                            ]);
                            $pform=PFormOrder::whereIn('order_status', [0, 1, 2])->where('parent_order_id',$sv->id)->where('scheme_order_type','categorywiseparts')->first();
                            if($pform){

                                $invicePform=InvoicePFormOrder::where('order_id',$pform->id)->first();
                                if($invicePform){
                                    InvoiceParentPFormTransaction::where('id',$invicePform->invoice_parent_p_form_id)->delete();
                                    $invicePform->invoicePFormOrderProductLists()->delete();
                                    $invicePform->delete();

                                }
                                ParentPFormTransaction::where('id',$pform->parent_p_form_id )->delete();
                                $pform->pFormOrderProductLists()->delete();
                                $pform->delete();
                            }
                            else{
                                $confirmedPform=PFormOrder::whereIn('order_status', [3,4])->where('parent_order_id',$sv->id)->where('scheme_order_type','categorywiseparts')->first();
                                if($confirmedPform){
                                    $invicePform=InvoicePFormOrder::where('order_id',$pform->id)->first();
                                    InvoiceParentPFormTransaction::where('id',$invicePform->invoice_parent_p_form_id)->update(['order_status' => 10]);
                                    $invicePform->update(['order_status' => 10]);
                                    ParentPFormTransaction::where('id',$pform->parent_p_form_id )->update(['order_status' => 10]);
                                    $confirmedPform->update(['order_status' => 10]);
                                }
                            }



                        } catch (\Exception $e) {

                        }



                    }
                }
            }
        }

        $foreignScheme = ForeignWiseScheme::where('end_date', date('Y-m-d', strtotime("-1 days")))->get();
        if (isset($foreignScheme) && $foreignScheme->isNotEmpty()) {
            foreach ($foreignScheme as $key => $value) {
                $sformOrder = SFormOrder::whereIn('order_status', [0, 1, 2, 3])->where('scheme_id', $value->id)->where('scheme_order_type', 'foreign')->get();
                if (isset($sformOrder) && $sformOrder->isNotEmpty()) {
                    foreach ($sformOrder as $sk => $sv) {
                        try {
                            ParentSFormTransaction::where('id', $sv->parent_sform_id)->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'foreign',
                                'expire_scheme_id' => $value->id
                            ]);
                            $sv->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'foreign',
                                'expire_scheme_id' => $value->id,
                                'scheme_order_type' => null,
                                'scheme_id' => null,
                                'scheme_type' => null,
                                'parent_order_id' => 0
                            ]);

                        } catch (\Exception $e) {

                        }

                    }
                }
            }
        }

        $invoiceWise = InvoicewiseScheme::where('end_date', date('Y-m-d', strtotime("-1 days")))->get();
        if (isset($invoiceWise) && $invoiceWise->isNotEmpty()) {
            foreach ($invoiceWise as $key => $value) {
                $sformOrder = SFormOrder::whereIn('order_status', [0, 1, 2, 3])->where('scheme_id', $value->id)->where('scheme_order_type', 'invoicewise')->get();
                if (isset($sformOrder) && $sformOrder->isNotEmpty()) {
                    foreach ($sformOrder as $sk => $sv) {
                        try {
                            ParentSFormTransaction::where('id', $sv->parent_sform_id)->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'invoicewise',
                                'expire_scheme_id' => $value->id
                            ]);
                            $sv->update([
                                'expire_scheme_status' => 1,
                                'order_status' => 0,
                                'approved_by_account_manager' => 0,
                                'payment_status' => 0,
                                'approved_by_sr_account_manger' => 0,
                                'approved_date_account_manager' => null,
                                'approved_date_sr_account_manager' => null,
                                'expire_scheme_type' => 'invoicewise',
                                'expire_scheme_id' => $value->id,
                                'scheme_order_type' => null,
                                'scheme_id' => null,
                                'scheme_type' => null,
                                'parent_order_id' => 0
                            ]);

                        } catch (\Exception $e) {

                        }

                    }
                }
            }
        }
    }
}
