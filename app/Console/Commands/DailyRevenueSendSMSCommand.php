<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\SendSMS;
use App\Category;
use App\Division;
use App\Models\ReportData;
use PDF;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Charts\UserChart;

class DailyRevenueSendSMSCommand extends Command
{
    protected $signature = 'DailyRevenueSendSMSDetails:DailyRevenueSendSMSDetails';

    protected $description = 'Send Daily Revenue SMS to Orpat Management Command';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {  
        try
        {  

           $date= date('Y-m-d',strtotime('-1 day'));
           $numbers = ['9537137777','9099477777','9930227118','9773062205'];
            
           // $numbers = ['9770231935']; 
           // $link2 =   'http://9m.io/69iB';
           // $link1 =   'http://9m.io/69ix';

            $link1 =   SendSMS::get_tiny_url('http://103.112.86.58:81/admin_orpat/public/admin/mobile-report/category-wise-report?date='.$date);
            $link2 =   SendSMS::get_tiny_url('http://103.112.86.58:81/admin_orpat/public/admin/mobile-report/division-wise-revenue-report?date='.$date);

            foreach($numbers as $number)
            {  
               SendSMS::dailyRevenueSendSMS($number,$link1,$link2, $date);
               sleep(3);
            }    
        }
        catch (\Exception $e)
        { 
            echo $e;
        }
    }
}
