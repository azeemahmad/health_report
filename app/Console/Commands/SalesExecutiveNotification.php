<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;






class SalesExecutiveNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SalesExecutiveNotification:SalesExecutiveNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sales Executive Notification';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $todaydate = date('Y-m-d');
        $allid=NotificationPusshwooshApi::where('type', '=', 'homesqintelligence')->pluck('type_id');
        $annualreport=HomeSqIntelligence::whereNotIn('id',$allid)->where('status', 1)->where('created_at', 'LIKE', "$todaydate%")->get();
        if (isset($annualreport) && $annualreport->isNotEmpty()) {
            foreach ($annualreport as $key => $value) {
                $client = Client::with(['client_interested_company','telegramUser'])->whereNotNull('telegram_status')->where('telegram_status','!=',0)->get();
                $client->map(function ($cli) {
                    $cli->subscription_type = unserialize($cli->subscription_type);
                });
                $pushtoken = array();
                foreach($client as $clientkey => $clientvalue){
                    if(in_array(2,$clientvalue->subscription_type)){
                        $telegramUser=$clientvalue->telegramUser()->first();
                        if(isset($telegramUser) && $telegramUser != null){
                            $pushtoken[] = $telegramUser->telegram_id;

                        }
                    }
                }

                $pushtoken=array_filter($pushtoken);
                if (isset($pushtoken) && !empty($pushtoken)) {
                    $addpush = NotificationPusshwooshApi::where('type', '=', 'homesqintelligence')->where('type_id', $value->id)->where('pooshwoosh_status', 1)->first();
                    if (!$addpush) {
                        try {
                            $data['security_code'] = 'NA';
                            $data['type'] = 'homesqintelligence';
                            $data['type_id'] = $value->id;
                            $data['newsfeed_id'] = null;
                            $data['pooshwoosh_status'] = 1;
                            $data['status'] = 1;
                            NotificationPusshwooshApi::create($data);
                            $newsID = $value->id;
                            $this->pushwoosh($newsID,$pushtoken);
                        } catch (\Illuminate\Database\QueryException $ex) {

                        }
                    }
                }
            }
        }
    }


    public function pushwoosh($newsID,$pushtoken)
    {
        $newsFeed=HomeSqIntelligence::find($newsID);
        $linkurlca=env('APP_URL') . '#sq_intelligence';
        $newline = "\n\n";
        $message=$newsFeed->title . $newline . strip_tags(substr($newsFeed->profile_longdescription, 0, 200));
        $url = 'https://cp.pushwoosh.com/json/1.3/createMessage';
        $custom_data = array('message' => $message, "link" => $linkurlca);
        $data = array(
            'application' => '90438-2C1BF',
            'auth' => 'p1JtpcCr6c9skirXB3duDDcu3JrD3uYLfusgdcW1wvtdOnAhezTjbwYySe0aYux79FqZD0jVaxj4TDkjNZ75',
            'notifications' => array(
                array(
                    'send_date' => 'now',
                    'content' => $message,
                    'ios_title' =>'SQ Intelligence',
                    'ios_subtitle' => 'SQ Intelligence',
                    'android_header'=>'SQ Intelligence',
                    //'link'=>$link,
                    // 'minimize_link' => 0,
                    'data' => array('custom' => $custom_data),
                    "devices" => $pushtoken,

                )
            )
        );
        $request = json_encode(['request' => $data]);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $response = curl_exec($ch);
        //dd($response);
        // $info = curl_getinfo($ch);
        curl_close($ch);
        return $response;
    }


}