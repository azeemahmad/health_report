<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ReportData;

class SyncReportDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:report_database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This cronjob responsible for sync closing count in report db daily at 5:00';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $product_ids =  ReportData::where('is_part',0)->groupBy('product_id')->pluck('product_id')->toArray();

        foreach( $product_ids as  $product_id){
            $closing_quantity =0;
           $products =  ReportData::where('is_part',0)->orderBy('date')->where('product_id',$product_id)->get();


           foreach($products as $key => $update_products){
            $closing_quantity += $update_products->production_quantity;
            $closing_quantity += $update_products->sales_return_quantity;
            $closing_quantity -= $update_products->integrated_quantity;
            $closing_quantity -= $update_products->online_quantity;
            $closing_quantity -= $update_products->state_quantity;
            $closing_quantity -= $update_products->export_quantity;
            $closing_quantity -= $update_products->nepal_quantity;
           
            $update_products->update(['closing_quantity'=>$closing_quantity]);

           }

        }
    }
}
