<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\SalesExecutiveNotification;
use App\Console\Commands\QueueListenCommand;
use App\Console\Commands\CancelSchemeOrderCommand;
use App\Console\Commands\SendSMSCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SalesExecutiveNotification::Class,
        QueueListenCommand::class,
        CancelSchemeOrderCommand::class,
        SendSMSCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       // $schedule->command('SalesExecutiveNotification:SalesExecutiveNotification')->withoutOverlapping()->between('9:00', '21:00')->everyFiveMinutes();
        $schedule->command('CancelSchemeOrderDetails:CancelSchemeOrderDetails')->withoutOverlapping()->between('9:45', '10:15')->everyFiveMinutes();
        $schedule->command('SendSMSDetails:SendSMSDetails')->withoutOverlapping()->dailyAt('08:00');
        $schedule->command('DailyRevenueSendSMSDetails:DailyRevenueSendSMSDetails')->withoutOverlapping()->dailyAt('07:00');
        $schedule->command('sync:report_database')->withoutOverlapping()->dailyAt('05:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
