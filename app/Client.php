<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Client extends Model
{
    use SoftDeletes;
    /* The database table used by the model.
    *
    * @var string
    */

    protected $table = 'clients';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['passport_no','dob','name', 'registered_date', 'sample_date','passport_display',
        'reported_date', 'url', 'status','logo',
        'sample_no','lab_no','age','gender','is_india','dubai_lab_no','full_age','file_no','clinic_file_no',
        'dubai_request_date','dubai_collected_on','dubai_recieved_od','dunai_authenticated_on','dubai_released_on','report_type'];

    public function logo(){
        return $this->hasMany('App\Models\ClientLogo');
    }




}
