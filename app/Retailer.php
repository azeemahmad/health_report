<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Retailer extends Model
{
     use SoftDeletes;
     /* The database table used by the model.
     *
     * @var string
     */
     use  HasRoles;
    protected $table = 'retailers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'sales_person_name','owner_name', 'retailer_outlet', 'address_1', 'address_2', 'address_3', 'address_4', 'city', 'state', 'pincode', 'whatsapp_mobile', 'mobile', 'land_line_number', 'email', 'area', 'classification', 'category_1', 'category_2', 'category_3', 'availability_status', 'territory_code', 'date_of_birth', 'wedding_year', 'pan_number', 'aadhar_number', 'gst_number'];

    public function salesPerson(){
        return $this->belongsTo('App\SalesPerson','sales_person_name','sales_person_name');
    }
    public function category(){

    }

    
}
