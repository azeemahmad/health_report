<?php

return [    
    'pdf' => [
        'enabled' => true,
        'binary'  => env('WKHTML_PDF_BINARY','C:\wkhtmltopdf\bin\wkhtmltopdf.exe'),
        'timeout' => false,
        'options' => [
            'page-size' => 'A4',
            'margin-top' => 30,
            'margin-right' => 6,
            'margin-left' => 6,
            'margin-bottom' => 10,
            'footer-right' => 'Page [page] of [toPage]',
            'footer-font-size' => 6,
            'footer-left' => 'Print Date [date] [time]',           
        ],
        'env'     => [],
    ],
    
    'image' => [
        'enabled' => true,
        'binary'  => env('WKHTML_IMG_BINARY', 'C:\wkhtmltopdf\bin\wkhtmltoimage.exe'),
        'timeout' => false,
        'options' => [],
        'env'     => [],
        'images'=>true
    ],
];
